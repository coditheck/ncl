"""
	@author: Obrymec
	@company: CodiTheck
	@famework: Godot Mega Assets
	@compatibility: Godot 3.x.x
	@platform: ANDROID || IOS || MACOSX || UWP || HTML5 || WINDOWS || LINUX
	@license: MIT
	@source: https://godot-mega-assets.herokuapp.com/home
	@language: GDscript
	@dimension: 2D || 3D
	@type: Indestructible
	@version: 0.4.3
	@created: 2021-06-16
	@updated: 2022-03-19
"""
################################################################################### [Main class] #############################################################################
"""@Description: A class that represents some basics functionalities common to all indestructibles modules of the Godot Mega Assets framework."""
tool class_name Indestructible, "indestructible.svg" extends Module;

#################################################################################### [Signals] ###############################################################################
"""@Description: Trigger when module is enabled."""
# warning-ignore:unused_signal
signal enabled ();
"""@Description: Trigger when module is disabled."""
# warning-ignore:unused_signal
signal disabled ();
"""@Description: Trigger after module initialisation."""
# warning-ignore:unused_signal
signal start ();
"""@Description: Trigger when module inputs values changed."""
# warning-ignore:unused_signal
signal values_changed ();
"""@Description: Trigger when a child or some children of module has/have been added or deleted."""
# warning-ignore:unused_signal
signal children_changed ();
"""@Description: Trigger when module parent changed."""
# warning-ignore:unused_signal
signal parent_changed ();

############################################################################## [Particulars variables] #######################################################################
# Contains the old module parent reference.
var _old_module_parent = null setget _unsetable_var_error;

############################################################################### [Properties manager] #########################################################################
# Unsetable module private variables.
func _unsetable_var_error (_new_value) -> void: self.output ("Can't change value of this variable. Because it private.", self.Message.ERROR, self);

############################################################################## [Logic and main tasks] ########################################################################
# Called before ready method run.
func _enter_tree () -> void: if not self.is_initialized (): self._indestructible_initialization (true);

# Returns a base class name.
func _get_class_name () -> String: return "Indestructible" if self.apply_visibility (self.MethodAccessMode.PROTECTED, "Module") else "Null";

# Listens module parent changed to notify unicity of indestructibles modules.
func _on_module_parent_changed () -> void:
	# Is the module parent changed ?
	if self.apply_visibility (self.MethodAccessMode.PROTECTED, "Module") and !Engine.editor_hint and self.get_parent () != self._old_module_parent:
		# Throwns an error message.
		self._output (("Can't change the parent of {" + self.name + "}. Because it's an indestructible module."), self.Message.ERROR);

# Manages indestructible module initialization.
func _indestructible_initialization (wait: bool) -> void:
	# Apply protected visibility to this method.
	if self.apply_visibility (self.MethodAccessMode.PROTECTED, "Module"):
		# Checks the unicity of this module and gets his parent reference.
		if self.get_class () != "Indestructible" && self.get_class () != "Recordable": self._check_unique (self.get_class ());
		# Waiting for idle frame, initializes module parent reference value and makes a verbose.
		if !Engine.editor_hint and wait: yield (self.get_tree (), "idle_frame"); _old_module_parent = self.get_parent (); self.verbose ("Indestructible initializing...");
		# Connects "parent_changed" signal to "_on_module_parent_changed" method to listen parent changements.
		if !self.is_connected ("parent_changed", self, "_on_module_parent_changed") && self.connect ("parent_changed", self, "_on_module_parent_changed") != OK: pass;
		# Makes module be indestructible.
		if !Engine.editor_hint and self.get_parent ().name != "DontDestroyOnLoad": self.dont_destroy_on_load ('.', self); self._thrown_basics_events ("Indestructible");

# Does the type of the module of nature indestructible have several instances of itself in the tree of the scene defines ?
func _check_unique (mcn: String) -> void:
	# Apply private visibility to this method.
	if self.apply_visibility (self.MethodAccessMode.PRIVATE):
		# The module is now indestructible.
		var refs = null; if self.get_parent ().name == "DontDestroyOnLoad": refs = self.get_indestructible (mcn, self, self.NodeProperty.TYPE, 2);
		# The game isn't initialised.
		elif !Engine.editor_hint and !self.is_game_initialised (): refs = self.search (mcn, self.get_tree ().current_scene, self.NodeProperty.TYPE, 2);
		# Otherwise.
		else: refs = self.search (mcn, self.get_viewport (), self.NodeProperty.TYPE, 2);
		# Checks the search result.
		if refs is Array and refs [0].has_method ("is_dont_destroy_mode") and refs [0].is_dont_destroy_mode () is bool and refs [0].is_dont_destroy_mode ():
			# Shows error message and destroys immediatly the new created reference.
			self._output (("Can't have severals instance of " + mcn + ". Because it's an indestructible module."), self.Message.ERROR); self.queue_free ();

############################################################################### [Availables features] ########################################################################
"""@Description: Returns module version."""
static func get_version () -> String: return "0.4.3";

"""@Description: Returns module origins."""
static func get_origin_name () -> String: return "MegaAssets.Module.Indestructible";

"""@Description: Is the module is indestructible ?"""
static func is_dont_destroy_mode () -> bool: return true;

"""@Description: Returns module class name."""
func get_class () -> String: return "Indestructible";

"""
	@Description: Opens the documentation associated with this class.
	@Parameters:
		Node object: Which node will be considered to perform the different operations ?
		String feature: The documentation will target which functionality of style ?
		float delay: What is the deadtime before the opening of the documentation ?
"""
static func open_doc (object: Node, feature: String = String (''), delay: float = 0.0) -> void:
	# Opens the documentation.
	open_doc_manager (object, "https://godot-mega-assets.herokuapp.com/docs/bases/indestructible", feature, delay);

"""
	@Description: Restarts module. Made very careful during module reboots. This can be problematic in certain cases.
	@Parameters:
		float delay: What is the timeout before restarting module ?
"""
func restart (delay: float = 0.0) -> void:
	# The module is it enabled ?
	if self.check_initialization () and self.is_unlock ():
		# Waiting for the given delay and resets all module particulars properties.
		if delay > 0.0 and !Engine.editor_hint: yield (self.get_tree ().create_timer (delay), "timeout"); _old_module_parent = null;
		# Disconnects this module reference from "parent_changed" signal.
		if self.is_connected ("parent_changed", self, "_on_module_parent_changed") && self.disconnect ("parent_changed", self, "_on_module_parent_changed") != OK: pass;
		# Makes a verbose and restarts the module.
		self.verbose ("Indestructible restarting..."); .restart (); if self._get_class_name () == "Recordable": self._saveable_initialization (false);
		# Otherwise.
		else:
			# Calls the last definition of Godot "_enter_tree ()" and "_ready ()" methods.
			self._enter_tree (); self._ready ();
