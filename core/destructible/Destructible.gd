"""
	@author: Obrymec
	@company: CodiTheck
	@famework: Godot Mega Assets
	@compatibility: Godot 3.x.x
	@platform: ANDROID || IOS || MACOSX || UWP || HTML5 || WINDOWS || LINUX
	@license: MIT
	@source: https://godot-mega-assets.herokuapp.com/home
	@language: GDscript
	@dimension: 2D || 3D
	@type: Destructible
	@version: 0.2.2
	@created: 2021-06-15
	@updated: 2022-03-19
"""
################################################################################### [Main class] #############################################################################
"""@Description: A class that represents some basics functionalities common to all destructibles modules of the Godot Mega Assets framework."""
tool class_name Destructible, "destructible.svg" extends Module;

################################################################################### [Attributes] #############################################################################
# Contains all basics properties of a Godot Mega Assets module.
func _basics_destroyable_module_properties () -> void:
	# Apply private visibility to this method.
	if self.apply_visibility (self.MethodAccessMode.PRIVATE):
		# Contains the module title category.
		self.bind_prop (Dictionary ({title = "Destructible", index = 0}));
		"""@Description: Enumeration controlling the scope of the events of a module."""
		self.bind_prop (Dictionary ({source = "EventsScope", dropdown = self.WornEvents.keys (), value = 1, min = 0, max = 3,
			changed = Dictionary ({callback = "module_values_changed ()", params = Array ([null, null])}), attach = "EventsScope"
		}));
		"""
			@Description: Table of dictionaries managing the links internal and external events of a module with one or more given methods and properties.
				To use this option, the developer must follow the key/value concept. Note that any event listened to without going through a connection
				beforehand will fall victim to the reach of events. This option is only available on destructible modules. It is also possible instead of
				a fixed value: that it is at the level of a property or parameters of a method, to refer the value of another property or of a result returned
				by a method recursively. To be able to do this, you must use a character string in which we will have a special character: the "?" placed in front
				of the name of the property or method whose value will be used as the value assigned to said property or parameter of the method in question.
				When you want to act on a method during a trigger, you must put at the end, the characters "()" in order to make the distinction between a property
				and a method.
		"""
		self.bind_prop (Dictionary ({source = "EventsBindings", value = Array ([]), duplicate = Engine.editor_hint, type = TYPE_ARRAY,
			changed = Dictionary ({callback = "module_values_changed ()", params = Array ([null, null])}), attach = "EventsBindings",
			clone = Dictionary ({id = "trigger", statement = "AutoCompile", actions = Dictionary ({message = "Can't have severals values of {trigger} key."})})
		}));
		# Attaches "EventsBindings" property to "AutoCompile" property.
		self.override_prop (Dictionary ({attach = PoolStringArray (["AutoCompile", "EventsBindings"])}), "AutoCompile");

#################################################################################### [Signals] ###############################################################################
"""@Description: Trigger when module is enabled."""
# warning-ignore:unused_signal
signal enabled (node);
"""@Description: Trigger when module is disabled."""
# warning-ignore:unused_signal
signal disabled (node);
"""@Description: Trigger after module initialisation."""
# warning-ignore:unused_signal
signal start (node);
"""@Description: Trigger when module inputs values changed."""
# warning-ignore:unused_signal
signal values_changed (node);
"""@Description: Trigger when a child or some children of module has/have been added or deleted."""
# warning-ignore:unused_signal
signal children_changed (node);
"""@Description: Trigger when module parent changed."""
# warning-ignore:unused_signal
signal parent_changed (node);

############################################################################## [Logic and main tasks] ########################################################################
# This method is called on game initialization and before all nodes instanciation.
func _init () -> void: self._basics_destroyable_module_properties ();

# Called before ready method run.
func _enter_tree () -> void: if not self.is_initialized (): self._destructible_initialization (true);

# Returns a base class name.
func _get_class_name () -> String: return "Destructible" if self.apply_visibility (self.MethodAccessMode.PROTECTED, "Module") else "Null";

# This method is called when the module started.
func _on_module_start (module: Object) -> void: if self.apply_visibility (self.MethodAccessMode.PROTECTED, "Module"): self.thrown_event ("start", module);

# This method is called when the module is enabled.
func _on_module_enabled (module: Object) -> void: if self.apply_visibility (self.MethodAccessMode.PROTECTED, "Module"): self.thrown_event ("enabled", module);

# This method is called when the module is disabled.
func _on_module_disabled (module: Object) -> void: if self.apply_visibility (self.MethodAccessMode.PROTECTED, "Module"): self.thrown_event ("disabled", module);

# This method is called when any module property value has changed.
func _on_module_values_changed (data: Dictionary) -> void: if self.apply_visibility (self.MethodAccessMode.PROTECTED, "Module"): self.thrown_event ("values_changed", data);

# This method is method is called when the module parent has changed.
func _on_module_parent_changed (module: Object) -> void: if self.apply_visibility (self.MethodAccessMode.PROTECTED, "Module"): self.thrown_event ("parent_changed", module);

# This method is called when the module children has changed.
func _on_module_children_changed (module: Object) -> void:
	# Throwns "children_changed" event.
	if self.apply_visibility (self.MethodAccessMode.PROTECTED, "Module"): self.thrown_event ("children_changed", module);

# Manages destructible module initialization.
func _destructible_initialization (wait: bool) -> void:
	# Apply protected visibility to this method.
	if self.apply_visibility (self.MethodAccessMode.PROTECTED, "Module"):
		# Waiting for idle frame.
		if !Engine.editor_hint and wait: yield (self.get_tree (), "idle_frame");
		# If ever on event is not connected to this instance.
		if not self.is_connected ("start", self, "_on_module_start"):
			# Connecting module "start" and "enabled" events to this instance.
			if self.connect ("start", self, "_on_module_start") != OK: pass; if self.connect ("enabled", self, "_on_module_enabled") != OK: pass;
			# Connecting module "disabled" and "values_changed" events to this instance.
			if self.connect ("disabled", self, "_on_module_disabled") != OK: pass; if self.connect ("values_changed", self, "_on_module_values_changed") != OK: pass;
			# Connecting module "children_changed" event to this instance.
			if self.connect ("children_changed", self, "_on_module_children_changed") != OK: pass;
			# Connecting module "parent_changed" event to this instance.
			if self.connect ("parent_changed", self, "_on_module_parent_changed") != OK: pass;
		# Makes a verbose and throwns basics module events.
		self.verbose ("Destructible initializing..."); self._thrown_basics_events ("Destructible");

############################################################################### [Availables features] ########################################################################
"""@Description: Returns module version."""
static func get_version () -> String: return "0.2.2";

"""@Description: Returns module origins."""
static func get_origin_name () -> String: return "MegaAssets.Module.Destructible";

"""@Description: Returns module class name."""
func get_class () -> String: return "Destructible";

"""
	@Description: Opens the documentation associated with this class.
	@Parameters:
		Node object: Which node will be considered to perform the different operations ?
		String feature: The documentation will target which functionality of style ?
		float delay: What is the deadtime before the opening of the documentation ?
"""
static func open_doc (object: Node, feature: String = String (''), delay: float = 0.0) -> void:
	# Opens the documentation.
	open_doc_manager (object, "https://godot-mega-assets.herokuapp.com/docs/bases/destructible", feature, delay);

"""
	@Description: Manages all events on the module. Note that this method doesn't call "emit_signal" method.
	@Parameters:
		String event: Contains an event name.
		Variant parameters: Contains event parameters when it called.
"""
func thrown_event (event: String, parameters, delay: float = 0.0) -> void:
	# Apply protected visibility to this method.
	if apply_visibility (self.MethodAccessMode.PROTECTED, PoolStringArray (["Saveable", self.get_class ()])):
		# Contains the corrected form of the "EventsScope" module property.
		var events_scope = self.get_prop ("EventsScope"); events_scope = events_scope if events_scope is int else self.WornEvents.NONE;
		# Checks events scope value.
		if events_scope != self.WornEvents.NONE:
			# Contains the corrected form of the "EventsBindings" module property.
			var events_bindings = self.get_prop ("EventsBindings"); events_bindings = events_bindings if events_bindings is Array else Array ([]);
			# Waiting for the given delay.
			if delay > 0.0 and !Engine.editor_hint: yield (self.get_tree ().create_timer (delay), "timeout");
			# Raises the passed event on all directly listen and runs all configurations on "EventsBindings" property.
			self.raise_event (Dictionary ({"event": ("_on_" + event), params = parameters}), self, events_scope); for cfg in events_bindings:
				# Checks configurations validation.
				if cfg is Dictionary && cfg.has ("trigger") && cfg.trigger is String && cfg.trigger == event && cfg.has ("actions"):
					# Runs the current event configurations.
					self.run_slots (cfg.actions, self, (float (cfg.timeout) if cfg.has ("timeout") && self.is_number (cfg.timeout) else 0.0));

"""
	@Description: Restarts module. Made very careful during module reboots. This can be problematic in certain cases.
	@Parameters:
		float delay: What is the timeout before restarting module ?
"""
func restart (delay: float = 0.0) -> void:
	# The module is it enabled ?
	if self.check_initialization () and self.is_unlock ():
		# Waiting for the given delay.
		if delay > 0.0 and !Engine.editor_hint: yield (self.get_tree ().create_timer (delay), "timeout");
		# "start" event is it already connected to this module reference ?
		if self.is_connected ("start", self, "_on_module_start"):
			# Disconnects this module reference from "start" and "enabled" signals.
			if self.disconnect ("start", self, "_on_module_start") != OK: pass; if self.disconnect ("enabled", self, "_on_module_enabled") != OK: pass;
			# Disconnects this module reference from "children_changed" signal.
			if self.disconnect ("children_changed", self, "_on_module_children_changed") != OK: pass;
			# Disconnects this module reference from "values_changed" signal.
			if self.disconnect ("values_changed", self, "_on_module_values_changed") != OK: pass;
			# Disconnects this module reference from "parent_changed" signal.
			if self.disconnect ("parent_changed", self, "_on_module_parent_changed") != OK: pass;
			# Disconnects this module reference from "disabled" signal.
			if self.disconnect ("disabled", self, "_on_module_disabled") != OK: pass;
		# Makes a verbose and restarts the module.
		self.verbose ("Destructible restarting..."); .restart (); if self._get_class_name () == "Saveable": self._saveable_initialization (false);
		# Otherwise.
		else:
			# Calls the last definition of Godot "_enter_tree ()" and "_ready ()" methods.
			self._enter_tree (); self._ready ();
