"""
	@author: Obrymec
	@company: CodiTheck
	@famework: Godot Mega Assets
	@compatibility: Godot 3.x.x
	@platform: ANDROID || IOS || MACOSX || UWP || HTML5 || WINDOWS || LINUX
	@license: MIT
	@source: https://godot-mega-assets.herokuapp.com/home
	@language: GDscript
	@dimension: 2D || 3D
	@type: MagaAssets
	@version: 0.1.3
	@created: 2020-10-21
	@updated: 2022-03-19
"""
################################################################################## [Main class] #############################################################################
"""
	@Description: MegaAssets is a class with a multitude of very useful and interesting that the developer can use to go faster in these programming. It
		is also a "low level" library on which the modules (high level) are based to provide good performance to their users. The developer can create some
		classes, all derived from the MegaAssets class.
		NB: Never try to instantiate MegaAssets if you want to avoid making the console of your engine red.
"""
tool class_name MegaAssets, "mega_assets.svg" extends Node;

################################################################################## [Attributes] #############################################################################
const _B32: int = 0xffffffff; # Contains the B32 value.
const _U32_SHIFTS: Array = Array ([24, 16, 8, 0]); # Contains an array of values [U32 SHIFTS].
const _U64_SHIFTS: Array = Array ([56, 48, 40, 32, 24, 16, 8, 0]); # Contains an array of values [U64 SHIFTS].
const _U64_SHIFTS_INV: Array = Array ([0, 8, 16, 24, 32, 40, 48, 56]); # Contains an array of values [U64 SHIFTS INV].
# Contains all supported hex values for hashing methods.
const _ROTL64_MASK: Array = Array ([0xffffffffffff, 0x7fffffffffff, 0x3fffffffffff, 0x1fffffffffff, 0x0fffffffffff, 0x07ffffffffff, 0x03ffffffffff,
	0x01ffffffffff, 0x00ffffffffff, 0x007fffffffff, 0x003fffffffff, 0x001fffffffff, 0x000fffffffff, 0x0007ffffffff, 0x0003ffffffff, 0x0001ffffffff,
	0x0000ffffffff, 0x00007fffffff, 0x00003fffffff, 0x00001fffffff, 0x00000fffffff, 0x000007ffffff, 0x000003ffffff, 0x000001ffffff, 0x000000ffffff,
	0x0000007fffff, 0x0000003fffff, 0x0000001fffff, 0x0000000fffff, 0x00000007ffff, 0x00000003ffff, 0x00000001ffff, 0x00000000ffff, 0x000000007fff,
	0x000000003fff, 0x000000001fff, 0x000000000fff, 0x0000000007ff, 0x0000000003ff, 0x0000000001ff, 0x0000000000ff, 0x00000000007f, 0x00000000003f,
	0x00000000001f, 0x00000000000f, 0x000000000007, 0x000000000003, 0x000000000001, 0x00000000ffff, 0x000000007fff, 0x000000000003, 0x000000000001,
	0x000000000fff, 0x0000000007ff, 0x0000000003ff, 0x000000000001, 0x0000000000ff, 0x00000000007f, 0x00000000003f, 0x00000000001f, 0x00000000000f,
	0x000000000007, 0x000000000003, 0x000000000001, 0x000000000000
]);
# Contains all supported hex values on SBOX (AES encryption).
const _S_BOX = Array ([
	PoolByteArray ([0x63, 0x7c, 0x77, 0x7b, 0xf2, 0x6b, 0x6f, 0xc5, 0x30, 0x01, 0x67, 0x2b, 0xfe, 0xd7, 0xab, 0x76]),
	PoolByteArray ([0xca, 0x82, 0xc9, 0x7d, 0xfa, 0x59, 0x47, 0xf0, 0xad, 0xd4, 0xa2, 0xaf, 0x9c, 0xa4, 0x72, 0xc0]),
	PoolByteArray ([0xb7, 0xfd, 0x93, 0x26, 0x36, 0x3f, 0xf7, 0xcc, 0x34, 0xa5, 0xe5, 0xf1, 0x71, 0xd8, 0x31, 0x15]),
	PoolByteArray ([0x04, 0xc7, 0x23, 0xc3, 0x18, 0x96, 0x05, 0x9a, 0x07, 0x12, 0x80, 0xe2, 0xeb, 0x27, 0xb2, 0x75]),
	PoolByteArray ([0x09, 0x83, 0x2c, 0x1a, 0x1b, 0x6e, 0x5a, 0xa0, 0x52, 0x3b, 0xd6, 0xb3, 0x29, 0xe3, 0x2f, 0x84]),
	PoolByteArray ([0x53, 0xd1, 0x00, 0xed, 0x20, 0xfc, 0xb1, 0x5b, 0x6a, 0xcb, 0xbe, 0x39, 0x4a, 0x4c, 0x58, 0xcf]),
	PoolByteArray ([0xd0, 0xef, 0xaa, 0xfb, 0x43, 0x4d, 0x33, 0x85, 0x45, 0xf9, 0x02, 0x7f, 0x50, 0x3c, 0x9f, 0xa8]),
	PoolByteArray ([0x51, 0xa3, 0x40, 0x8f, 0x92, 0x9d, 0x38, 0xf5, 0xbc, 0xb6, 0xda, 0x21, 0x10, 0xff, 0xf3, 0xd2]),
	PoolByteArray ([0xcd, 0x0c, 0x13, 0xec, 0x5f, 0x97, 0x44, 0x17, 0xc4, 0xa7, 0x7e, 0x3d, 0x64, 0x5d, 0x19, 0x73]),
	PoolByteArray ([0x60, 0x81, 0x4f, 0xdc, 0x22, 0x2a, 0x90, 0x88, 0x46, 0xee, 0xb8, 0x14, 0xde, 0x5e, 0x0b, 0xdb]),
	PoolByteArray ([0xe0, 0x32, 0x3a, 0x0a, 0x49, 0x06, 0x24, 0x5c, 0xc2, 0xd3, 0xac, 0x62, 0x91, 0x95, 0xe4, 0x79]),
	PoolByteArray ([0xe7, 0xc8, 0x37, 0x6d, 0x8d, 0xd5, 0x4e, 0xa9, 0x6c, 0x56, 0xf4, 0xea, 0x65, 0x7a, 0xae, 0x08]),
	PoolByteArray ([0xba, 0x78, 0x25, 0x2e, 0x1c, 0xa6, 0xb4, 0xc6, 0xe8, 0xdd, 0x74, 0x1f, 0x4b, 0xbd, 0x8b, 0x8a]),
	PoolByteArray ([0x70, 0x3e, 0xb5, 0x66, 0x48, 0x03, 0xf6, 0x0e, 0x61, 0x35, 0x57, 0xb9, 0x86, 0xc1, 0x1d, 0x9e]),
	PoolByteArray ([0xe1, 0xf8, 0x98, 0x11, 0x69, 0xd9, 0x8e, 0x94, 0x9b, 0x1e, 0x87, 0xe9, 0xce, 0x55, 0x28, 0xdf]),
	PoolByteArray ([0x8c, 0xa1, 0x89, 0x0d, 0xbf, 0xe6, 0x42, 0x68, 0x41, 0x99, 0x2d, 0x0f, 0xb0, 0x54, 0xbb, 0x16])
]);
# Contains all supported hex values on SBOX inverse (AES encryption).
const _S_BOX_INVERSE = Array ([
	PoolByteArray ([0x52, 0x09, 0x6a, 0xd5, 0x30, 0x36, 0xa5, 0x38, 0xbf, 0x40, 0xa3, 0x9e, 0x81, 0xf3, 0xd7, 0xfb]),
	PoolByteArray ([0x7c, 0xe3, 0x39, 0x82, 0x9b, 0x2f, 0xff, 0x87, 0x34, 0x8e, 0x43, 0x44, 0xc4, 0xde, 0xe9, 0xcb]),
	PoolByteArray ([0x54, 0x7b, 0x94, 0x32, 0xa6, 0xc2, 0x23, 0x3d, 0xee, 0x4c, 0x95, 0x0b, 0x42, 0xfa, 0xc3, 0x4e]),
	PoolByteArray ([0x08, 0x2e, 0xa1, 0x66, 0x28, 0xd9, 0x24, 0xb2, 0x76, 0x5b, 0xa2, 0x49, 0x6d, 0x8b, 0xd1, 0x25]),
	PoolByteArray ([0x72, 0xf8, 0xf6, 0x64, 0x86, 0x68, 0x98, 0x16, 0xd4, 0xa4, 0x5c, 0xcc, 0x5d, 0x65, 0xb6, 0x92]),
	PoolByteArray ([0x6c, 0x70, 0x48, 0x50, 0xfd, 0xed, 0xb9, 0xda, 0x5e, 0x15, 0x46, 0x57, 0xa7, 0x8d, 0x9d, 0x84]),
	PoolByteArray ([0x90, 0xd8, 0xab, 0x00, 0x8c, 0xbc, 0xd3, 0x0a, 0xf7, 0xe4, 0x58, 0x05, 0xb8, 0xb3, 0x45, 0x06]),
	PoolByteArray ([0xd0, 0x2c, 0x1e, 0x8f, 0xca, 0x3f, 0x0f, 0x02, 0xc1, 0xaf, 0xbd, 0x03, 0x01, 0x13, 0x8a, 0x6b]),
	PoolByteArray ([0x3a, 0x91, 0x11, 0x41, 0x4f, 0x67, 0xdc, 0xea, 0x97, 0xf2, 0xcf, 0xce, 0xf0, 0xb4, 0xe6, 0x73]),
	PoolByteArray ([0x96, 0xac, 0x74, 0x22, 0xe7, 0xad, 0x35, 0x85, 0xe2, 0xf9, 0x37, 0xe8, 0x1c, 0x75, 0xdf, 0x6e]),
	PoolByteArray ([0x47, 0xf1, 0x1a, 0x71, 0x1d, 0x29, 0xc5, 0x89, 0x6f, 0xb7, 0x62, 0x0e, 0xaa, 0x18, 0xbe, 0x1b]),
	PoolByteArray ([0xfc, 0x56, 0x3e, 0x4b, 0xc6, 0xd2, 0x79, 0x20, 0x9a, 0xdb, 0xc0, 0xfe, 0x78, 0xcd, 0x5a, 0xf4]),
	PoolByteArray ([0x1f, 0xdd, 0xa8, 0x33, 0x88, 0x07, 0xc7, 0x31, 0xb1, 0x12, 0x10, 0x59, 0x27, 0x80, 0xec, 0x5f]),
	PoolByteArray ([0x60, 0x51, 0x7f, 0xa9, 0x19, 0xb5, 0x4a, 0x0d, 0x2d, 0xe5, 0x7a, 0x9f, 0x93, 0xc9, 0x9c, 0xef]),
	PoolByteArray ([0xa0, 0xe0, 0x3b, 0x4d, 0xae, 0x2a, 0xf5, 0xb0, 0xc8, 0xeb, 0xbb, 0x3c, 0x83, 0x53, 0x99, 0x61]),
	PoolByteArray ([0x17, 0x2b, 0x04, 0x7e, 0xba, 0x77, 0xd6, 0x26, 0xe1, 0x69, 0x14, 0x63, 0x55, 0x21, 0x0c, 0x7d])
]);
# Contains all supported hex values on MIX MATRIX (AES encryption).
const _MIX_MATRIX = Array ([PoolByteArray ([0x02, 0x03, 0x01, 0x01]), PoolByteArray ([0x01, 0x02, 0x03, 0x01]),
	PoolByteArray ([0x01, 0x01, 0x02, 0x03]), PoolByteArray ([0x03, 0x01, 0x01, 0x02])
]);
# Contains all supported hex values on MIX MATRIX INVERSE (AES encryption).
const _MIX_MATRIX_INVERSE = Array ([PoolByteArray ([0x0E, 0x0B, 0x0D, 0x09]), PoolByteArray ([0x09, 0x0E, 0x0B, 0x0D]),
	PoolByteArray ([0x0D, 0x09, 0x0E, 0x0B]), PoolByteArray ([0x0B, 0x0D, 0x09, 0x0E])
]);
# Contains all supported hex values on RCI (AES encryption).
const _RCI = PoolByteArray ([0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x1B, 0x36]);
# Round constants. First 32 bits of the fractional parts of the cube roots of the first 64 primes 2..311 (hashing method).
const _RK: Array = Array ([0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5, 0x3956c25b, 0x59f111f1, 0x923f82a4, 0xab1c5ed5, 0xd807aa98, 0x12835b01, 0x243185be,
	0x550c7dc3, 0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174, 0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc, 0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da,
	0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7, 0xc6e00bf3, 0xd5a79147, 0x06ca6351, 0x14292967, 0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13, 0x650a7354,
	0x766a0abb, 0x81c2c92e, 0x92722c85, 0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3, 0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070, 0x19a4c116, 0x1e376c08,
	0x2748774c, 0x34b0bcb5, 0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f, 0x682e6ff3, 0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208, 0x90befffa, 0xa4506ceb, 0xbef9a3f7,
	0xc67178f2
]);
# Contains the useful letters for generating name.
const _LETTERS = Dictionary ({"COMPOSE": PoolStringArray (["qu", "gu", "cc", "sc", "tr", "fr", "pr", "br", "cr", "ch", "ll", "tt", "ss", "gn"]),
	"VOYELLE": PoolStringArray (['a', 'e', 'a', 'e', 'i', 'o', 'o', 'a', 'e', 'a', 'e', 'i', 'a', 'e', 'a', 'e', 'i', 'o', 'o', 'a', 'e', 'a', 'e', 'i', 'y']),
	"DOUBLE_VOYELLE": PoolStringArray (["oi", "ai", "ou", "ei", "ae", "eu", "ie", "ea"]),
	"DOUBLE_CONSONNE": PoolStringArray (["mm", "nn", "st", "ch", "ll", "tt", "ss"]),
	"CONSONNE": PoolStringArray (['b', 'c', 'c', 'd', 'f', 'g', 'h', 'j', 'l', 'm', 'n', 'n', 'p', 'r', 'r', 's', 't', 's', 't', 'b', 'c', 'c', 'd', 'f', 'g', 'h',
		'j', 'k', 'l', 'm', 'n', 'n', 'p', 'r', 'r', 's', 't', 's', 't', 'v', 'w', 'x', 'z'])
});
# Contains the possible transitions for generating name.
const _TRANSITION = Dictionary ({"DOUBLE_CONSONNE": PoolStringArray (["VOYELLE", "DOUBLE_VOYELLE"]), "COMPOSE": PoolStringArray (["VOYELLE"]),
	"INITIAL": PoolStringArray (["VOYELLE", "CONSONNE", "COMPOSE"]), "VOYELLE": PoolStringArray (["CONSONNE", "DOUBLE_CONSONNE", "COMPOSE"]),
	"DOUBLE_VOYELLE": PoolStringArray (["CONSONNE", "DOUBLE_CONSONNE", "COMPOSE"]), "CONSONNE": PoolStringArray (["VOYELLE", "DOUBLE_VOYELLE"])
});
# Contains all scripts core names.
const _SCRIPTS_CORE = PoolStringArray (["MegaAssets", "Module", "Destructible", "Indestructible", "Saveable", "Recordable"]);
# Contains all constants about media states.
enum MediaState {NONE = 0, PLAY = 1, PAUSE = 2, STOP = 3, LOOP = 4};
# Contains all constants about "output" function.
enum Message {NORMAL = 0, WARNING = 1, ERROR = 2};
# Contains all contants about "apply_visibility" function.
enum MethodAccessMode {PUBLIC = 0, PRIVATE = 1, PROTECTED = 2, ANY = 3};
# Contains all constants for security method about "serialize/deserialize" functions.
enum SecurityMethod {NONE = 0, AES = 1, ARCFOUR = 2, CHACHA = 3, BINARY = 4, HEXADECIMAL = 5, OCTAL = 6, GODOT = 7};
# Contains all constants for security level about "serialize/deserialize" functions.
enum SecurityLevel {SIMPLE = 0, NORMAL = 1, ADVANCED = 2};
# Contains all constants about "serialize/deserialize" functions.
enum Checksum {NONE = 0, MD5 = 1, SHA256 = 2};
# Contains all constants for node property about "search" functions.
enum NodeProperty {NAME = 0, GROUP = 1, TYPE = 2, ANY = 3};
# Contains all constants for encryption method about "encrypt/decrypt" functions.
enum EncryptionMethod {AES = 0, ARCFOUR = 1, CHACHA = 2};
# Contains all constants for encryption schema about "encrypt/decrypt/hash_var" functions.
enum EncryptionSchema {BASE64 = 0, HEXADECIMAL = 1, RAW = 2};
# Contains all constants for hashing method about "hash_var" function.
enum HashingMethod {SIP = 0, SHA256 = 1, HASHMAC_SHA256 = 2, GODOT_SHA256 = 3, MD5 = 4, AES = 5, ARCFOUR = 6, CHACHA = 7};
# Contains all constants for event scope about event functions.
enum WornEvents {NONE = 0, CHILDREN_ONLY = 1, PARENTS_ONLY = 2, ALL = 3};
# Contains all constants for directions about "instanciates" function.
enum Orientation {NORMAL = 0, REVERSED = 1, RANDOM = 2, ALTERNATE = 3, ALTERNATE_REVERSE = 4, PING_PONG = 5};
# Contains all constants for array convertion about "any_to_array" function.
enum DictionaryProperty {KEYS = 0, VALUES = 1, BOTH = 2};
# Contains all contants about "apply_camera_effect" function.
enum Disposal {NONE = 0, CENTER = 1, TOP = 2, RIGHT = 3, BOTTOM = 4, LEFT = 5, FORWARD = 6, BACKWARD = 7};
# Contains all constants about "apply_camera_effect" function.
enum FullMonitor {NONE = 0, HORIZONTAL = 1, VERTICAL = 2, BOTH = 3};
# Contains all constants about "apply_game_settings" function.
enum GameGrade {LOW = 0, MEDIUM = 1, HIGH = 2};
# Contains all compress mode constants for an image compression.
enum ImageCompression {NONE = 0, ETC = 1, ETC2 = 2, S3TC = 3};
# Contains all constants for an image format.
enum ImageFormat {RH = 0, RF = 1, RGH = 2, RGF = 3, RGBH = 4, RGBF = 5, RGBAH = 6, RGBAF = 7, RGBA4444 = 8, RGBA5551 = 9, RGBE9995 = 10};
# Contains all constants about mains system folders.
enum Path {
	GAME_LOCATION = 0, OS_ROOT = 1, USER_DATA = 2, USER_ROOT = 3, USER_DESKTOP = 4, USER_PICTURES = 5, USER_MUSIC = 6, USER_VIDEOS = 7,
	USER_DOCUMENTS = 8, USER_DOWNLOADS = 9
};
# Contains all constants about "apply_camera_effect" function.
enum CameraEffect {
	NONE = 0, SIMLE_BLUR = 1, VIGNETTE = 2, PIXELIZE = 3, WHIRL = 4, SEPIA = 5, NEGATIVE = 6, CONTRASTED = 7,
	NORMALIZED = 8, BCS = 9, MIRAGE = 10, OLD_FILM = 11, STATIC_CRT = 12, MOSAIC = 13, LCD = 14, GAMEBOY = 15,
	TONE_COMIC = 16, INVERT = 17, TV = 18, VHS = 19, VHS_GLITCH = 20, VHS_PAUSE = 21, VHS_SIMPLE_GLITCH = 22,
	BW = 23, BETTER_CC = 24, COLOR_PRECISION = 25, GRAIN = 26, LENS_DISTORTION = 27, SHARPNESS = 28,
	SIMPLE_GRAIN = 29, RANDOM_NOISE = 30, SCANLINES = 31, GLITCH = 32, CRT_SCREEN = 33, SIMPLE_CRT = 34,
	SIMPLE_GLITCH = 35, CRT_LOTTES = 36, ABERRATION = 37, ADVANCED_MOSIC = 38, ANIMATED_NOISE = 39,
	AVERAGE = 40, BACKGROUND = 41, BINARY_CONVERSION = 42, BINARY_DEFAULT_MIX = 43, COLOR_BLINDNESS = 44,
	DEFAULT = 45, EDGE_DEFAULT_MIX = 46, EDGE_MOTION_MIX = 47, EDGE_PREWITT = 48, SIMPLE_EDGE = 49,
	EDGE_SOBEL = 50, MONOCHROME = 51, MOTION = 52, SIMPLE_MOSIC = 53
};
# Contains all constants about "apply_standard_effect" function.
enum StandardEffect {
	NONE = 0, BAKED_SPRITE_GLOW2D = 1, BILINEAR_FILTERING2D = 2, BILLBOARD2D = 3, BLUR2D = 4, CHROMATIC_ABERATION2D = 5,
	CLOUD2D = 6, COMPOSE2D = 7, CROSSHAIR2D = 8, DISSOLVE2D = 9, DISTORTION2D = 10, FADE2D = 11, FLAT_OUTLINE2D = 12,
	GAUSSIAN_BLUR2D = 13, GAUSSIAN_BLUR2D_OPTIMIZED = 14, GRADIENT2D = 15, GRADIENT_SHIFT2D = 16, GRAYSCALE2D = 17,
	INLINE2D = 18, INOUTLINE2D = 19, INVERT2D = 20, NEGATIVE2D = 21, OUTLINE2D = 22, PIXELIZE2D = 23,
	PIXEL_OUTLINE2D = 24, POINTILISM2D = 25, PSYCHADELIC2D = 26, REFLECTION2D = 27, SEPIA2D = 28, SHADOW2D = 29,
	SHINY2D = 30, SHOCKWAVE2D = 31, SIMPLE_DISSOLVE2D = 32, SOBEL_EDGE2D = 33, STACKED2D = 34, SWIRL2D = 35,
	VIGNETTE2D = 36, WATER2D = 37, XRAY_MASK2D = 38, ZOOM_BLUR2D = 39, XBRZ2D = 40, SIMPLE_FIRE2D = 41, OMNISCALE2D = 42,
	EASY_BLEND2D = 43, AVANCED_TOON3D = 44, CEL3D = 45, CLOUD3D = 46, COLOR_BLENDED3D = 47, CRISTAL3D = 48, CURVATURE3D = 49,
	DISSOLVE3D = 50, FLEXIBLE_TOON3D = 51, FORCE_FIELD3D = 52, GRADIENT3D = 53, MOSAIC3D = 54, OUTLINE3D = 55, OVERDRAW3D = 56,
	PLANET_ATMOSPHERE3D = 57, PSX_LIT3D = 58, PULSE_GLOW3D = 59, REFRACTION3D = 60, RIM3D = 61, SHOCKWAVE3D = 62,
	SIMPLE_FORCE_FIELD3D = 63, STYLIZED_LIQUID = 64, WIND3D = 65, XRAY_GLOW3D = 66, DEBANDING_MATERIAL3D = 67,
	ADVANCED_DECAL3D = 68, SIMPLE_DECAL3D = 69, SIMPLE_FIRE3D = 70, LIGHT_RAYS3D = 71, LENS_FLARE3D = 72
};
# Contains all constants for modules transformations.
enum Transformation {NONE = 0, LOCATION = 1, ROTATION = 2, SCALE = 3, LOCROT = 4, LOCSCALE = 5, ROTSCALE = 6, ALL = 7};
# Contains all constants about "get_ik_look_at" function.
enum Axis {NONE = 0, X = 1, Y = 2, Z = 3, _X = 4, _Y = 5, _Z = 6, XY = 7, XZ = 8, YZ = 9, _XY = 10, _XZ = 11, _YZ = 12, XYZ = 13, _XYZ = 14};
# Contains module cash.
var __cash__: Dictionary = Dictionary ({occ_data = Array ([[1.0, 1.0], 2.0]), velocity = Vector3.ZERO, queue_methods = Dictionary ({}),
	base_resolution = Vector2 (320, 240), stretch_mode = 0, stretch_aspect = 0, orientation = Transform ()
}) setget _unsetable_var_error;

################################################################################# [Main process] ############################################################################
# Unsetable private variables.
func _unsetable_var_error (_new_value) -> void: self._output ("Can't change value of this variable. Because it private.", self.Message.ERROR);

# ChaChaBlockCipher is a private class that provides some functions for CHACHA encryption method. All functions from this class are private.
class _ChaChaBlockCipher:
	# Attributes.
	var st: Array = Array ([]); var ws: Array = Array ([]); var ky: PoolByteArray = PoolByteArray ([]);
	var _is_private: bool = true setget _private; # Determines whether a method is private.

	# Setter for "is_private" variable.
	func _private (_new_value: bool) -> void: assert (false, "Can't change value of this variable. Because it private.");

	# When this class is instantiated.
	func _init () -> void:
		# Checks method caller source file.
		var stack: Array = get_stack (); if stack [0].source != stack [1].source:
			# Contains the message that will be shown into the console.
			var message: String = (" Because it private::line " + str (stack [1].line) + " from {" + stack [1].source + " -> " + stack [1].function + " ()} file.");
			# Error message.
			assert (false, ('{' + stack [0].function + " ()} method call is denied." + message));
		# No errors found.
		else:
			# Initializes chacha ciphier block.
			st.resize (16); ws.resize (16);
			for i in range (16):
				st [i] = 0; ws [i] = 0; ky.resize (64);

	# Chacha20 block method definition.
	func chacha20_block (a_ky: PoolByteArray, a_iv: PoolByteArray, a_ctr: int, a_rounds: int) -> PoolByteArray:
		# Disables private mode on private functions.
		_is_private = false; assert (st.size () == 16); assert (ws.size () == 16); assert (a_ky.size () == 16 || a_ky.size () == 32);
		assert (a_iv.size () ==  8 || a_iv.size () == 12 || a_iv.size () == 16);
		# Initialises state.
		var s: int = 4;
		if (a_ky.size () == 16):
			# 128 bit key..."expand 16-byte k".
			st [0] = 0x61707865; st [1] = 0x3120646e; st [2] = 0x79622d36; st [3] = 0x6b206574;
			# Inserts 128-bit key twice.
			for i in range (0, 16, 4):
				st [s] = self._u8_to_u32le (a_ky [i], a_ky [(i + 1)], a_ky [(i + 2)], a_ky [(i + 3)]); st [(s + 4)] = st [s]; s += 1;
		else:
			# 256 bit key..."expand 32-byte k".
			st [0] = 0x61707865; st [1] = 0x3320646e; st [2] = 0x79622d32; st [3] = 0x6b206574;
			# Inserts 256-bit key.
			for i in range (0, 32, 4):
				st [s] = self._u8_to_u32le (a_ky [i], a_ky [(i + 1)], a_ky [(i + 2)], a_ky [(i + 3)]); s += 1;
		# Counter & nonce.
		if (a_iv.size () == 8):
			# Original: 64 bit counter, 64 bit nonce.
			st [12] = (a_ctr >> 32) & _B32; st [13] = a_ctr & _B32; s = 14;
			for i in range (0, 8, 4):
				st [s] = self._u8_to_u32le (a_iv [i], a_iv [(i + 1)], a_iv [(i + 2)], a_iv [(i + 3)]); s += 1;
		elif (a_iv.size () == 12):
			# RFC7539: 32 bit counter, 96 bit nonce.
			st [12] = a_ctr & _B32; s = 13;
			for i in range (0, 12, 4):
				st [s] = self._u8_to_u32le (a_iv [i], a_iv [(i + 1)], a_iv [(i + 2)], a_iv [(i + 3)]); s += 1;
		else:
			# PRNG mode: 128 bit nonce, a_ctr is ignored.
			s = 12;
			for i in range (0, 16, 4):
				st [s] = self._u8_to_u32le (a_iv [i], a_iv [(i + 1)], a_iv [(i + 2)], a_iv [(i + 3)]); s += 1;
		# Round loop.
		for i in range (ws.size ()): ws [i] = st [i];
		for _r in range (a_rounds >> 1):
			self._qround (ws, 0, 4, 8, 12); self._qround (ws, 1, 5, 9, 13); self._qround (ws, 2, 6, 10, 14); self._qround (ws, 3, 7, 11, 15);
			self._qround (ws, 0, 5, 10, 15); self._qround (ws, 1, 6, 11, 12); self._qround (ws, 2, 7, 8, 13); self._qround (ws, 3, 4, 9, 14);
		# Creates keystream.
		var ki: int = 0;
		for i in range (st.size ()):
			st [i] = (st [i] + ws [i]) & _B32; ky [ki] = (st [i] & 0xff); ky [(ki + 1)] = ((st [i] >> 8) & 0xff);
			ky [(ki + 2)] = ((st [i] >> 16) & 0xff); ky [(ki + 3)] = ((st [i] >> 24) & 0xff); ki += 4;
		# Enables private mode on private functions.
		_is_private = true; return ky;

	# Rotl method definition.
	func _rotl32 (n: int, r: int) -> int:
		# If the private mode is disabled.
		if !_is_private:
			# Returns the final value.
			n = n & _B32; r = r & 0x1f; if (r == 0): return n; return (((n << r) & _B32) | ((n >> (32 - r)) & _B32)) & _B32;
		# Error message.
		else: assert (false, "{rotl32} method is private."); return 0;

	# Qround method definition.
	func _qround (ST: Array, a: int, b: int, c: int, d: int) -> void:
		# If the private mode is disabled.
		if !_is_private:
			ST [a] = (ST [a] + ST [b]) & _B32; ST [d] ^= ST [a]; ST [d] = self._rotl32 (ST [d], 16);
			ST [c] = (ST [c] + ST [d]) & _B32; ST [b] ^= ST [c]; ST [b] = self._rotl32 (ST [b], 12);
			ST [a] = (ST [a] + ST [b]) & _B32; ST [d] ^= ST [a]; ST [d] = self._rotl32 (ST [d], 8);
			ST [c] = (ST [c] + ST [d]) & _B32; ST [b] ^= ST [c]; ST [b] = self._rotl32 (ST [b], 7);
		# Error message.
		else: assert (false, "{qround} method is private.");

	# U8TOU32LE method definition.
	func _u8_to_u32le (a: int, b: int, c: int, d: int) -> int:
		# If the private mode is disabled.
		if !_is_private: return ((a & 0xff) | ((b & 0xff) << 8) | ((c & 0xff) << 16) | ((d & 0xff) << 24)) & _B32;
		# Error message.
		else: assert (false, "{u8_to_u32le} method is private."); return 0;

# Encrypts a block with AES CBC encryption schema.
static func _encrypt_block (block: Array, keys: Array) -> PoolByteArray:
	# Apply private visibility to this method.
	if apply_visibility (MethodAccessMode.PRIVATE):
		# Initializes the block.
		block = Array ([
			PoolByteArray ([block [0x00], block [0x01], block [0x02], block [0x03]]), PoolByteArray ([block [0x04], block [0x05], block [0x06], block [0x07]]),
			PoolByteArray ([block [0x08], block [0x09], block [0x0a], block [0x0b]]), PoolByteArray ([block [0x0c], block [0x0d], block [0x0e], block [0x0f]])
		]); for x in range (4): for d in range (4): block [x] [d] ^= keys [0] [x] [d];
		# Initializes input encrypting.
		for rnd in range (1, keys.size ()):
			# SubBytes.
			for u in range (4): for t in range (4): block [u] [t] = _S_BOX [(block [u] [t] / 16)] [(block [u] [t] % 16)];
			# ShiftRows.
			for i in range (1, 4):
				# Contains some news rows and generates some row(s).
				var new_row = PoolByteArray ([]); new_row.resize (4); for r in range (4): new_row [r] = block [((r + i) % 4)] [i];
				for e in range (4): block [e] [i] = new_row [e];
			# Mix columns and add round keys.
			if rnd != (len (keys) - 1): _mix_columns (block, _MIX_MATRIX); for i in range (4): for j in range (4): block [i] [j] ^= keys [rnd] [i] [j];
		# Contains the encrypted block.
		var cipher_block: PoolByteArray = block [0]; for i in range (1, 4): cipher_block.append_array (block [i]); return cipher_block;
	# Otherwise.
	else: return PoolByteArray ([]);

# Generates the passed key schedule from a string format (AES encryption).
static func _key_schedule (key: PoolByteArray) -> Array:
	# Apply private visibility to this method.
	if apply_visibility (MethodAccessMode.PRIVATE):
		# Calculates the key width.
		var key_width: int = int (key.size () / 4.0); var rounds: int = (key_width + 6);
		var expansions: float = ceil (4.0 * (rounds + 1) / key_width); var words: Array = Array ([]);
		# Generates a key character.
		for j in range (key_width): words.append (PoolByteArray ([key [(j * 4)], key [(j * 4 + 1)], key [(j * 4 + 2)], key [(j * 4 + 3)]]));
		# Generates words with expansions.
		for m in range (1, expansions):
			# Targets the preview expansion.
			var exp_prev: PoolByteArray = words [(len (words) - key_width)]; var word: PoolByteArray = words [(len (words) - 1)]; var temp: int = word [0];
			# Updates the current word.
			for z in range (3): word [z] = word [(z + 1)]; word [3] = temp;
			for f in range (4): word [f] = (_S_BOX [int (word [f] / 16.0)] [(word [f] % 16)] ^ exp_prev [f]); word [0] ^= _RCI [(m - 1)]; words.append (word);
			for y in range (1, key_width):
				word = words [(len (words) - 1)]; if key_width > 6 and (y % 4) == 0: for k in range (4): word [k] = _S_BOX [int (word [k] / 16.0)] [(word [k] % 16)];
				for k in range (4): word [k] ^= words [(len (words) - key_width)] [k]; words.append (word);
		# Contains the final value of the passed key.
		var keys: Array = Array ([]);
		# Calculates key sequence.
		for g in range (rounds + 1):
			# Generates each word of the passed key.
			var key_rnd: Array = Array ([words [(g * 4)]]); for v in range (1, 4): key_rnd.append (words [(g * 4 + v)]); keys.append (key_rnd);
		# Returns the final result.
		return keys;
	# Otherwise.
	else: return Array ([]);

# Creates a mixed colunm(s) for a block and mix matrix (AES encryption).
static func _mix_columns (block: Array, mix_matrix: Array) -> void:
	# Apply private visibility to this method.
	if apply_visibility (MethodAccessMode.PRIVATE):
		# Starting colunms creation.
		for n in range (4):
			# Contains a column data.
			var new_column: PoolByteArray = PoolByteArray ([]); new_column.resize (4);
			for q in range (4):
				var sum: int = 0;
				for k in range (4):
					var p: int = 0; var blck: int = block [n] [k]; var mtrx: int = mix_matrix [q] [k];
					for _counter in range (8):
						if (mtrx & 1) != 0: p ^= blck; var high_bit_set: bool = (blck & 0x80) != 0;
						blck = ((blck << 1) & 0xff); if high_bit_set: (blck ^= 0x1b); mtrx >>= 1;
					sum ^= p;
				new_column [q] = sum;
			block [n] = new_column;

# Decrypts a block with AES CBC encryption schema.
static func _decrypt_block (block: Array, keys: Array) -> PoolByteArray:
	# Apply private visibility to this method.
	if apply_visibility (MethodAccessMode.PRIVATE):
		# Initializes block.
		block = Array ([
			PoolByteArray ([block [0x00], block [0x01], block [0x02], block [0x03]]), PoolByteArray ([block [0x04], block [0x05], block [0x06], block [0x07]]),
			PoolByteArray ([block [0x08], block [0x09], block [0x0a], block [0x0b]]), PoolByteArray ([block [0x0c], block [0x0d], block [0x0e], block [0x0f]])
		]);
		# Decodes the passed block.
		for rnd in range ((keys.size () - 1), 0, -1):
			# Adds round key.
			for i in range (4): for j in range (4): block [i] [j] ^= keys [rnd] [i] [j];
			# Mix columns.
			if rnd != (keys.size () - 1): _mix_columns (block, _MIX_MATRIX_INVERSE);
			# Shift rows.
			for i in range (1, 4):
				# Initializes a new row.
				var new_row: PoolByteArray = PoolByteArray ([]); new_row.resize (4);
				for j in range (4): new_row [j] = block [((j - i + 4) % 4)] [i]; for j in range (4): block [j] [i] = new_row [j];
			# Sub bytes.
			for i in range (4): for j in range (4): block [i] [j] = _S_BOX_INVERSE [(block [i] [j] / 16)] [(block [i] [j] % 16)];
		# Updates the current block.
		for i in range (4): for j in range (4): block [i] [j] ^= keys [0] [i] [j]; var decrypted_block: PoolByteArray = PoolByteArray (block [0]);
		# Returns the decrypted passed block.
		for i in range (1, 4): decrypted_block.append_array (block [i]); return decrypted_block;
	# Otherwise.
	else: return PoolByteArray ([]);

# This method displays a message on editor console.
static func _output (message: String, type: int = 0) -> void:
	# Apply private visibility to this method.
	if apply_visibility (MethodAccessMode.PROTECTED, PoolStringArray (["Module", "Indestructible"])):
		# Message not empty.
		if !message.empty ():
			# For the normal messages.
			if type <= Message.NORMAL: print (message);
			# For the warning messages.
			elif type == Message.WARNING:
				# Editor state.
				if Engine.editor_hint: push_warning (message);
				# Otherwise.
				else:
					# Prints the given message.
					printerr (message); push_warning (message);
			# For error messages.
			else:
				# If the game is not running.
				if Engine.editor_hint: printerr (message);
				# If the game is running.
				else:
					# Prints the given message.
					printerr (message); push_error (message); assert (false, message);

# Returns all keys data from gamepad name.
static func _check_joy_keys (jname: String) -> Array: return Array ([jname.find ("input") != -1, jname.find ("dual") != -1, jname.find ("ps") != -1,
	jname.find ("shock") != -1, jname.find ("sixaxis") != -1, jname.find ("playstation") != -1, jname.find ("sony") != -1, jname.find ("taiko") != -1,
	jname.find ("drum") != -1, jname.find ('4') != -1, jname.find ("xbox") != -1, jname.find ("360") != -1, jname.find ("one") != -1, jname.find ("microsoft") != -1,
	jname.find ("nintendo") != -1, jname.find ("switch") != -1, jname.find ("wii") != -1, jname.find ("mega") != -1, jname.find ("joy-con") != -1,
	jname.find ("neogeo") != -1, jname.find ("genesis") != -1, jname.find ("drive") != -1, jname.find ("nunchuck") != -1
]) if apply_visibility (MethodAccessMode.PRIVATE) else Array ([]);

# Draws line caps to increase line renderer performance (LineRenderer).
static func _draw_line_caps (current_coords: Vector3, next_coords: Vector3, thickness: float, smoothing: float, cam_trans: Vector3, geometry: ImmediateGeometry) -> void:
	# Apply private visibility to this method.
	if apply_visibility (MethodAccessMode.PRIVATE):
		# Othogonalizes cordinates.
		var orthogonal: Vector3 = ((cam_trans - current_coords).cross (current_coords - next_coords).normalized () * thickness);
		# Calculates point axis.
		var axis: Vector3 = (current_coords - cam_trans).normalized (); var array: PoolVector3Array = PoolVector3Array ([]);
		# Initializes cap array.
		for _s in range (smoothing + 1): array.append (Vector3.ZERO); array [0] = (current_coords + orthogonal); array [smoothing] = (current_coords - orthogonal);
		for n in range (1, smoothing): array [n] = (current_coords + (orthogonal.rotated (axis, lerp (0.0, PI, (float (n) / smoothing)))));
		# Applying the given smoothing.
		for c in range (1, (smoothing + 1)):
			geometry.set_uv (Vector2 (0.0, (c - 1) / smoothing)); geometry.add_vertex (array [(c - 1)]);
			geometry.set_uv (Vector2 (0.0, (c - 1) / smoothing)); geometry.add_vertex (array [c]);
			geometry.set_uv (Vector2 (0.5, 0.5)); geometry.add_vertex (current_coords);

# Draws line corners to increase line renderer performance (LineRenderer).
static func _draw_line_corners (next_coords: Vector3, start: Vector3, end: Vector3, smoothing: float, geometry: ImmediateGeometry) -> void:
	# Apply private visibility to this method.
	if apply_visibility (MethodAccessMode.PRIVATE):
		# Initializes array.
		var array: PoolVector3Array = PoolVector3Array ([]); for _v in range (smoothing + 1): array.append (Vector3.ZERO);
		array [0] = start; array [smoothing] = end; var axis: Vector3 = start.cross (end).normalized ();
		var offset: Vector3 = (start - next_coords); var angle: float = offset.angle_to (end - next_coords);
		# Calculates point axis.
		for k in range (1, smoothing): array [k] = (next_coords + offset.rotated (axis, lerp (0.0, angle, (float (k) / smoothing))));
		# Applying the given smoothing.
		for f in range (1, (smoothing + 1)):
			geometry.set_uv (Vector2 (0.0, ((f - 1) / smoothing))); geometry.add_vertex (array [(f - 1)]);
			geometry.set_uv (Vector2 (0.0, ((f - 1) / smoothing))); geometry.add_vertex (array [f]);
			geometry.set_uv (Vector2 (0.5, 0.5)); geometry.add_vertex (next_coords);

# Apply common line configurations (LineRenderer).
static func _run_line_common_configs (data: Dictionary, old: Dictionary, geometry: ImmediateGeometry, points: Array, object: Spatial) -> Dictionary:
	# Apply private visibility to this method.
	if apply_visibility (MethodAccessMode.PRIVATE):
		# Getting freeze value.
		data.freeze = data.freeze if data.has ("freeze") and data.freeze is int else (Axis.NONE if old.empty () else old.freeze);
		# Getting mesh value.
		data.mesh = data.mesh if data.has ("mesh") and data.mesh is int else (Mesh.PRIMITIVE_TRIANGLES if old.empty () else old.mesh);
		# The mesh value is less than or equal to 0.
		data.mesh = Mesh.PRIMITIVE_POINTS if data.mesh <= Mesh.PRIMITIVE_POINTS else data.mesh;
		# The mesh value is greather than or equal to 6.
		data.mesh = Mesh.PRIMITIVE_TRIANGLE_FAN if data.mesh >= Mesh.PRIMITIVE_TRIANGLE_FAN else data.mesh;
		# Getting skin value.
		data.skin = data.skin if data.has ("skin") else (Color.white if old.empty () else old.skin);
		# The given skin is an instance of a SpatialMaterial or ShaderMaterial.
		if data.skin is SpatialMaterial || data.skin is ShaderMaterial: geometry.material_override = data.skin;
		# Otherwise.
		else:
			# Creating a new insatance of a SpatialMaterial.
			var skin: SpatialMaterial = SpatialMaterial.new (); skin.albedo_color = Color.white;
			# The given skin is a Color.
			skin.albedo_color = data.skin if data.skin is Color else skin.albedo_color;
			# The given skin is a Texture and updates the line material.
			skin.albedo_texture = data.skin if data.skin is Texture else skin.albedo_texture; geometry.material_override = skin;
		# Some points have been passed.
		if not points.empty ():
			# The points size must not be less than 0.
			if points.size () < 2: return data;
			# Otherwise.
			else:
				# Getting geometry width.
				data.width = data.width if data.has ("width") else (0.004 if old.empty () else old.width);
				# Getting smooth.
				data.smooth = data.smooth if data.has ("smooth") else (5.0 if old.empty () else old.smooth); var smooth: Vector2 = Vector2 (5.0, 5.0);
				# The given smooth is a number or a string that contains only numbers.
				if is_number (data.smooth): smooth = Vector2 (float (data.smooth), float (data.smooth));
				# The given smooth is a Vector.
				else:
					# Contains the real vector value.
					var vect = any_to_vector2 (data.smooth); smooth = vect if vect != null else smooth;
				# Getting skin scale.
				data.skinscale = data.skinscale if data.has ("skinscale") and data.skinscale is bool else (false if old.empty () else old.skinscale);
				# Contains the active scene camera.
				var camera = object.get_viewport ().get_camera (); 
				# Getting camera global transform.
				var cam_trans: Vector3 = geometry.to_local (camera.global_transform.origin if camera is Camera else Vector3.ZERO);
				# Calculates the progress step and geometry width.
				var progress_step: float = (1.0 / points.size ()); var progress = 0; var width: Vector2 = Vector2 (0.004, 0.004);
				# The given width is a number or a string that contains only numbers.
				if is_number (data.width): width = Vector2 (float (data.width), float (data.width));
				# The given width is a Vector.
				else:
					# Contains the real vector value.
					var vect = any_to_vector2 (data.width); width = vect if vect != null else width;
				# Updates thickness and next thickness for each points (current and next) in for loop.
				var thickness = lerp (width.x, width.y, progress); var next_thickness = lerp (width.x, width.y, (progress + progress_step));
				# Clears all preview data and initialize line with the current mesh.
				geometry.clear (); geometry.begin (data.mesh);
				# Adding each point to immediate geometry after clearing of the preview data.
				for t in range (points.size () - 1):
					# Contains current point coordinates.
					var current_coords = _get_real_point_cords (data.freeze, points [t], object);
					# Contains the next point coordinates.
					var next_coords = _get_real_point_cords (data.freeze, points [(t + 1)], object);
					# The current coordinates and the next coordinates aren't null.
					if current_coords != null and next_coords != null:
						# Rest coordinates.
						var rest: PoolVector3Array = PoolVector3Array ([(current_coords + next_coords), (next_coords - current_coords)]);
						var ortho_nc: Vector3 = (cam_trans - (rest [0] / 2)).cross (rest [1]).normalized ();
						# Calculates the start and end points coordinates.
						var borders: PoolVector3Array = PoolVector3Array ([(ortho_nc * thickness), (ortho_nc * next_thickness)]);
						var results: PoolVector3Array = PoolVector3Array ([
							(current_coords + borders [0]), (current_coords - borders [0]), (next_coords + borders [1]), (next_coords - borders [1])
						]);
						# Should us draw geometry caps.
						if t == 0: _draw_line_caps (current_coords, next_coords, thickness, smooth.x, cam_trans, geometry);
						# Should us scale geometry texture.
						if data.skinscale:
							# Gets vectors magnitude.
							var magitude: float = rest [1].length ();
							# Calculates texture scale.
							var tex_scale: PoolRealArray = PoolRealArray ([floor (magitude), (magitude - floor (magitude))]);
							geometry.set_uv (Vector2 (tex_scale [0], 0.0)); geometry.add_vertex (rest [0]); geometry.set_uv (Vector2 (-tex_scale [1], 0.0));
							geometry.add_vertex (results [2]); geometry.set_uv (Vector2 (tex_scale [0], 1.0)); geometry.add_vertex (results [1]);
							geometry.set_uv (Vector2 (-tex_scale [1], 0.0)); geometry.add_vertex (results [2]); geometry.set_uv (Vector2 (-tex_scale [1], 1.0));
							geometry.add_vertex (results [3]); geometry.set_uv (Vector2 (tex_scale [0], 1.0)); geometry.add_vertex (results [1]);
						# Otherwise.
						else:
							# Updates geometry UVs and vertex.
							geometry.set_uv (Vector2.RIGHT); geometry.add_vertex (results [0]); geometry.set_uv (Vector2.ZERO);
							geometry.add_vertex (results [2]); geometry.set_uv (Vector2.ONE); geometry.add_vertex (results [1]);
							geometry.set_uv (Vector2.ZERO); geometry.add_vertex (results [2]); geometry.set_uv (Vector2.DOWN);
							geometry.add_vertex (results [3]); geometry.set_uv (Vector2.ONE); geometry.add_vertex (results [1]);
						# Should us draw geometry caps.
						if t == (len (points) - 2): if true: _draw_line_caps (current_coords, next_coords, thickness, smooth.x, cam_trans, geometry);
						# Should us draw corners ?
						else:
							# Contains the next of the next point coordinates.
							var next_next_coords = _get_real_point_cords (data.freeze, points [(t + 2)], object);
							# For not null value.
							if next_next_coords != null:
								# Rest coordinates.
								var dist: PoolVector3Array = PoolVector3Array ([(next_coords + next_next_coords), (next_next_coords - next_coords)]);
								# Calculates corners othogonalization.
								var ortho_nn: Vector3 = ((cam_trans - (dist [0] / 2)).cross (dist [1]).normalized () * next_thickness);
								# Calculates angle dot.
								var angle_dot: float = rest [1].dot (ortho_nn);
								# The angle dot is greather than 0.
								if angle_dot > 0.0: _draw_line_corners (next_coords, results [2], (next_coords + ortho_nn), smooth.y, geometry);
								# Otherwise.
								else: _draw_line_corners (next_coords, (next_coords - ortho_nn), results [3], smooth.y, geometry);
						# Updates progress and calculate the tickness and the next tickness for the next points.
						progress += progress_step; thickness = lerp (width.x, width.y, progress); next_thickness = lerp (width.x, width.y, (progress + progress_step));
				# Ends the drawing sequence.
				geometry.end ();
		# Returns the final value.
		return data;
	# Otherwise.
	else: return Dictionary ({});

# Searches an found the real point coordinates with developper constraints (LineRenderer).
static func _get_real_point_cords (freeze: int, coords, object: Spatial):
	# Apply private visibility to this method.
	if apply_visibility (MethodAccessMode.PRIVATE):
		# Getting the real node.
		coords = object.get_node_or_null (coords) if coords is String or coords is NodePath else coords;
		# For a Spatial node.
		if coords is Spatial:
			var x: float = 0.0 if freeze == Axis.X || freeze == Axis.XY || freeze == Axis.XZ || freeze == Axis.XYZ else coords.global_transform.origin.x;
			var y: float = 0.0 if freeze == Axis.Y || freeze == Axis.XY || freeze == Axis.YZ || freeze == Axis.XYZ else coords.global_transform.origin.y;
			var z: float = 0.0 if freeze == Axis.Z || freeze == Axis.XZ || freeze == Axis.YZ || freeze == Axis.XYZ else coords.global_transform.origin.z;
			coords = Vector3 (x, y, z); return coords;
		# For a Vector2.
		elif coords is Vector2:
			var x: float = 0.0 if freeze == Axis.X || freeze == Axis.XY || freeze == Axis.XZ || freeze == Axis.XYZ else coords.x;
			var y: float = 0.0 if freeze == Axis.Y || freeze == Axis.XY || freeze == Axis.YZ || freeze == Axis.XYZ else coords.y;
			coords = Vector3 (x, y, 0.0); return coords;
		# For a Vector3.
		elif coords is Vector3:
			var x: float = 0.0 if freeze == Axis.X || freeze == Axis.XY || freeze == Axis.XZ || freeze == Axis.XYZ else coords.x;
			var y: float = 0.0 if freeze == Axis.Y || freeze == Axis.XY || freeze == Axis.YZ || freeze == Axis.XYZ else coords.y;
			var z: float = 0.0 if freeze == Axis.Z || freeze == Axis.XZ || freeze == Axis.YZ || freeze == Axis.XYZ else coords.z;
			coords = Vector3 (x, y, z); return coords;
		# Returns a null value.
		else: return null;
	# Otherwise.
	else: return null;

# Apply common line configurations when using a raycast (LineRenderer).
static func _run_line_ray_common_configs (data: Dictionary, ray: RayCast, geometry: ImmediateGeometry, object: Node) -> void:
	# Apply private visibility to this method.
	if apply_visibility (MethodAccessMode.PRIVATE):
		# Getting all given actions and converting theirs into an array.
		data.actions = data.actions if data.has ("actions") else Array ([]); data.actions = Array ([data.actions]) if !is_array (data.actions) else Array (data.actions);
		# Checks destroy key.
		run_slots (data.actions, object); data.destroy = data.destroy if data.has ("destroy") and data.destroy is bool else true;
		# Disables all existing imported objects.
		for child in geometry.get_children ():
			# Checks destroy key.
			if data.destroy: if ray == null: child.queue_free ();
			# Otherwise.
			else:
				# Hides imported the current and change his translation to geometry translation.
				child.visible = false; child.transform.origin = Vector3.ZERO;
		# The ray parameter is not null.
		if ray != null:
			# Getting all objects impact.
			data.impact = data.impact if data.has ("impact") else Array ([]); var impact_point: Vector3 = ray.get_collision_point ();
			# Converting impact into an array.
			data.impact = Array ([data.impact]) if !is_array (data.impact) else Array (data.impact);
			# Affects all availables objects impact.
			for obj in data.impact:
				# The object impact is a NodePath or String.
				if obj is NodePath or obj is String:
					# Getting the target node.
					var node = object.get_node_or_null (NodePath (obj)); obj = String (obj);
					# The given node is correct.
					if node is Spatial:
						# Contains the object parent.
						var node_parent: Node = node.get_parent ();
						# The parent node is it a instance of a Spatial node ?
						if node_parent is Spatial:
							# Updates object local transformation.
							node.transform.origin = node_parent.to_local (impact_point);
						# Otherwise.
						else: _output (("The parent of {" + node.name + "} isn't an instance of Spatial class."), Message.ERROR);
					# The given node is a prefab.
					elif obj.ends_with (".tscn") || obj.ends_with (".scn"):
						# Generates the passed object id.
						var id: String = obj.get_file ().split ('.') [0].lstrip (' ').rstrip (' ');
						# Checks whether this object is already a child of the line.
						var child = geometry.get_node_or_null (id); impact_point = geometry.to_local (impact_point);
						# Undefined node.
						if child == null:
							# Loads object.
							instanciate (Dictionary ({parent = geometry.get_path (), background = false, path = obj, position = impact_point}), object, -1.0);
							# Updates the child node to check whether the target object is already loaded correctly.
							child = geometry.get_node_or_null (id);
						# The child must be an instance of a Spatial node.
						if child is Spatial:
							# Enables child visibility and update his translation.
							child.visible = true; child.translation = impact_point;
						# Otherwise.
						else: _output (("{" + child.name + "} should be an instance of Spatial class."), Message.ERROR);

# Destroys the drew line after a certain time (LineRenderer).
static func _destroy_geometry_after_live (live: float, geometry: ImmediateGeometry, object: Node) -> void:
	# Apply private visibility to this method.
	if apply_visibility (MethodAccessMode.PRIVATE) and live > 0.0:
		# Destroying line object after live and checks whether the geometry instance is valid.
		yield (object.get_tree ().create_timer (live), "timeout"); if is_instance_valid (geometry): geometry.queue_free ();

# Manages methods inner callbacks.
static func _callback_manager (data: Dictionary, parameters: Array, error: bool, object: Node):
	# Apply private visibility to this method.
	if apply_visibility (MethodAccessMode.PRIVATE):
		# Data size is not empty.
		if not data.empty ():
			# Getting callback name.
			data.method = data.method if data.has ("method") and data.method is String else '';
			# Getting callback source.
			data.source = data.source if data.has ("source") else object.get_path (); data.source = object.get_node_or_null (NodePath (str (data.source)));
			# Calls the given callback using MegaAssets run_slot method.
			return invoke (data.method, parameters, (data.source if data.source is Node else object));
		# Otherwise.
		else:
			# Custom message.
			if error:
				# Gets the last parameter element.
				var last: Dictionary = parameters [(len (parameters) - 1)]; _output (last.message, last.type);
			# Returns a null value.
			return null;
	# Otherwise.
	else: return null;

# Converts a boolean value into a bit type. Eg: binary, hex, etc...
static func _bool_to_bit (boolean: bool, bit: int, security: bool = true) -> String:
	# Returns the final value.
	return _string_to_bit (str (boolean), bit, security) if apply_visibility (MethodAccessMode.PRIVATE) else "Null";

# Returns a normal boolean value from his coded value.
static func _bool_from_bit (coded_value: String, bit: int, security: bool = true) -> bool:
	# Returns the final value.
	return (true if _string_from_bit (coded_value, bit, security) == "True" else false) if apply_visibility (MethodAccessMode.PRIVATE) else false;

# Converts an integer value into a bit type. Eg: binary, hex, etc...
static func _int_to_bit (integer: int, bit: int, security: bool = true) -> String:
	# Apply private visibility to this method.
	if apply_visibility (MethodAccessMode.PRIVATE):
		# If the user bit is not less than 2.
		if bit > 1:
			# Contains the sign the of the passed integer, the list of the all transformed string characters and the last value of bit determination.
			var is_neg: String = '1' if integer < 0 else '0'; integer = int (abs (integer)); randomize (); var coded_value: String = ''; var result: int = -1;
			# Converting the integer into the given bit.
			while result != 0:
				# Contains the rest of the division.
				var rest: String = str (fmod (result, bit) if result != -1 else fmod (integer, bit));
				# Filtering the rest value.
				rest = str (get_hex_symbol (int (rest), (true if randi () % 2 == 0 else false)) if bit == 16 else rest);
				# Updates the result value.
				if result == -1: result = int (float (integer) / bit); else: result = int (float (result) / bit);
				# If the security is actived.
				if security: coded_value += rest;
				# Otherwise.
				else:
					# If the byte size is less than 0.
					if coded_value.empty (): coded_value += rest; else: coded_value = coded_value.insert (0, rest);
			# Returns the final value.
			return (is_neg + 'b' + coded_value) if bit == 2 else (is_neg + 'x' + coded_value);
		# Warning message.
		else: _output ("The value of the bit parameter mustn't less than 2.", Message.WARNING);
	# Returns a nullable value for others cases.
	return "Null";

# Returns a normal integer value from his coded value.
static func _int_from_bit (coded_value: String, bit: int, security: bool = true) -> int:
	# Apply private visibility to this method.
	if apply_visibility (MethodAccessMode.PRIVATE):
		# Corrects the coded value.
		coded_value = str_replace (coded_value, Array (["\n", "\t", "\a", "\b", "\r", "\v", "\f", ' ', '(', ')', '[', ']', '{', '}', ',', '.', ';', '_', '-']), '');
		# If the coded text is correct.
		if coded_value != null and not coded_value.empty ():
			# Filtering the coded value.
			var cod_val: PoolStringArray = coded_value.split ('b') if bit == 2 else coded_value.split ('x'); var decrypted_value: int = 0;
			# Checks coded value division size.
			if cod_val.size () == 2 and !cod_val [0].empty () and !cod_val [1].empty ():
				# Converting the other bits into 10 bits.
				for ex in cod_val [1].length ():
					# Checks the security.
					if security: decrypted_value += int (int (get_int_from_hex (cod_val [1] [ex])) * pow (bit, ex));
					# Otherwise.
					else: decrypted_value += int (int (get_int_from_hex (cod_val [1] [((ex + 1) * -1)])) * pow (bit, ex));
				# Returns the final value.
				return decrypted_value if cod_val [0] == '0' else (decrypted_value * -1);
			# Otherwise.
			else: return decrypted_value;
	# Returns a nullable value.
	return 0;

# Converts a string value into a bit type. Eg: binary, hex, etc...
static func _string_to_bit (text: String, bit: int, security: bool = true) -> String:
	# Apply private visibility to this method.
	if apply_visibility (MethodAccessMode.PRIVATE):
		# If the user string is not null and not empty.
		if text != null and not text.empty ():
			# Contains the list of the all transformed string characters.
			var coded_value: String = String ('');
			# Transforming each byte into the given bit.
			for c in text.length ():
				# Contains the final result of the converting.
				var coded_byte: String = _int_to_bit (ord (text [c]), bit, security);
				# If the security is actived.
				if security: coded_value = coded_value.insert (0, (coded_byte if c == 0 else (coded_byte + ' ')));
				# Otherwise.
				else: coded_value += (coded_byte if c == 0 else (' ' + coded_byte));
				# Returns the final value.
			return coded_value;
	# Returns an empty value.
	return "Null";

# Returns a normal string value from his coded value.
static func _string_from_bit (coded_value: String, bit: int, security: bool = true) -> String:
	# Apply private visibility to this method.
	if apply_visibility (MethodAccessMode.PRIVATE):
		# If the coded text is not empty.
		if coded_value != null and not coded_value.empty ():
			# Contains the encryted value of the given string and the decrypted string from the decrypted bytes values.
			var coded_str: PoolStringArray = PoolStringArray ([]); var decrypted_str: String = String ('');
			# If the character count of coded text is great than 1 and have a space.
			coded_str = coded_value.split (' ') if len (coded_value) > 1 && coded_value.find (' ') != -1 else PoolStringArray ([coded_value]);
			# Transforming each character of the encrypted value to normal.
			for code in coded_str:
				# If the security is actived.
				if security: decrypted_str = decrypted_str.insert (0, char (_int_from_bit (code, bit, security)));
				# Otherwise.
				else: decrypted_str += char (_int_from_bit (code, bit, security));
			# Returns the final value.
			return decrypted_str;
	# Returns a fake value.
	return "Null";

# Converts a float value into a bit type. Eg: binary, hex, etc...
static func _float_to_bit (real: float, bit: int, security: bool = true) -> String:
	# Apply private visibility to this method.
	if apply_visibility (MethodAccessMode.PRIVATE):
		# Contains a string format of the given real.
		var stringify_real: String = str (real);
		# If the float value doesn't contain a double.
		if stringify_real.find ('.') == -1: return _int_to_bit (int (real), bit, security);
		# Otherwise.
		else:
			# Contains each number of the given real.
			var digits: PoolStringArray = stringify_real.split ('.');
			# Returns the final value.
			return (_int_to_bit (int (digits [0]), bit, security) + '.' + _int_to_bit (int (digits [1]), bit, security));
	# Otherwise.
	else: return "Null";

# Returns a normal float value from his coded value.
static func _float_from_bit (coded_value: String, bit: int, security: bool = true) -> float:
	# Apply private visibility to this method.
	if apply_visibility (MethodAccessMode.PRIVATE):
		# Corrects the coded value.
		coded_value = coded_value.lstrip ('.').rstrip ('.');
		# If the code text is not empty.
		if coded_value != null and not coded_value.empty ():
			# If the float value doestn't contain a double.
			if coded_value.find ('.') == -1: return float (_int_from_bit (coded_value, bit, security));
			# Otherwise.
			else:
				# Contains each number of the given real.
				var digits: PoolStringArray = coded_value.split ('.');
				# Returns the final value.
				return float (str (_int_from_bit (digits [0], bit, security)) + '.' + str (_int_from_bit (digits [1], bit, security)));
	# Returns a fake value.
	return 0.0;

# Converts a Vector2 into a bit type. Eg: binary, hex, etc...
static func _vector2_to_bit (vec2: Vector2, bit: int, security: bool = true) -> String:
	# Returns the final value.
	return ('(' + _float_to_bit (vec2.x, bit, security) + ", " + _float_to_bit (vec2.y, bit, security) + ')') if apply_visibility (MethodAccessMode.PRIVATE) else "Null";

# Returns a normal Vector2 from his coded value.
static func _vector2_from_bit (coded_value: String, bit: int, security: bool = true) -> Vector2:
	# Apply private visibility to this method.
	if apply_visibility (MethodAccessMode.PRIVATE):
		# Removes a certain characters.
		coded_value = coded_value.lstrip ('(').rstrip (')').replace (' ', '');
		# If the coded value is not empty.
		if coded_value != null and !coded_value.empty () and coded_value.find (',') != -1:
			# Contains all sequence values about the given vector2.
			var vect_values: PoolStringArray = coded_value.split (',');
			# Returns the final value.
			return Vector2 (_float_from_bit (vect_values [0], bit, security), _float_from_bit (vect_values [1], bit, security));
	# Returns a fake value.
	return Vector2.ZERO;

# Converts a Vector3 into a bit type. Eg: binary, hex, etc...
static func _vector3_to_bit (vec3: Vector3, bit: int, security: bool = true) -> String:
	# Returns the final value.
	return ('(' + _float_to_bit (vec3.x, bit, security) + ", " + _float_to_bit (vec3.y, bit, security) + ", " + _float_to_bit (vec3.z, bit, security) + ')')\
		if apply_visibility (MethodAccessMode.PRIVATE) else "Null";

# Returns a normal Vector3 from his coded value.
static func _vector3_from_bit (coded_value: String, bit: int, security: bool = true) -> Vector3:
	# Apply private visibility to this method.
	if apply_visibility (MethodAccessMode.PRIVATE):
		# Removes all left and right spaces.
		coded_value = coded_value.lstrip ('(').rstrip (')').replace (' ', '');
		# If the coded value is not empty.
		if coded_value != null and !coded_value.empty () and coded_value.find (',') != -1:
			# Contains all sequence values about the given vector3, values that will be returned.
			var vect_values: PoolStringArray = coded_value.split (','); var vector3: Vector3 = Vector3 (0.0, 0.0, 0.0);
			# Updates the first and the second value.
			vector3 [0] = _float_from_bit (vect_values [0], bit, security); vector3 [1] = _float_from_bit (vect_values [1], bit, security);
			# Updates the third value and returns it.
			if 2 < len (vect_values): vector3 [2] = _float_from_bit (vect_values [2], bit, security); return vector3;
	# Returns a fake value.
	return Vector3.ZERO;

# Converts an array values into a bit type. Eg: binary, hex, etc...
static func _list_to_bit (list: Array, bit: int, security: bool = true) -> Array:
	# Apply private visibility to this method.
	if apply_visibility (MethodAccessMode.PRIVATE):
		# Encrypting all value found in the given list.
		var coded_list: Array = Array ([]); for item in list:
			# The current item is not an array.
			if not is_array (item):
				# The current value isn't a dictionary.
				if not item is Dictionary:
					# If the key is a character and updates the coded list.
					if item is String and item.length () == 1: item += ' '; coded_list.append (_type_to_bit (item, bit, security));
				# Otlerwise.
				else: coded_list.append (_dic_to_bit (item, bit, security));
			# Otherwise.
			else: coded_list.append (_list_to_bit (Array (item), bit, security));
		# Returns the final value.
		return coded_list;
	# Otherwise.
	else: return Array ([]);

# Returns a normal array from his coded value.
static func _list_from_bit (coded_list: Array, bit: int, security: bool = true) -> Array:
	# Apply private visibility to this method.
	if apply_visibility (MethodAccessMode.PRIVATE):
		# Decrypting the list.
		var real_list: Array = Array ([]); for item in coded_list:
			# The current item is not an array.
			if not item is Array:
				# The current item isn't a dictionary.
				if not item is Dictionary: real_list.append (_type_from_bit (item, bit, security)); else: real_list.append (_dic_from_bit (item, bit, security));
			# Otherwise.
			else: real_list.append (_list_from_bit (item, bit, security));
		# Returns the final result.
		return real_list;
	# Returns an empty array.
	else: return Array ([]);

# Converts a dictionary into a bit type. Eg: binary, hex, etc...
static func _dic_to_bit (dic: Dictionary, bit: int, security: bool = true) -> Dictionary:
	# Apply private visibility to this method.
	if apply_visibility (MethodAccessMode.PRIVATE):
		# Encrypting each key and value.
		var coded_dic: Dictionary = Dictionary ({}); for key in dic.keys ():
			# Contains coded key and value.
			var encp_val = null; var encp_key = null;
			# The current value is not an array.
			if not is_array (dic [key]):
				# The current value isn't a dictionary.
				if not dic [key] is Dictionary:
					# If the value is a character and updates the coded dictionary.
					if dic [key] is String and dic [key].length () == 1: dic [key] += ' '; encp_val = _type_to_bit (dic [key], bit, security);
				# Otherwise.
				else: encp_val = _dic_to_bit (dic [key], bit, security);
			# Otherwise.
			else: encp_val = _list_to_bit (Array (dic [key]), bit, security);
			# The current key is not an array.
			if not is_array (key):
				# The current key isn't a dictionary.
				if not key is Dictionary:
					# If the key is a character and updates the coded dictionary.
					if key is String and key.length () == 1: key += ' '; encp_key = _type_to_bit (key, bit, security);
				# Otherwise.
				else: encp_key = _dic_to_bit (key, bit, security);
			# Updates the encrypted dic.
			else: encp_key = _list_to_bit (Array (key), bit, security); coded_dic [encp_key] = encp_val;
		# Returns the final value.
		return coded_dic;
	# Otherwise.
	else: return Dictionary ({});

# Returns a normal dictionary from his coded value.
static func _dic_from_bit (coded_dic: Dictionary, bit: int, security: bool = true) -> Dictionary:
	# Apply private visibility to this method.
	if apply_visibility (MethodAccessMode.PRIVATE):
		# Encrypting each key and value.
		var real_dic: Dictionary = Dictionary ({}); for key in coded_dic.keys ():
			# Contains decoded key and value.
			var decp_val = null; var decp_key = null;
			# If the value of the current key is Array.
			if coded_dic [key] is Array: decp_val = _list_from_bit (coded_dic [key], bit, security);
			# Otherwise.
			else:
				# The current value is a Dictionary.
				if coded_dic [key] is Dictionary: decp_val = _dic_from_bit (coded_dic [key], bit, security);
				# Otherwise.
				else: decp_val = _type_from_bit (coded_dic [key], bit, security);
			# If the current key is Array.
			if key is Array: decp_key = _list_from_bit (key, bit, security);
			# Otherwise.
			else:
				# The current key is a Dictionary.
				if key is Dictionary: decp_key = _dic_from_bit (key, bit, security); else: decp_key = _type_from_bit (key, bit, security);
			# Updates the decrypted dictionary.
			real_dic [decp_key] = decp_val;
		# Returns the final result.
		return real_dic;
	# Returns an empty dictionary.
	else: return Dictionary ({});

# Converts any type into a bit type. Eg: binary, hex, etc... Pay attention ! the supported types are: Integer, Float, String, Vector2 and Vector3.
static func _type_to_bit (type, bit: int, security: bool = true) -> String:
	# Apply private visibility to this method.
	if apply_visibility (MethodAccessMode.PRIVATE):
		# For int and float types value.
		if type is int: return _int_to_bit (type, bit, security); elif type is float: return _float_to_bit (type, bit, security);
		# For string and boolean types value.
		elif type is String: return _string_to_bit (type, bit, security); elif type is bool: return _bool_to_bit (type, bit, security);
		# For vector2 and vector3 types value.
		elif type is Vector2: return _vector2_to_bit (type, bit, security); elif type is Vector3: return _vector3_to_bit (type, bit, security);
		# For other type.
		else: return _string_to_bit (str (weakref (type)), bit, security);
	# Otherwise.
	else: return "Null";

# Returns a normal value of any type from his coded value. Pay attention ! the supported types are: Integer, Float, String, Vector2 and Vector3.
static func _type_from_bit (coded_type: String, bit: int, security: bool = true):
	# Apply private visibility to this method.
	if apply_visibility (MethodAccessMode.PRIVATE):
		# For vector type.
		if coded_type.begins_with ('(') and coded_type.ends_with (')') and coded_type.find (',') != -1:
			# Getting the length for each item.
			var size: int = len (coded_type.split (", "));
			# For vector2.
			if size == 2: return _vector2_from_bit (coded_type, bit, security); else: return _vector3_from_bit (coded_type, bit, security);
		# For float type.
		elif coded_type.find ('.') != -1: return _float_from_bit (coded_type, bit, security);
		# For an integer.
		elif coded_type.find ('.') == -1 and coded_type.find (' ') == -1: return _int_from_bit (coded_type, bit, security);
		# For a string.
		else:
			# Contains the decrypted value as string from his coded value.
			var decrpt_str: String = _string_from_bit (coded_type, bit, security);
			# For boolean type.
			if decrpt_str == "True" or decrpt_str == "False": return (true if decrpt_str == "True" else false);
			# For a character or string.
			elif decrpt_str.length () == 2: return decrpt_str.rstrip (' '); else: return decrpt_str;
	# Otherwise.
	else: return null;

# Manages some events as a sinple methods call.
static func _event_manger (data: Dictionary, node: Node) -> void:
	# The current data has "event" key.
	if apply_visibility (MethodAccessMode.PRIVATE) and data.has ("event") and data.event is String and node.has_method (data.event):
		# Getting user delay.
		data.delay = float (data.delay) if data.has ("delay") && is_number (data.delay) else 0.0;
		# Waiting for user delay.
		if data.delay > 0.0 and !Engine.editor_hint: yield (node.get_tree ().create_timer (data.delay), "timeout");
		# Corrects the given parameters.
		data.params = (Array (data.params) if is_array (data.params) else Array ([data.params])) if data.has ("params") else Array ([]);
		# Calls the current event on the current node as a function.
		node.callv (data.event, data.params.duplicate ());

# Sets value of a given id on a dictionary or an array.
static func _set_id_val (id, value, input, index: int = -1, typed: bool = false, rec: bool = true, _pos: int = -1) -> Array:
	# Apply private visibility to this method.
	if apply_visibility (MethodAccessMode.PRIVATE):
		# Updates occurrence value.
		var occ: int = _pos if _pos != -1 else -1;
		# The given input is an array.
		if input is Array:
			# Searches all availables dictionaries from array.
			for elemt in input:
				# For an array or dictionary.
				if elemt is Dictionary or elemt is Array:
					# Contains the result of this method called in other process.
					var result = _set_id_val (id, value, elemt, index, typed, true, (-1 if index < 0 else occ));
					# The passed index is greather than 0.
					if index >= 0:
						# Updates the current element to the given result.
						elemt = result [0]; occ = result [1]; if occ == index: break;
					# Updates the current element value.
					else: elemt = result [0];
			# Returns the final result.
			return Array ([input, occ]);
		# The given input is a dictionary.
		elif input is Dictionary:
			# Searches the given id.
			for elmt in input:
				# The given id is found.
				if typeof (id) == typeof (elmt) && id == elmt:
					# The passed index is less than 0.
					if index < 0: input [elmt] = value if !typed || typed and typeof (input [elmt]) == typeof (value) else input [elmt];
					# Otherwise.
					else:
						# Increases the occurence value before any treatments.
						occ += 1;
						# The current occurence is equal to the given index.
						if occ == index:
							# Updates the current element value.
							input [elmt] = value if !typed || typed and typeof (input [elmt]) == typeof (value) else input [elmt]; break;
				# Is it recursive ?
				elif rec:
					# Checks the value of the current element.
					if input [elmt] is Dictionary || input [elmt] is Array:
						# Contains the result of this method called in other process.
						var result = _set_id_val (id, value, input [elmt].duplicate (), index, typed, true, (-1 if index < 0 else occ));
						# The passed index is greather than 0.
						if index >= 0:
							# Updates the current element to the given result.
							input [elmt] = result [0]; occ = result [1]; if occ == index: break;
						# Updates the current element value.
						else: input [elmt] = result [0];
			# Returns the modified input.
			return Array ([input, occ]);
		# Returns the given input with no modifications.
		else: return Array ([input, occ]);
	# Otherwise.
	else: return Array ([]);

# Corrects a root motion application to a KinematicBody.
func _root_motion_manager (data: Dictionary, t1: float, t2: float, t3: float, r1: float, r2: float, r3: float, r4: float) -> Transform:
	# Apply private visibility to this method.
	if apply_visibility (self.MethodAccessMode.PRIVATE):
		# Contains root motion location and rotation.
		var origin: Vector3 = Vector3.ZERO; var quat: Vector3 = Vector3.ZERO; var transform: Transform = Transform ();
		# Root motion translation on x, y and z axis.
		origin.x = t1 if data.trdir == Axis.X || data.trdir == Axis.XY || data.trdir == Axis.XZ || data.trdir == Axis.XYZ else 0.0;
		origin.x = -t1 if data.trdir == Axis._X || data.trdir == Axis._XY || data.trdir == Axis._XZ || data.trdir == Axis._XYZ else origin.x;
		origin.y = t2 if data.trdir == Axis.Y || data.trdir == Axis.XY || data.trdir == Axis.YZ || data.trdir == Axis.XYZ else 0.0;
		origin.y = -t2 if data.trdir == Axis._Y || data.trdir == Axis._XY || data.trdir == Axis._YZ || data.trdir == Axis._XYZ else origin.y;
		origin.z = t3 if data.trdir == Axis.Z || data.trdir == Axis.XZ || data.trdir == Axis.YZ || data.trdir == Axis.XYZ else 0.0;
		origin.z = -t3 if data.trdir == Axis._Z || data.trdir == Axis._XZ || data.trdir == Axis._YZ || data.trdir == Axis._XYZ else origin.z;
		# Root motion rotation on x, y and z axis.
		quat.x = r1 if data.rtdir == Axis.X || data.rtdir == Axis.XY || data.rtdir == Axis.XZ || data.rtdir == Axis.XYZ else 0.0;
		quat.x = -r1 if data.rtdir == Axis._X || data.rtdir == Axis._XY || data.rtdir == Axis._XZ || data.rtdir == Axis._XYZ else quat.x;
		quat.y = r2 if data.rtdir == Axis.Y || data.rtdir == Axis.XY || data.rtdir == Axis.YZ || data.rtdir == Axis.XYZ else 0.0;
		quat.y = -r2 if data.rtdir == Axis._Y || data.rtdir == Axis._XY || data.rtdir == Axis._YZ || data.rtdir == Axis._XYZ else quat.y;
		quat.z = r3 if data.rtdir == Axis.Z || data.rtdir == Axis.XZ || data.rtdir == Axis.YZ || data.rtdir == Axis.XYZ else 0.0;
		quat.z = -r3 if data.rtdir == Axis._Z || data.rtdir == Axis._XZ || data.rtdir == Axis._YZ || data.rtdir == Axis._XYZ else quat.z;
		# Creating a new transform, initializes it and returns the final result.
		transform.origin = origin; transform.basis = Basis (Quat (-quat.x, -quat.y, -quat.z, r4)); return transform;
	# Returns an initialize value.
	else: return Transform ();

# This method is called on game initialization and before all nodes instanciation.
func _init () -> void:
	# Initializes librairy backend tasks.
	self.set_physics_process_internal (false); self.set_physics_process (false); self.set_block_signals (false); self.set_display_folded (false);
	self.set_process_unhandled_input (false); self.set_process_unhandled_key_input (false); self.set_scene_instance_load_placeholder (false);
	self.set_message_translation (false); self.set_process (false); self.set_process_input (false); self.set_process_internal (false);

# Called before ready method run.
func _enter_tree ():
	# The current class name is it a script of Godot Mega Assets framework core ?
	if self.get_class () in (self._SCRIPTS_CORE + PoolStringArray (["Module"])):
		# Error message about script core using.
		self._output ("You cannot instanciate this script because it's a base class for Godot Mega Assets modules.", self.Message.ERROR); self.queue_free ();

# Virtual method which can be overridden to customize the return value of to_string method.
func _to_string () -> String: return (self.get_class () + " [" + self.name + ':' + String (self.get_instance_id ()) + "]");

# Returns a base class name.
func _get_class_name () -> String: return "MagaAssets" if apply_visibility (self.MethodAccessMode.PRIVATE) else "Null";

############################################################################## [Available features] #########################################################################
"""
	@Description: Returns cash data.
	@Parameters:
		bool json: Do you want to get data as json format ?
"""
func get_cash (json: bool = false):
	# Returns cash data.
	if json: return JSON.print (self.__cash__, "\t"); else: return self.__cash__;

"""
	@Description: Determinates whether a/some node(s) is/are child/ren of other(s) node(s).
	@Parameters:
		Node | Array nodes: Contains the target node(s). It's the first node(s).
		Node | Array others: Contains the second node(s).
		bool recursive: Do you want to use a recursive program to check whether a node is a child of other node ?
"""
static func is_child_of (nodes, others, recursive: bool = true) -> bool:
	# Converts "nodes" and "others" parameters to an array.
	nodes = Array ([nodes]) if not nodes is Array else nodes; others = Array ([others]) if not others is Array else others;
	# Checks parameters sizes.
	if nodes.size () > 0 and others.size () > 0:
		# Searches parent(s) of the given node(s) on other(s) node(s).
		for node in nodes:
			# Is it a Node ?
			if node is Node:
				# Checks items in "others".
				for item in others:
					# Is it a child of the given parent ?
					if !recursive and node.get_parent () != item: return false;
					# Otherwise.
					elif recursive:
						# Contains the node parent.
						var parent = node.get_parent (); var found: bool = false;
						# While the current parent is a Node.
						while parent is Node:
							# The current is the given parent.
							if parent == item:
								# I found a parent and break the while loop.
								found = true; break;
							# Gets the parent of the current parent.
							parent = parent.get_parent ();
						# No parent found.
						if !found: return false;
			# Otherwise.
			else: return false;
		# All items are a valid nodes.
		return true;
	# Otherwise.
	else: return false;

"""
	@Description: Prints a message on the editor console. This message can be a simple, a warning or an error message.
	@Parameters:
		String message: Contains the message that will be printed on the editor console.
		int type: Contains the type of the given message. The availables values are:
			-> MegaAssets.Message.NORMAL or 0: Simple message.
			-> MegaAssets.Message.WARNING or 1: Warning message.
			-> MegaAssets.Message.ERROR or 2: Error message.
		Node object: Which knot will be considered to make the differents operations ?
		float delay: What is the timeout before printing the given message ?
"""
static func output (message: String, type: int, object: Node, delay: float = 0.0) -> void:
	# Removes left and right spaces.
	message = (get_object_prefix (object) + ": " + message.lstrip (' ').rstrip (' '));
	# Message not empty.
	if !message.empty ():
		# Waiting for user delay.
		if !Engine.editor_hint && delay > 0.0: yield (object.get_tree ().create_timer (delay), "timeout");
		# For the normal messages.
		if type <= Message.NORMAL: print (message);
		# For the warning messages.
		elif type == Message.WARNING:
			# Editor state.
			if Engine.editor_hint: push_warning (message);
			# Otherwise.
			else:
				# Prints the given message.
				printerr (message); push_warning (message);
		# For error messages.
		else:
			# If the game is not running.
			if Engine.editor_hint: printerr (message);
			# If the game is running.
			else:
				# Prints the given message.
				printerr (message); push_error (message); assert (false, message);

"""
	@Description: Determinates whether a/some node(s) is/are child/ren of other node.
	@Parameters:
		Node | Array nodes: Contains the target node(s).
"""
static func is_child (nodes) -> bool:
	# Converts "nodes" parameter to an array and determinates whether the given node(s) is/are a/some child/ren.
	nodes = Array ([nodes]) if not nodes is Array else nodes; for node in nodes: if !node is Node or !node.get_parent () is Node: return false; return true;

"""
	@Description: Determinates whether a/some node(s) is/are a/some parent(s).
	@Parameters:
		Node | Array nodes: Contains the target node(s).
"""
static func is_parent (nodes) -> bool:
	# Converts "nodes" parameter to an array and determinates whether the given node(s) is/are a/some child/ren.
	nodes = Array ([nodes]) if not nodes is Array else nodes; for node in nodes: if not node is Node or node.get_child_count () == 0: return false; return true;

"""@Description: Determinates whether the game is initialised."""
static func is_game_initialised () -> bool: return Engine.get_idle_frames () > 0;

"""
	@Description: Changes parent(s) of the given node(s).
	@Parameters:
		NodePath | String | PoolStringArray nodes: Contains the node(s) path(s) that has/have a parent.
		NodePath | String | PoolStringArray nparents: Contains the new parent(s) path(s) of the target node(s).
		Node object: Which knot will be considered to make the differents operations ?
		bool reversed: Do you want to inverse treatment ?
		float delay: What is the timeout before changing parent of the given node(s) ?
		float interval: What is the timeout before each change ?
"""
static func re_parent (nodes, nparents, object: Node, reversed: bool = false, delay: float = 0.0, interval: float = 0.0) -> void:
	# The game is running.
	if !Engine.editor_hint:
		# If the game is not initialised.
		if delay == 0.0 && !is_game_initialised (): yield (object.get_tree (), "idle_frame");
		# Otherwise.
		elif delay > 0.0: yield (object.get_tree ().create_timer (delay), "timeout");
	# Converts "nodes" parameter to a PoolStringArray.
	nodes = PoolStringArray (Array ([nodes]) if not is_array (nodes) else Array (nodes));
	# Converts "nparents" parameter to a PoolStringArray.
	nparents = PoolStringArray (Array ([nparents]) if not is_array (nparents) else Array (nparents));
	# Contains the new parents and node sizes.
	var sizes: PoolIntArray = PoolIntArray ([nodes.size (), nparents.size ()]);
	# The size of the first param is greater than size of the second param
	if sizes [0] >= sizes [1] and sizes [1] == 1:
		# Transforms the given new parent.
		var nparent = object.get_node_or_null (nparents [0]);
		# If the new parent is a node.
		if nparent is Node:
			# Changes parent of each node.
			for pos in range ((0 if !reversed else (sizes [0] - 1)), (sizes [0] if !reversed else -1), (1 if !reversed else -1)):
				# Filters each item from the node list.
				var node = object.get_node_or_null (nodes [pos]);
				# If the item is valid config.
				if node is Node:
					# Makes a delay for each node and gets the old parent of the current node.
					if !Engine.editor_hint && interval > 0.0: yield (object.get_tree ().create_timer (interval), "timeout"); var old_parent = node.get_parent ();
					# If the new parent is not equal to the old parent.
					if nparent != old_parent and not is_child_of (nparent, node, true):
						# If the parent of the given node is not "DontDestroyOnLoad".
						if old_parent != object.get_viewport ().get_node_or_null ("DontDestroyOnLoad"):
							# Moves the node to the new given parent.
							old_parent.remove_child (node); nparent.add_child (node);
						# Otherwise.
						else:
							# Is the Godot Mega Assets module ?
							if not node.has_method ("is_dont_destroy_mode"):
								# Moves the node to the new given parent.
								old_parent.remove_child (node); nparent.add_child (node);
							# Otherwise.
							else:
								# If the module is not dont destroy mode.
								if node.is_dont_destroy_mode () is bool and not node.is_dont_destroy_mode ():
									# Moves the node to the new given parent.
									old_parent.remove_child (node); nparent.add_child (node);
								# Error message.
								else: _output (("You cannot change parent of indestructible Mega Assets module::index " + str (pos)), Message.ERROR);
				# Error message.
				else: _output (("The current node is not defined::index " + str (pos)), Message.ERROR);
		# Error message.
		else: _output ("The new parent node is not defined.", Message.ERROR);
	# The size of the first param is greater than size of the second param.
	elif sizes [0] == sizes [1]:
		# Changes parent of each node.
		for idx in range ((0 if !reversed else (sizes [0] - 1)), (sizes [0] if !reversed else -1), (1 if !reversed else -1)):
			# Makes a delay for each node.
			if !Engine.editor_hint && interval > 0.0: yield (object.get_tree ().create_timer (interval), "timeout");
			# Re-parents all available nodes.
			re_parent (nodes [idx], nparents [idx], object, false, -1.0, 0.0);
	# Error message.
	else: _output ("The both arrays should have the same size.", Message.ERROR);

"""
	@Description: Creates an/some indestructible node(s). This/These node(s) is/are always present when the scene changed.
	@Parameters:
		NodePath | String | PoolStringArray nodes: Contains the target node(s) path(s).
		Node obj: Which knot will be considered to make the differents operations ?
		bool reversed: Do you want to inverse treatment ?
		float delay: What is the timeout before this action ?
		float interval: What is the timeout before each definition ?
"""
static func dont_destroy_on_load (nodes, obj: Node, reversed: bool = false, delay: float = 0.0, interval: float = 0.0) -> void:
	# If the game is running.
	if not Engine.editor_hint:
		# If the game is not initialised.
		if delay == 0.0 && not is_game_initialised (): yield (obj.get_tree (), "idle_frame"); elif delay > 0.0: yield (obj.get_tree ().create_timer (delay), "timeout");
		# Containts the don't destroy container node.
		var dont_destroy_on_load_container = obj.get_viewport ().get_node_or_null ("DontDestroyOnLoad");
		# If DontDestroyNode is not defined.
		if dont_destroy_on_load_container == null:
			# Creating a new instance of Node class.
			var container: Node = Node.new (); container.name = "DontDestroyOnLoad";
			# Gets out to the current scene.
			obj.get_viewport ().add_child (container); re_parent (nodes, container.get_path (), obj, reversed, -1.0, interval);
		# Gets out to the current scene.
		else: re_parent (nodes, dont_destroy_on_load_container.get_path (), obj, reversed, -1.0, interval);
	# Warning message.
	else: _output ("{dont_destroy_on_load} method must be call on runtime only.", Message.WARNING);

"""
	@Description: Moves a node to other node.
	@Parameters:
		NodePath | String node: Contains the target node path.
		NodePath | String other: Contains the parent node path of the node that will be moved.
		Node object: Which knot will be considered to make the differents operations ?
		int position: Contains the position of moved node in his parent.
		float delay: What is the timeout before moving ?
"""
static func move_node (node: NodePath, other: NodePath, object: Node, position: int = -1, delay: float = 0.0) -> void:
	# The game is running.
	if !Engine.editor_hint:
		# If the game is not initialised.
		if delay == 0.0 && !is_game_initialised (): yield (object.get_tree (), "idle_frame");
		# Otherwise.
		elif delay > 0.0: yield (object.get_tree ().create_timer (delay), "timeout");
	# Getting the target future parent nodes.
	var target_node = object.get_node_or_null (node); var future_parent = object.get_node_or_null (other);
	# Checks the given node reference value.
	if target_node is Node:
		# Checks the future parent node reference value.
		if future_parent is Node:
			# Contient the old parent of the given node and changes the parent of the given node.
			var old_parent = target_node.get_parent (); var last_position: int = 0; if future_parent is Node: re_parent (node, other, object, false, -1.0);
			# Changes the position of the child to the given position.
			if position >= 0:
				# The other is an instance of a node.
				if !future_parent is Node || future_parent is Node && target_node.get_parent () == old_parent:
					# Calculates the last position.
					last_position = (old_parent.get_child_count () - 1);
					# Valid position.
					if is_range (position, 0, last_position): old_parent.move_child (target_node, position); else: old_parent.move_child (target_node, last_position);
				# Otherwise.
				else:
					# Calculates the last position.
					last_position = (future_parent.get_child_count () - 1);
					# Valid position.
					if is_range (position, 0, last_position): future_parent.move_child (future_parent.get_child (last_position), position);
					# Otherwise.
					else: future_parent.move_child (future_parent.get_child (last_position), last_position);
		# Error message.
		else: _output ("The future parent node isn't defined.", Message.ERROR);
	# Error message.
	else: _output ("The given node isn't defined.", Message.ERROR);

"""
	@Description: Calls a method named "mname" with a certain time.
	@Parameters:
		String mname: Contains the method name.
		Array params: Contains all parameters of the target method.
		Object object: Contains the object instance that keep the target method.
		float delay: What is the timeout before calling the given method ? Note that if you give the reference of an object that is not
			the instance of a node, you will be exempt from using this parameter.
"""
static func invoke (mname: String, params: Array, object: Object, delay: float = 0.0):
	# Corrects the given method name.
	mname = str_replace (mname, PoolStringArray (["\n", "\t", "\a", "\b", "\r", "\v", "\f", ' ', '(', ')', '[', ']', '{', '}', ',', ';', '.', '-']), '');
	# If the method name is not empty.
	if not mname.empty ():
		# Checks method existance.
		if object.has_method (mname):
			# Waiting for user delay.
			if !Engine.editor_hint && object is Node && delay > 0.0: yield (object.get_tree ().create_timer (delay), "timeout");
			# Returns the method result whether it returns something in output.
			return funcref (object, mname).call_funcv (params);
		# Error message.
		else: _output (("Undefined {" + mname + "} method on {" + get_object_prefix (object) + "} instance."), Message.ERROR);
	# Warning message.
	else: _output ("The method name shouldn't be empty.", Message.WARNING); return null;

"""
	@Description: Calls a method named "mname" with a certain time at each interval.
	@Parameters:
		String mname: Contains the method name.
		Array params: Contains all params of the method.
		Object instance: Contains the object instance that keep the target method.
		int count: How many the target method will be called ?
		float interval: What is the timeout before each call ?
"""
func invoke_repeating (mname: String, params: Array = Array ([]), instance: Object = self, count: int = 1, interval: float = 0.0) -> void:
	# If the method name is not empty.
	if not mname.empty ():
		# If the given method is not defined in the dictionary.
		if !mname in self.__cash__.queue_methods: __cash__.queue_methods [mname] = [0, true];
		# If the repeat count is great than 0.
		if count > 0:
			# Waiting for user interval.
			if !Engine.editor_hint && interval > 0.0: yield (self.get_tree ().create_timer (interval), "timeout");
			# If the current repeatition is less than the given count.
			if self.__cash__.queue_methods [mname] [1] and self.__cash__.queue_methods [mname] [0] < count:
				# Calls the given method and recalls the same function.
				self.invoke (mname, params, instance); __cash__.queue_methods [mname] [0] += 1; self.invoke_repeating (mname, params, instance, count, interval);
			# Resets the repeating count to initial value.
			else: if __cash__.queue_methods.erase (mname): pass;
		# If the repeat count is less than 0.
		elif count < 0 and !Engine.editor_hint:
			# Waiting for the given interval.
			if interval > 0.0: yield (self.get_tree ().create_timer (interval), "timeout"); else: yield (self.get_tree (), "idle_frame");
			# If the current method is in repeating mode.
			if self.__cash__.queue_methods [mname] [1]:
				# Invokes infinite call.
				self.invoke (mname, params, instance); self.invoke_repeating (mname, params, instance, count, interval);
			# Free the computer memory.
			else: if __cash__.queue_methods.erase (mname): pass;
		# Free the computer memory.
		else: if __cash__.queue_methods.erase (mname): pass;
	# Warning message.
	else: self._output ("The method name shouldn't be empty.", self.Message.WARNING);

"""
	@Description: Cancels the method in invoke repeating mode.
	@Parameters:
		String mname: Contains the method name.
		Dictionary callback: Contains information on the method to be executed when the invocation of a method has been performed.
			This dictionary supports the following keys:
			-> String | NodePath source: Contains the address from method to targeted. The presence of this key is not mandatory.
				In this case, we assumed that the referred method is on the current reference.
			-> String method: Contains the name of the method to executed. The use of this key is mandatory.
		Note that the method to be executed must have only one parameter to contain the name of the method that has been canceled.
		float delay: What is the timeout before this action ?
"""
func cancel_invoke (mname: String, callback: Dictionary = Dictionary ({}), delay: float = 0.0) -> void:
	# Corrects the given method name.
	mname = str_replace (mname, PoolStringArray (["\n", "\t", "\a", "\b", "\r", "\v", "\f", ' ', '(', ')', '[', ']', '{', '}', ',', ';', '.', '-']), '');
	# If the method name is not empty.
	if not mname.empty ():
		# Waiting for user delay.
		if !Engine.editor_hint && delay > 0.0: yield (self.get_tree ().create_timer (delay), "timeout");
		# If the method data exists.
		if mname in self.__cash__.queue_methods:
			# Stops the invoking method and calls the given callback whether exists.
			__cash__.queue_methods [mname] [0] = 0; __cash__.queue_methods [mname] [1] = false; _callback_manager (callback, Array ([mname]), false, self);
	# Warning message.
	else: self._output ("The method name shouldn't be empty.", self.Message.WARNING);

"""
	@Description: Determinates whether a method named "mname" is in invoke repeating mode.
	@Parameters:
		String mname: Contains the method name.
"""
func is_invoking (mname: String) -> bool:
	# Corrects the given method name.
	mname = str_replace (mname, PoolStringArray (["\n", "\t", "\a", "\b", "\r", "\v", "\f", ' ', '(', ')', '[', ']', '{', '}', ',', ';', '.', '-']), '');
	# If the method name is not empty.
	if not mname.empty (): return (self.__cash__.queue_methods [mname] [1] if mname in self.__cash__.queue_methods else false);
	# Warning message.
	else: self._output ("The method name shouldn't be empty.", self.Message.WARNING); return false;

"""
	@Description: Finds all nodes from their name/group/class name.
	@Parameters:
		String | PoolStringArray | Array id: Contains the target node(s) name(s)/group(s)/class name(s).
		Node ref: Contains the search reference.
		int prop: What is the node property that has been targeted to find the given node(s) ? The supported values are:
			-> MegaAssets.NodeProperty.NAME or 0: Finds nodes using their name.
			-> MegaAssets.NodeProperty.GROUP or 1: Finds nodes using their group name.
			-> MegaAssets.NodeProperty.TYPE or 2: Finds nodes using their class name.
			-> MegaAssets.NodeProperty.ANY or 3: Finds nodes using their class name/name/group.
		int count: How many result(s) will be returned ? If this value is less than 0, you will get all found values.
		bool inv: Do you want to inverse treatment ?
		bool rec: Do you want to use the recursive algorithm to find some nodes ?
"""
static func search (id, ref: Node, prop: int = NodeProperty.ANY, count: int = -1, inv: bool = false, rec: bool = true):
	# Checks count value.
	if count != 0:
		# Converts "id" parameter to an array and then into a PoolStringArray.
		id = PoolStringArray (Array ([id]) if not is_array (id) else Array (id)); var nodes: Array = Array ([]); var children: Array = ref.get_children ();
		# Searches the given nodes names.
		for c in range ((0 if !inv else (children.size () - 1)), (children.size () if !inv else -1), (1 if !inv else -1)):
			# Gets out of this loop.
			if count > 0 && nodes.size () == count: break;
			# For nodes names.
			if prop <= NodeProperty.NAME and children [c].name in id: nodes.append (children [c]);
			# For nodes groups.
			elif prop == NodeProperty.GROUP:
				# If the id contains the given group name.
				for x in id: if x in children [c].get_groups (): nodes.append (children [c]);
			# For node class names.
			elif prop == NodeProperty.TYPE and children [c].get_class () in id: nodes.append (children [c]);
			# For any node.
			elif prop >= NodeProperty.ANY:
				# Try node name and class type.
				if children [c].name in id: nodes.append (children [c]); elif children [c].get_class () in id: nodes.append (children [c]);
				# Try node group.
				else:
					# If the id contains the given group name.
					for string in id: if string in children [c].get_groups (): nodes.append (children [c]);
			# Gets out of this loop.
			if count > 0 && nodes.size () == count: break;
			# If the recursivity is activate.
			if rec and children [c].get_child_count () > 0:
				# Contains the found node in other process.
				var result = search (id, children [c], prop, ((count - nodes.size ()) if count > 0 else count), inv, rec);
				# If there are a node that will be found.
				if result != null:
					# If the result is an array.
					if result is Array: for item in result: nodes.append (item); else: nodes.append (result);
		# Returns the final value.
		if nodes.size () == 1: return nodes [0]; elif nodes.size () <= 0: return null; else: return nodes;
	# Returns an empty value.
	else: return null;

"""
	@Description: Determinates whether a/some node(s) is/are inside in other node.
	@Parameters:
		String | PoolStringArray | Array id: Contains the target node(s) name(s)/group(s)/class name(s).
		Node | Array scope: Contains the search scope. In the array, use objects node instance only.
		int prop: What is the node property that has been targeted to find the given node(s) ? The supported values are:
			-> MegaAssets.NodeProperty.NAME or 0: Finds nodes using their name.
			-> MegaAssets.NodeProperty.GROUP or 1: Finds nodes using their group name.
			-> MegaAssets.NodeProperty.TYPE or 2: Finds nodes using their class name.
			-> MegaAssets.NodeProperty.ANY or 3: Finds nodes using their class name/name/group.
		bool rec: Do you want to use the recursive algorithm to find some nodes ?
"""
static func contains (id, scope, prop: int = NodeProperty.ANY, rec: bool = true) -> bool:
	# Converts "scope" parameter to an array.
	scope = Array ([scope]) if not scope is Array else scope;
	# Determines whether these scope contains the given node id.
	for parent in scope:
		# The current parent is not an instance of Node class.
		if not parent is Node: return false;
		# Otherwise.
		else:
			# Searches all availables ids.
			for key in id: if search (key, parent, prop, 1, false, rec) == null: return false;
	# All specified node(s) are contains in the scope.
	return true;

"""
	@Description: Determinates whether a/some node(s) can be destroyed on changing a scene. The search here isn't recursive.
	@Parameters:
		String | PoolStringArray | Array id: Contains the target node(s) id(s).
		Node object: Which knot will be considered to make the differents operations ?
		int property: What is the node property that has been targeted to find the given node(s) id(s) ? The supported values are:
			-> MegaAssets.NodeProperty.NAME or 0: Finds nodes using their name.
			-> MegaAssets.NodeProperty.GROUP or 1: Finds nodes using their group name.
			-> MegaAssets.NodeProperty.TYPE or 2: Finds nodes using their class name.
			-> MegaAssets.NodeProperty.ANY or 3: Finds nodes using their class name/name/group.
"""
static func is_indestructible (id, object: Node, property: int = NodeProperty.ANY) -> bool:
	# Contains the game viewport and one his child.
	var viewport: Viewport = object.get_viewport (); var container = viewport.get_node_or_null ("DontDestroyOnLoad");
	# Searches the node id into the viewport.
	var on_viewport: bool = contains (id, viewport, property, false);
	# Searches the node id into the DontDestroyOnLoad node and checks whether the current node id is into the autoload section.
	var on_dont_destroy_container: bool = contains (id, container, property, false) if container != null else false; return on_viewport or on_dont_destroy_container;

"""
	@Description: Finds all indestructible node(s) from their name/group/class name. The search scope here is "DontDestroyOnLoad" node.
	@Parameters:
		String | PoolStringArray | Array id: Contains an/some indestructible node(s) name(s)/group(s)/class name(s).
		Node obj: Which knot will be considered to make the differents operations ?
		int prop: What is the node property that has been targeted to find the given node(s) id(s) ? The supported values are:
			-> MegaAssets.NodeProperty.NAME or 0: Finds nodes using their name.
			-> MegaAssets.NodeProperty.GROUP or 1: Finds nodes using their group name.
			-> MegaAssets.NodeProperty.TYPE or 2: Finds nodes using their class name.
			-> MegaAssets.NodeProperty.ANY or 3: Finds nodes using their class name/name/group.
		int count: How many result(s) will be returned ? If this value is less than 0, you will get all found values.
		bool inv: Do you want to inverse treatment ?
		bool red: Do you want to use the recursive algorithm to find an/some indestructible node(s) ?
"""
static func get_indestructible (id, obj: Node, prop: int = NodeProperty.ANY, count: int = -1, inv: bool = false, rec: bool = true):
	# The game isn't initialized ?
	if not is_game_initialised ():
		# Getting the active scene reference and returns an/some indestructible node(s).
		var active_scene = obj.get_tree ().current_scene; return search (id, active_scene, prop, count, inv, rec) if active_scene != null else null;
	# Otherwise.
	else:
		# Contains "DontDestroyOnLoad" node reference.
		var container = obj.get_viewport ().get_node_or_null ("DontDestroyOnLoad") if not Engine.editor_hint else obj.get_viewport ();
		# Returns an/some indestructible node(s).
		return search (id, container, prop, count, inv, rec) if container != null else null;

"""
	@Description: Gets all nodes inside of named node "DontDestroyOnLoad".
	@Parameters:
		Node object: Which knot will be considered to make the differents operations ?
"""
static func get_all_indestructible_nodes (object: Node) -> Array:
	# Contains the container of don't destroy nodes and checks whether the "DontDestroyOnLoad" container is not defined.
	var dont_destroyer = object.get_viewport ().get_node_or_null ("DontDestroyOnLoad"); return dont_destroyer.get_children () if dont_destroyer != null else Array ([]);

"""
	@Description: Gets the node where the class name is "SaveLoadFx".
	@Parameters:
		Node object: Which knot will be considered to make the differents operations ?
"""
static func get_data_manager (object: Node): return get_indestructible ("SaveLoadFx", object, NodeProperty.TYPE, 1);

"""
	@Description: Gets the node where the class name is "AudioControllerFx".
	@Parameters:
		Node object: Which knot will be considered to make the differents operations ?
"""
static func get_audio_manager (object: Node): return get_indestructible ("AudioControllerFx", object, NodeProperty.TYPE, 1);

"""
	@Description: Gets the node where the class name is "ScenesFx".
	@Parameters:
		Node object: Which knot will be considered to make the differents operations ?
"""
static func get_scenes_manager (object: Node): return get_indestructible ("ScenesFx", object, NodeProperty.TYPE, 1);

"""
	@Description: Gets the node where the class name is "LanguagesFx".
	@Parameters:
		Node object: Which knot will be considered to make the differents operations ?
"""
static func get_languages_manager (object: Node): return get_indestructible ("LanguagesFx", object, NodeProperty.TYPE, 1);

"""
	@Description: Gets the node where the class name is "SettingsFx".
	@Parameters:
		Node object: Which knot will be considered to make the differents operations ?
"""
static func get_settings_manager (object: Node): return get_indestructible ("SettingsFx", object, NodeProperty.TYPE, 1);

"""
	@Description: Gets the node where the class name is "ControllersSensorFx".
	@Parameters:
		Node object: Which knot will be considered to make the differents operations ?
"""
static func get_controllers_manager (object: Node): return get_indestructible ("ControllersSensorFx", object, NodeProperty.TYPE, 1);

"""
	@Description: Gets the node where the class name is "CursorFx".
	@Parameters:
		Node object: Which knot will be considered to make the differents operations ?
"""
static func get_cursor_manager (object: Node): return get_indestructible ("CursorFx", object, NodeProperty.TYPE, 1);

"""
	@Description: Gets the node where the class name is "VideoRecorderFx".
	@Parameters:
		Node object: Which knot will be considered to make the differents operations ?
"""
static func get_video_recorder (object: Node): return get_indestructible ("VideoRecorderFx", object, NodeProperty.TYPE, 1);

"""
	@Description: Gets the node where the class name is "AudioRecorderFx".
	@Parameters:
		Node object: Which knot will be considered to make the differents operations ?
"""
static func get_audio_recorder (object: Node): return get_indestructible ("AudioRecorderFx", object, NodeProperty.TYPE, 1);

"""
	@Description: Gets the node where the class name is "MultiplayerFx".
	@Parameters:
		Node object: Which knot will be considered to make the differents operations ?
"""
static func get_multiplayer_manager (object: Node): return get_indestructible ("MultiplayerFx", object, NodeProperty.TYPE, 1);

"""
	@Description: Gets the node where the class name is "OcclusionCullingFx".
	@Parameters:
		Node object: Which knot will be considered to make the differents operations ?
"""
static func get_occlusion_culling (object: Node): return get_indestructible ("OcclusionCullingFx", object, NodeProperty.TYPE, 1);

"""
	@Description: Determinates whether an index is out of range from an array or dictionary.
	@Parameters:
		int index_value: Contains the value of an index.
		int size: Contains the size of the target array or dictionary.
"""
static func index_validation (index_value: int, size: int) -> bool: return index_value >= (size * -1) and index_value < size;

"""
	@Description: Determinates whether an index is out of range from an array or dictionary.
	@Parameters:
		int index_value: Contains the value of an index.
		int size: Contains the size of the target array or dictionary.
		String message: Contains the message that will be printed on the editor console when the given index value is out of range of the given array or dictionary.
		int type: Contains the type of the given message. The available values are:
			-> MegaAssets.Message.NORMAL or 0: Simple message.
			-> MegaAssets.Message.WARNING or 1: Warning message.
			-> MegaAssets.Message.ERROR or 2: Error message.
"""
static func index_validation_msg (index_value: int, size: int, message: String = String (''), type: int = Message.NORMAL) -> bool:
	# If it's a correct index.
	if !index_validation (index_value, size):
		# If a message is defined.
		if not message.lstrip (' ').rstrip (' ').empty (): _output (message, type); return false;
	# Otherwise.
	else: return true;

"""
	@Description: Instanciates a node from a file path.
	@Parameters:
		Dictionary data: Contains all data about the future instance of the target prefab. This parameter accepts the following keys:
			-> String path: Contains the path to the future prefab to loaded.
			-> NodePath | String parent: Where the given prefab will be imported ?
			-> Vector3 position: Contains the position of the prefab to loaded.
			-> Vector3 rotation: Contains the rotation of the prefab to loaded.
			-> Vector3 scale: Contains the scaling of the prefab to loaded.
			-> float live = -1: Contains the delay before destroying the prefab.
			-> bool visible = true: Your prefab will visible or not.
			-> bool duplicate = false: Duplicate prefab when it already loaded previously.
			-> String name: Do you want to change the name of the future prefab ?
			-> String | PoolStringArray groups: Do you want to add some group(s) to the future prefab ?
			-> int zindex = -1: Do you want to change the prefab hierarchy position after loading ?
			-> bool background = true: Do you want to load the prefab on background process ?
			-> bool open = true: Do you want to fix the future prefab into the scene tree.
			-> bool global = false: Do you want to fix the prefab into the scene global position, rotation and scale after loading ?
			-> Dictionary callback: Contains a callback method that will be executed when the prefab is loading to get in real time, the loading
				progress. This dictionary support the following keys:
				-> String method: Contains a callback name.
				-> String | NodePath source: Where found the given callback ?
				Note that the method to be executed must have four (04) parameters namely:
				-> String id: Will contain the name of the file or the object.
				-> int progress: Will contain the current progress of the object being loaded.
				-> Variant reference: Will contain the reference of the object when it has been successfully loaded. By default, you will have a zero value.
				-> Variant error: Will contain the triggered error while loading the object. This parameter will return you a dictionary containing the keys:
					"message", "code" and "type" or null if no error occurred while loading the object.
		Node object: Which knot will be considered to make the differents operations ?
		float delay: What is the time before this action ?
"""
static func instanciate (data: Dictionary, object: Node, delay: float = 0.0) -> void:
	# The game is running.
	if !Engine.editor_hint:
		# If the game is not initialised.
		if delay == 0.0 && !is_game_initialised (): yield (object.get_tree (), "idle_frame");
		# Otherwise.
		elif delay > 0.0: yield (object.get_tree ().create_timer (delay), "timeout");
	# Getting the given callback.
	data.callback = data.callback if data.has ("callback") && data.callback is Dictionary else Dictionary ({});
	# Getting the path value.
	data.path = data.path.lstrip (' ').rstrip (' ') if data.has ("path") and data.path is String else '';
	# If the prefab path is defined.
	if not data.path.empty ():
		# Checks prefab extension.
		if data.path.ends_with (".tscn") or data.path.ends_with (".scn") or data.path.ends_with (".tres") or data.path.ends_with (".res"):
			# Checks path validity.
			if ResourceLoader.exists (data.path):
				# Getting the given new name of the future prefab.
				data.name = data.name if data.has ("name") and data.name is String else '';
				# Contains an instance of the loaded prefab.
				var prefab = null; var fname: String = data.path.get_file (); var step: int = 0; var progress: int = 0;
				# Getting the future parent of the given prefab.
				data.parent = (data.parent if data.parent is String || data.parent is NodePath else object.get_path ()) if data.has ("parent") else object.get_path ();
				# Gets the real parent node.
				var parent = object.get_node_or_null (data.parent);
				# If the parent is a Node.
				if parent is Node:
					# Gets the parent node prefix and checks whether the prefab to loaded is already exists on the given parent node.
					var parent_prefix: String = get_object_prefix (parent); var child = parent.get_node_or_null (fname.split ('.') [0].lstrip (' ').rstrip (' '));
					# Re-gets the parent child whether the last reference is null.
					child = parent.get_node_or_null (data.name if !data.name.empty () else '') if child == null else child;
					# The prefab to loaded don't exists.
					if child == null:
						# The prefab won't be loaded on background.
						if not (data.background if data.has ("background") and data.background is bool else true):
							# Checks "callback" key presence and start loading prefab.
							_callback_manager (data.callback, Array ([fname, 0, null, null]), false, object); prefab = load (data.path);
							# Checks whether the given path has been cached before.
							if prefab != null:
								# Gets prefab reference and updates the loading progress.
								prefab = prefab.instance (); progress = 100;
							# Otherwise.
							else: _callback_manager (data.callback, Array ([fname, 0, null, Dictionary ({
								"message": ('{' + fname + "} is already loading in other process."), "code": ERR_FILE_ALREADY_IN_USE, "type": Message.ERROR
							})]), true, object);
						# The prefab will be loaded in background.
						else:
							# Updates the prefab.
							prefab = ResourceLoader.load_interactive (data.path);
							# The path is not correct or the target access is denied.
							if prefab == null: _callback_manager (data.callback, Array ([fname, 0, null, Dictionary ({
								"message": ('{' + fname + "} is already loading in other process."), "code": ERR_FILE_ALREADY_IN_USE, "type": Message.ERROR
							})]), true, object);
							# Otherwise.
							else:
								# Checks the loading step for any error.
								while step == OK:
									# Waiting for smooth and updates the loading progress.
									if !Engine.editor_hint: yield (object.get_tree (), "idle_frame"); progress = ((prefab.get_stage () * 100) / prefab.get_stage_count ());
									# Calls the given callback to update prefab progress and polls the next stage.
									_callback_manager (data.callback, Array ([fname, progress, null, null]), false, object); step = prefab.poll ();
								# There are no errors.
								if step == ERR_FILE_EOF:
									# Gets prefab reference and updates the loading progress.
									prefab = prefab.get_resource ().instance (); progress = 100;
								# Some errors have been thrown.
								else: _callback_manager (data.callback, Array ([fname, progress, null, Dictionary ({
									"message": "An error has been thrown on loading prefab.", "code": FAILED, "type": Message.ERROR
								})]), true, object);
					# Otherwise.
					else:
						# Gets duplicated key value.
						data.duplicate = data.duplicate if data.has ("duplicate") and data.duplicate is bool else false;
						# Duplicates option is not enabled.
						if not data.duplicate:
							# Normal message.
							_output (('{' + child.name + "} node is already loaded on {" + parent_prefix + "}."), Message.NORMAL); return;
						# Otherwise.
						else:
							# Changes a certains configurations.
							prefab = child.duplicate (); fname = data.name if not data.name.empty () else prefab.name; progress = -1;
					# Checks pref value.
					if prefab != null:
						# Do you want to change the global coordinates ?
						data.global = data.global if data.has ("global") and data.global is bool else false;
						# Changes the prefab name.
						prefab.name = data.name if not data.name.empty () else prefab.name;
						# Getting user group(s).
						data.groups = data.groups if data.has ("groups") else PoolStringArray ([]);
						# Converting the given group into an Array and the final result into a PoolStringArray.
						data.groups = PoolStringArray (Array ([data.group]) if not is_array (data.groups) else Array (data.groups));
						# Adds available group(s) on the prefab.
						for group in data.groups: if not prefab.is_in_group (group): prefab.add_to_group (group);
						# Do you want to import the loaded prefab into the scene tree ?
						if (data.open if data.has ("open") and data.open is bool else true):
							# Getting zindex value.
							data.zindex = data.zindex if data.has ("zindex") and is_number (data.zindex) else -1;
							# If the visibility is defined.
							if data.has ("visible") && data.visible is bool: prefab.visible = data.visible; parent.add_child (prefab);
							# Applying zindex effect.
							if prefab is Node: move_node (prefab.get_path (), parent.get_path (), object, data.zindex, -1.0);
							# If the position is defined.
							if data.has ("position"):
								# For a Spatial object.
								if prefab is Spatial:
									data.position = any_to_vector3 (data.position);
									prefab.transform.origin = data.position if !data.global && data.position != null else prefab.transform.origin;
									prefab.global_transform.origin = data.position if data.global && data.position != null else prefab.global_transform.origin;
								# For a Node2D object.
								elif prefab is Node2D:
									data.position = any_to_vector2 (data.position);
									prefab.position = data.position if !data.global && data.position != null else prefab.position;
									prefab.global_position = data.position if data.global && data.position != null else prefab.global_position;
								# For a Control object.
								elif prefab is Control:
									data.position = any_to_vector2 (data.position);
									prefab.rect_position = data.position if !data.global && data.position != null else prefab.rect_position;
									prefab.rect_global_position = data.position if data.global && data.position != null else prefab.rect_global_position;
							# If the rotation is defined.
							if data.has ("rotation"):
								# For a Spatial object.
								if prefab is Spatial:
									data.rotation = any_to_vector3 (data.rotation);
									prefab.rotation_degrees = data.rotation if data.rotation != null else prefab.rotation_degrees;
								# For a Node2D object.
								elif prefab is Node2D and is_number (data.rotation): prefab.rotation_degrees = float (data.rotation);
								# For a Control object.
								elif prefab is Control and is_number (data.rotation): prefab.rect_rotation = float (data.rotation);
							# If the scale is defined.
							if data.has ("scale"):
								# For a Spatial object.
								if prefab is Spatial:
									data.scale = any_to_vector3 (data.scale); prefab.scale = data.scale if data.scale != null else prefab.scale;
								# For a Node2D object.
								elif prefab is Node2D:
									data.scale = any_to_vector2 (data.scale); prefab.scale = data.scale if !data.global && data.scale != null else prefab.scale;
									prefab.global_scale = data.scale if data.global && data.scale != null else prefab.global_scale;
								# For a Control object.
								elif prefab is Control:
									data.scale = any_to_vector2 (data.scale); prefab.rect_scale = data.scale if data.scale != null else prefab.rect_scale;
							# Calls the given callback to pass the loaded prefab.
							_callback_manager (data.callback, Array ([fname, progress, prefab, null]), false, object);
						# Checks the prefab live.
						if !Engine.editor_hint && data.has ("live") && is_number (data.live) && float (data.live) >= 0.0:
							# Waiting for prefab live and checks the prefab definition after.
							yield (object.get_tree ().create_timer (float (data.live)), "timeout"); if is_instance_valid (prefab): prefab.queue_free ();
					# Otherwise.
					else: _callback_manager (data.callback, Array ([fname, progress, prefab, null]), false, object);
				# Error message.
				else: _output ("The given parent node is not defined.", Message.ERROR);
			# Error message.
			else: _output ("The prefab to loaded doesn't exists.", Message.ERROR);
		# Error message.
		else: _output ("This method can only support (.tscn), (.scn), (.tres) and (.res) files extension.", Message.ERROR);
	# Error message.
	else: _output ("The prefab path is not defined.", Message.ERROR);

"""
	@Description: Instanciates severals nodes from their file path.
	@Parameters:
		Array data: Contains a array of dictionary data about each prefab. The dictionary accepts the following keys:
			-> String path: Contains the path to the future prefab to loaded.
			-> Node parent: Where the given prefab will be imported ?
			-> Vector3 position: Contains the position of the prefab to loaded.
			-> Vector3 rotation: Contains the rotation of the prefab to loaded.
			-> Vector3 scale: Contains the scaling of the prefab to loaded.
			-> float live = -1: Contains the delay before destroying of the prefab.
			-> int count = 1: How many prefab will be imported ? If the count value is less than 0, you will get an infinite prefab importation.
			-> float interval = 0.0: What is the delay before each instance ?
			-> bool visible = true: Your prefab will visible or not.
			-> bool duplicate = false: Duplicate prefab when it already loaded previously.
			-> String name: Do you want to change the name of the future prefab ?
			-> String | PoolStringArray groups: Do you want to add some group(s) to the future prefab ?
			-> int zindex = -1: Do you want to change the prefab hierarchy position after loading ?
			-> bool background = true: Do you want to load the prefab on background process ?
			-> bool open = true: Do you want to fix the future prefab into the scene tree.
			-> bool global = false: Do you want to fix the prefab into the scene global position, rotation and scale after loading ?
			-> Dictionary callback: Contains a callback method that will be executed when the prefab is loading to get in real time, the loading
				progress. This dictionary support the following keys:
				-> String method: Contains a callback name.
				-> String | NodePath source: Where found the given callback ?
				Note that the method to be executed must have four (04) parameters namely:
				-> String id: Will contain the name of the file or the object.
				-> int progress: Will contain the current progress of the object being loaded.
				-> Variant reference: Will contain the reference of the object when it has been successfully loaded. By default, you will have a zero value.
				-> Variant error: Will contain the triggered error while loading the object. This parameter will return you a dictionary containing the keys:
					"message", "code" and "type" or null if no error occurred while loading the object.
		Node object: Which knot will be considered to make the differents operations ?
		bool async: Do us asynchronously load the different(s) object(s) given ?
		int direction: which direction should be used for instancing prefabs ? The possibles values are:
			-> MegaAssets.Orientation.NORMAL or 0: Normal prefabs importation.
			-> MegaAssets.Orientation.REVERSED or 1: Reversed prefabs importation.
			-> MegaAssets.Orientation.RANDOM or 2: Randomize prefabs importation.
			-> MegaAssets.Orientation.ALTERNATE or 3: Alternate prefabs importation.
		int repeat: How many prefabs array will have to be runned ? If this value is less than 0, the array will be runned infinity. Nothing would
			happen over no repeat.
		float delay: What is the timeout before this action ?
"""
static func instanciates (data: Array, object: Node, async: bool = false, direction: int = Orientation.NORMAL, repeat: int = 1, delay: float = 0.0) -> void:
	# If the array size is not null.
	if not data.empty ():
		# The game is running.
		if !Engine.editor_hint:
			# If the game is not initialised.
			if delay == 0.0 && !is_game_initialised (): yield (object.get_tree (), "idle_frame");
			# Otherwise.
			elif delay > 0.0: yield (object.get_tree ().create_timer (delay), "timeout");
		# Contains some usefull value for managing directions modes.
		var start: int = 0; var end: int = 0; var step: int = 1; var indexer: int = 0;
		# For normal, reverse and alternate mode.
		if direction == Orientation.NORMAL || direction == Orientation.REVERSED || direction == Orientation.ALTERNATE:
			# Checks mode to update the start and end position.
			start = 0 if direction == Orientation.NORMAL || direction == Orientation.ALTERNATE else (data.size () - 1);
			end = data.size () if direction == Orientation.NORMAL || direction == Orientation.ALTERNATE else -1;
			step = 1 if direction == Orientation.NORMAL || direction == Orientation.ALTERNATE else -1;
		# Runs the array with repeat value.
		while repeat != 0:
			# The indexer value is equal to the repeat parameter value.
			if indexer == repeat: break;
			# For random mode.
			if direction == Orientation.RANDOM:
				# Makes a random position.
				randomize (); start = (randi () % data.size ()); end = (start + 1);
			# Instanciating several prefabs.
			for p in range (start, end, step):
				# If the prefab data is a dictionary.
				if data [p] is Dictionary:
					# Getting the object count instance.
					data [p].count = data [p].count if data [p].has ("count") else 1; data [p].count = int (data [p].count);
					# If the prefab count is greather than 0.
					if data [p].count > 0:
						# Instanciates the given prefab at count variable.
						for n in range (0, data [p].count):
							# While n value is less than or equal (count - 1).
							if data [p].count == 1 or n <= (data [p].count - 1) && data [p].count > 1:
								# If user put an interval for each prefab importation.
								if !Engine.editor_hint && data [p].has ("interval") && is_number (data [p].interval) && float (data [p].interval) > 0.0:
									# Waiting for user interval.
									yield (object.get_tree ().create_timer (float (data [p].interval)), "timeout");
								# Checks async value.
								if not async and data [p].count == 1: yield (instanciate (data [p], object, -1.0), "completed");
								# Otherwise.
								else: instanciate (data [p], object, -1.0);
							# Gets out of the for loop.
							else: break;
					# The prefab count is less than 0.
					elif data [p].count < 0:
						# Infinite importation.
						while data [p].count < 0:
							# Getting object inportation interval.
							data [p].interval = float (data [p].interval if data [p].has ("interval") && is_number (data [p].interval) else 0.0);
							# If user puts an interval for each prefab importation.
							if !Engine.editor_hint and data [p].interval > 0.0: yield (object.get_tree ().create_timer (data [p].interval), "timeout");
							# Waiting for the game idle frame and instanciates the current object or prefab.
							else: yield (object.get_tree (), "idle_frame"); instanciate (data [p], object, -1.0);
			# For alternate reverse mode.
			if direction == Orientation.ALTERNATE:
				# Calls the same method in resverse mode and get out of the for loop.
				instanciates (data, object, async, Orientation.REVERSED, 1, -1.0); break;
			# Updates indexer value.
			indexer += 1 if repeat > 0 else 0;
	# Warning message.
	else: _output ("The data size mustn't be empty.", Message.WARNING);

"""
	@Description: Determinates whether a character is a number.
	@Parameters:
		String character: Contains a character. The size of this parameter must be equal to 1.
"""
static func is_a_number (character: String) -> bool:
	# If the string count is greather than 1.
	if character.length () == 1: return is_range (ord (character), 48, 57);
	# Otherwise.
	else:
		# Error message.
		if character.length () > 1: _output ("The character count must be equal to 1.", Message.ERROR); return false;

"""
	@Description: Determinates whether a string is full numbers.
	@Parameters:
		String string: Contains a list of characters.
"""
static func is_full_numbers (string: String) -> bool:
	# Removes all left and right spaces and searches whether all characters are full numbers.
	string = str_replace (string, PoolStringArray (["\n", "\t", "\a", "\b", "\r", "\v", "\f", ' ']), '');
	for chr in string: if !is_a_number (chr): return false; return true if not string.empty () else false;

"""
	@Description: Determinates whether a character is a letter.
	@Parameters:
		String character: Contains a character. The size of this parameter must be equal to 1.
"""
static func is_a_letter (character: String) -> bool: return not is_a_number (character);

"""
	@Description: Determinates whether a string is full letters.
	@Parameters:
		String string: Contains a list of characters.
"""
static func is_full_letters (string: String) -> bool:
	# Removes all left and right spaces and searches whether all characters are full letters.
	string = str_replace (string, PoolStringArray (["\n", "\t", "\a", "\b", "\r", "\v", "\f", ' ']), '');
	for chr in string: if !is_a_letter (chr): return false; return true if not string.empty () else false;

"""
	@Description: Determinates whether a character is upper case.
	@Parameters:
		String character: Contains a character. The size of this parameter must be equal to 1.
"""
static func is_upper_case (character: String) -> bool:
	# If the string count is great than 1.
	if character.length () == 1: return not ord (character.to_upper ()) < ord (character);
	# Otherwise.
	else:
		# Error message.
		if character.length () > 1: _output ("The character count must be equal to 1.", Message.ERROR); return false;

"""
	@Description: Determinates whether a string is full upper case.
	@Parameters:
		String string: Contains a list of characters.
"""
static func is_full_upper_case (string: String) -> bool:
	# Removes all left and right spaces and searches whether all characters are full letters.
	string = str_replace (string, PoolStringArray (["\n", "\t", "\a", "\b", "\r", "\v", "\f", ' ']), '');
	for chr in string: if !is_upper_case (chr): return false; return true if not string.empty () else false;

"""
	@Description: Determinates whether a character is lower case.
	@Parameters:
		String character: Contains a character. The size of this parameter must be equal to 1.
"""
static func is_lower_case (character: String) -> bool: return not is_upper_case (character);

"""
	@Description: Determinates whether a string is full lower case.
	@Parameters:
		String string: Contains a list of characters.
"""
static func is_full_lower_case (string: String) -> bool:
	# Removes all left and right spaces and searches whether all characters are full letters.
	string = str_replace (string, PoolStringArray (["\n", "\t", "\a", "\b", "\r", "\v", "\f", ' ']), '');
	for chr in string: if !is_lower_case (chr): return false; return true if not string.empty () else false;

"""
	@Description: Finds all numbers from a string.
	@Parameters:
		String string: Contains a list of characters.
"""
static func get_numbers_from (string: String):
	# Removes all left and right spaces.
	string = str_replace (string, PoolStringArray (["\n", "\t", "\a", "\b", "\r", "\v", "\f", ' ']), '');
	# If the given string is empty or is a simple space.
	if string.empty (): return null;
	# Otherwise.
	else:
		# Searches all numbers in the given string.
		var filter: PoolIntArray = PoolIntArray ([]); for chr in string: if is_a_number (chr): filter.append (int (chr));
		# Returns the final value.
		if filter.size () <= 0: return null; elif filter.size () == 1: return filter [0]; else: return filter;

"""
	@Description: Finds all letters from a string.
	@Parameters:
		String string: Contains a list of characters.
"""
static func get_letters_from (string: String):
	# Removes all left and right spaces.
	string = str_replace (string, PoolStringArray (["\n", "\t", "\a", "\b", "\r", "\v", "\f", ' ']), '');
	# If the given string is empty or is a simple space.
	if string.empty (): return null;
	# Otherwise.
	else:
		# Searches all letters in the given string.
		var filter: PoolStringArray = PoolStringArray ([]); for chr in string: if is_a_letter (chr): filter.append (chr);
		# Returns the final value.
		if filter.size () <= 0: return null; elif filter.size () == 1: return filter [0]; else: return filter;

"""
	@Description: Finds all letters from a string and the given case (Upper or Lower).
	@Parameters:
		String string: Contains a list of characters.
		bool is_lower: Defined which both cases will be returned.
"""
static func get_letters_case_from (string: String, is_lower: bool):
	# Removes all left and right spaces.
	string = str_replace (string, PoolStringArray (["\n", "\t", "\a", "\b", "\r", "\v", "\f", ' ']), '');
	# If the given string is empty or is a simple space.
	if string.empty (): return null;
	# Otherwise.
	else:
		# Contains all contained numbers in the given string.
		var filter: PoolStringArray = PoolStringArray ([]);
		# Searches all numbers in the given string.
		for chr in string:
			# Checks the nature of this character.
			if is_a_letter (chr):
				# For upper and lower case characters.
				if !is_lower and is_upper_case (chr): filter.append (chr); elif is_lower and is_lower_case (chr): filter.append (chr);
		# Returns the final value.
		if filter.size () <= 0: return null; elif filter.size () == 1: return filter [0]; else: return filter;

"""
	@Description: Returns a hexadecimal symbol from the given integer value. Your value must be in range (10; 15).
	@Parameters:
		int value: Contains the value that will be transformed.
		bool is_lower: Defined which both cases will be returned.
"""
static func get_hex_symbol (value: int, is_lower: bool = false):
	# Searches the corresponding value.
	match value:
		# Checks the given value.
		10: return ('A' if !is_lower else 'a'); 11: return ('B' if !is_lower else 'b'); 12: return ('C' if !is_lower else 'c'); 13: return ('D' if !is_lower else 'd');
		14: return ('E' if !is_lower else 'e'); 15: return ('F' if !is_lower else 'f'); _: return value;

"""
	@Description: Returns the normal value from the given hexadecimal symbol. Your value must be in range (a/A; f/F).
	@Parameters:
		String symbol: Contains the symbol that will be transformed.
"""
static func get_int_from_hex (symbol: String):
	# Searches the corresponding value.
	match symbol.to_lower ():
		# Checks the given value
		'a': return 10; 'b': return 11; 'c': return 12; 'd': return 13; 'e': return 14; 'f': return 15; _: return symbol;

"""
	@Description: Converts any type into a bit type. Eg: binary, hex, etc... Pay attention ! the supported types are:
		Integer, Float, String, Vector2, Vector3, Array and Dictionary.
	@Parameters:
		Variant type: What is the value of your type ?
		int bit: What is your bit ?
		bool security: Do you want to put a security on the final value ?
"""
static func any_to_bit (type, bit: int, security: bool = true):
	# Checks bit value.
	if bit > 1:
		# For Array and Dictionary types value.
		if type is Array: return _list_to_bit (type, bit, security); elif type is Dictionary: return _dic_to_bit (type, bit, security);
		# For basics types values.
		else: return _type_to_bit (type, bit, security);
	# Warning message.
	else: _output ("The value of the bit parameter mustn't be less than 2.", Message.WARNING);

"""
	@Description: Returns a normal value of any type from his coded value. Pay attention ! the supported types are: Integer, Float, String, Vector2,
		Vector3, Array and Dictionary. However, if you want to review your data intact, you must use one of the databases
		following: 2 to 10 or 16, knowing that the data has been previously encoded in one of the recommended bases.
	@Parametters:
		String | Array | Dictionary coded_type: Contains the coded value of a type.
		int bit: What is the bit that have been used to coded the type ?
		bool security: Is the coded type have a security ?
"""
static func any_from_bit (coded_type, bit: int, security: bool = true):
	# Checks bit value.
	if bit > 1:
		# For Array and Dictionary types value.
		if coded_type is Array: return _list_from_bit (coded_type, bit, security); elif coded_type is Dictionary: return _dic_from_bit (coded_type, bit, security);
		# For basics types values.
		else: return _type_from_bit (coded_type, bit, security);
	# Warning message.
	else: _output ("The value of the bit parameter mustn't be less than 2.", Message.WARNING);

"""
	@Description: Generates a key as a string format.
	@Parameters:
		int max_size: Contains the max value of the key that will be generated.
"""
static func generate_key (max_size: int = 16) -> String:
	# If the key size is not less than 0.
	if max_size > 0:
		# Contains the generated key and lists all availables characters.
		var generated_key: String = String (''); var characters: PoolStringArray = PoolStringArray (['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'm',
			'n', 'l', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '!', '@', '#', '%', '&', '^', '*', '_', '+', '=', '|', '<', ']', '`', '²', 'ô',
			'ŵ', '/', '>', '(', 'é', '\'', 'ê', 'ŝ', 'ĉ', '.', '$', ')', 'è', '"', 'ŷ', 'ĝ', ';', '?', '~', '{', 'à', '\\', 'û', 'ĥ', ',', ':', '[', '}', '-', 'â',
			'î', 'ĵ', 'ù', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'M', 'N', 'L', 'O', 'P', 'A', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'Ô', 'Ŵ',
			'Ê', 'Ŝ', 'Ĉ', 'Ŷ', 'Ĝ', 'Û', 'Ĥ', 'Â', 'Î', 'Ĵ', '8', '9', 'é'.to_upper (), 'è'.to_upper (), 'à'.to_upper (), 'ù'.to_upper (), '0', '1', '2', '3', '4',
			'5', '6', '7'
		]);
		# Generating each character of the key.
		for _p in range (0, max_size):
			# Makes a randomize.
			randomize (); var chr_gen: String = characters  [(randi () % characters.size ())];
			# For invert and normal generation.
			if randi () % 2 == 0: generated_key = generated_key.insert (0, chr_gen); else: generated_key = generated_key.insert ((len (generated_key) - 1), chr_gen);
		# Returns the final value.
		return generated_key;
	# Returns an empty value.
	else: return "Null";

"""
	@Description: Generates some keys as string format.
	@Parameters:
		int key_count: Contains the total key count that will be generated.
		int max_size: Contains the max value of each key.
"""
static func generate_keys (key_count: int, max_size: int = 16) -> PoolStringArray:
	# Contains all generated keys.
	var keys: PoolStringArray = PoolStringArray ([]); var key: String = String ('');
	# If key count is equal to zero.
	if key_count > 0:
		# Generating keys.
		for _v in range (0, key_count):
			# If the keys list has a key and checks whether the generated key is equal the last generated key in the keys list.
			if keys.size () == 0: key = generate_key (max_size); else: while key == keys [(keys.size () - 1)]: key = generate_key (max_size); keys.append (key);
		# Returns the final value.
		return keys;
	# Returns an empty value.
	else: return keys;

"""
	@Description: Transforms a bytes list into text.
	@Parameters:
		PoolByteArray bytes: Contains all bytes of transformed characters.
"""
static func bytes_to_text (bytes: PoolByteArray) -> String:
	# If the bytes array is not less than 0.
	if bytes.size () > 0:
		# Contains the normal text of the given bytes array.
		var text: String = ''; for byte in bytes: text += char (byte); return text;
	# Otherwise.
	else: _output ("The bytes array size mustn't be empty.", Message.WARNING); return "Null";

"""
	@Description: Generates a screenshot from the active camera.
	@Parameters:
		Node object: Which knot will be considered to make the differents operations ?
		Dictionary data: Contains the different configurations to be made on the capture, once generated. This dictionary supports keys following:
			Vector2 size = Vector2 (-1, -1): Contains the screenshot resolution.
			int quality = Image.INTERPOLATE_CUBIC: Contains the image resolution quality. he supported constants are already defined on Godot image class.
			int skrink = 0: Control the image shrinkage.
			int comp_mode = MegaAssets.ImageCompression.ETC: What mode of compression do you want to adopt to compress the image ? The possibles values ​​are:
				-> MegaAssets.ImageCompression.NONE or 0: No compression will be applyed.
				-> MegaAssets.ImageCompression.ETC or 1: Compression with ETC method.
				-> MegaAssets.ImageCompression.ETC2 or 2: Compression with ETC2 method.
				-> MegaAssets.ImageCompression.S3TC or 3: Compression with S3TC method.
			int comp_source = Image.COMPRESS_SOURCE_GENERIC: From what compression source the image will compress ? See Godot Image class constants.
				Use this key only if the image needs to be compressed.
			int format = MegaAssets.ImageFormat.RGBAH: Contains the format of the image. Use this key only if the image needs to be compressed.
				The possibles values are:
				-> MegaAssets.ImageFormat.RH or 0: Converts image to RH format.
				-> MegaAssets.ImageFormat.RF or 1: Converts image to RF format.
				-> MegaAssets.ImageFormat.RGH or 2: Converts image to RGH format.
				-> MegaAssets.ImageFormat.RGF or 3: Converts image to RGF format.
				-> MegaAssets.ImageFormat.RGBH or 4: Converts image to RGBH format.
				-> MegaAssets.ImageFormat.RGBF or 5: Converts image to RGBF format.
				-> MegaAssets.ImageFormat.RGBAH or 6: Converts image to RGBAH format.
				-> MegaAssets.ImageFormat.RGBAF or 7: Converts image to RGBAF format.
				-> MegaAssets.ImageFormat.RGBA4444 or 8: Converts image to RGBA4444 format.
				-> MegaAssets.ImageFormat.RGBA5551 or 9: Converts image to RGBA5551 format.
				-> MegaAssets.ImageFormat.RGBA9995 or 10: Converts image to RGBA9995 format.
			float ratio = 100.0: What rate of compression do you want to apply to the image ? Use this key only if the image must be compressed.
"""
static func get_screen_shot (object: Node, data: Dictionary = Dictionary ({})) -> ImageTexture:
	# Getting the current game viewport.
	var app = object.get_viewport ();
	# Checks the viewport value.
	if app is Viewport:
		# Getting the generated image from the viewport and the passed screenshot size.
		var img: Image = app.get_texture ().get_data (); img.flip_y (); data.size = data.size if data.has ("size") and data.size is Vector2 else Vector2 (-1, -1);
		# Getting the passed screenshot quality.
		data.quality = int (data.quality) if data.has ("quality") && is_number (data.quality) else Image.INTERPOLATE_CUBIC;
		# Resizes the given image with the available size.
		data.size = Vector2 ((app.size.x if data.size.x < 0 else data.size.x), (app.size.y if data.size.y < 0 else data.size.y));
		# Resizing the generated screenshot.
		img.resize (int (data.size.x), int (data.size.y), (0 if data.quality < 0 else (4 if data.quality > 4 else data.quality)));
		# Getting the passed screenshot skrink count.
		data.skrink = int (data.skrink) if data.has ("skrink") && is_number (data.skrink) else 0;
		# Apply the skrink count.
		for _f in range (data.skrink if data.skrink >= 0 else 0): img.shrink_x2 (); var screen_shot: ImageTexture = ImageTexture.new ();
		# Getting the passed screenshot compress mode.
		data.mode = int (data.mode) if data.has ("mode") && is_number (data.mode) else Image.COMPRESS_ETC;
		# Filters the given screenshot compression mode.
		var filter_compress_mode: int = get_real_image_compression (data.mode);
		# Checks compression mode.
		if filter_compress_mode >= 0:
			# Converting the generated screenshot texture to the passed image format.
			img.convert (get_real_image_format (int (data.format) if data.has ("format") && is_number (data.format) else Image.FORMAT_RGBAH));
			# Getting the passed screenshot compress source.
			data.source = int (data.source) if data.has ("source") && is_number (data.source) else Image.COMPRESS_SOURCE_GENERIC;
			# Getting the passed screenshot compress ratio.
			data.ratio = float (data.ratio) if data.has ("ratio") && is_number (data.ratio) && data.ratio >= 0 else 100.0;
			# Compress the generated screenshot image.
			if img.compress (filter_compress_mode, (0 if data.source < 0 else (2 if data.source > 2 else data.source)), data.ratio) == OK:
				# Creating a texture with the given screenshot.
				screen_shot.create_from_image (img);
			# An error has been thrown on screenshot compression.
			else: _output ("Failed to compress the generated screenshot texture.", Message.WARNING);
		# Otherwise.
		else: screen_shot.create_from_image (img); return screen_shot;
	# Otherwise.
	else: return ImageTexture.new ();

"""
	@Description: Creates a screenshot from the active camera.
	@Parameters:
		String path: Where want you put your screen shot ? The required format of your image must be (.png).
		Node object: Which knot will be considered to make the differents operations ?
		Dictionary data: Contains the different configurations to be made on the capture, once generated. For more information, consult the documentation
			about "get_screen_shot ()" method.
		float delay: What is the timeout before creating of the screen shot ?
"""
static func create_screen_shot (path: String, object: Node, data: Dictionary = Dictionary ({}), delay: float = 0.0) -> void:
	# Corrects the passed path value.
	path = path.replace ('\\', '/').replace (' ', '');
	# If the path value is not empty.
	if not path.empty ():
		# Checks the given screen shot format.
		if path.ends_with (".png"):
			# The game is running.
			if !Engine.editor_hint:
				# If the game is not initialised.
				if delay <= 0.0 && not is_game_initialised ():
					# Waiting for game initialization.
					yield (object.get_tree (), "idle_frame"); yield (object.get_tree (), "idle_frame");
				# Otherwise.
				elif delay > 0.0: yield (object.get_tree ().create_timer (delay), "timeout");
			# Getting the current viewport screenshot image.
			var screen_shot: Image = (get_screen_shot (object, data) as Texture).get_data ();
			# Creating the screenshot on the disk.
			if screen_shot.save_png (path.get_file ()) != OK: _output ("Failed to create the game screenshot !", Message.WARNING);
		# Error message.
		else: _output ("The required format of a screenshot is (.png).", Message.ERROR);
	# Warning message.
	else: _output ("The path shouldn't be empty.", Message.WARNING);

"""
	@Description: Transforms a hexadecimal value into raw value from a string value.
	@Parameters:
		String string: Contains a hex value as string format.
"""
static func hex_to_raw (string: String) -> String:
	# Initializes raw arrays.
	var out: PoolByteArray = PoolByteArray (); var hs: String = "0x00";
	# Converting the given string to raw.
	for i in range (0, string.length (), 2):
		hs [2] = string [i]; hs [3] = string [(i + 1)]; out.append (hs.hex_to_int () & 0xff);
	# Returns the final result.
	return bytes_to_text (out);

"""
	@Description: Transforms a raw value into a hexadecimal value from a string value.
	@Parameters:
		String string: Contains a raw value as string format.
"""
static func raw_to_hex (string: String) -> String:
	# Contains the string value as bytes format and returns the final value.
	var bytes: PoolByteArray = string.to_ascii (); var hex: String = String (''); for i in range (bytes.size ()): hex += ("%02x" % (bytes [i] & 0xff)); return hex;

"""
	@Description: Returns a rotl64 of two integers.
	@Parameters:
		int n: Contains an integer value.
		int r: Contains an integer value.
"""
static func rotl64 (n: int, r: int) -> int:
	# Preserves the sign bit so we need to mask to perform a logical shift on the second part.
	r = (r & 0x3f); if (r == 0): return n; return (n << r) | ((n >> (64 - r)) & _ROTL64_MASK [(64 - r)]);

"""
	@Description: Returns a rotr32 of two integers.
	@Parameters:
		int n: Contains an integer value.
		int r: Contains an integer value.
"""
static func rotr32 (n: int, r: int) -> int:
	# Returns the final value.
	n = (n & _B32); r = (r & 0x1f); if (r == 0): return n; return ((n >> r) & _B32) | ((n << (32 - r)) & _B32);

"""
	@Description: Encrypts an input buffer with a key. This method support severals encryption methods.
	@Parameters:
		Variant input: A buffer that will be encrypted.
		String key: Contains the key that will be used to encrypt the passed input (16/32 bytes).
		int method: which method that will be used to encrypt the passes input ? The possibles values are:
			-> MegaAssets.EncryptionMethod.AES or 0: Data encryption with AES method.
			-> MegaAssets.EncryptionMethod.ARCFOUR or 1: Data encryption with ARCFOUR method.
			-> MegaAssets.EncryptionMethod.CHACHA or 2: Data encryption with CHACHA method.
		int schema: Contains the way that will be used to encrypt the passed input. The possibles values are:
			-> MegaAssets.EncryptionSchema.BASE64 or 0: Data encryption with Base64 schema.
			-> MegaAssets.EncryptionSchema.HEXADECIMAL or 1: Data encryption with Hexadecimal schema.
			-> MegaAssets.EncryptionSchema.RAW or 2: Data encryption with Raw schema.
"""
static func encrypt (input, key: String = String (''), method: int = EncryptionMethod.CHACHA, schema: int = EncryptionSchema.BASE64) -> String:
	# The key length is out of range.
	if len (key) > 32: _output ("The key size must be 128/256 bits (16/32 bytes).", Message.ERROR);
	# No errors detected.
	else:
		# Corrects the passed key.
		if len (key) < 16: while key.length () < 16: key += ' '; elif len (key) > 16 && len (key) < 32: while key.length () < 32: key += ' ';
		# For AES method.
		if method <= EncryptionMethod.AES:
			# For BASE 64 schema.
			if schema <= EncryptionSchema.BASE64: return Marshalls.raw_to_base64 (encrypt (str (input), key, method, 2).to_ascii ());
			# For HEX schema.
			elif schema == EncryptionSchema.HEXADECIMAL: return raw_to_hex (encrypt (str (input), key, method, 2));
			# For RAW schema.
			else:
				# Converts the input into a PoolByteArray and calculates the padding.
				input = str (input).to_ascii (); var padding: int = (16 - (input.size () % 16)); for _k in range (padding): input.append (padding);
				# Corrects the passed key value.
				var keys: Array = _key_schedule (key.to_ascii ()); var encrypted_value: PoolByteArray = PoolByteArray ([]);
				# Encrypts the given input with AES encryption method.
				for p in range (int (len (input) / 16.0)):
					# Decrypting the current block.
					var block: PoolByteArray = input.subarray ((p * 16), (p * 16 + 15)); block = _encrypt_block (block, keys); encrypted_value.append_array (block);
				# Returns the final value as string format.
				return bytes_to_text (encrypted_value);
		# For ARCFOUR method.
		elif method == EncryptionMethod.ARCFOUR:
			# For BASE 64 schema.
			if schema <= EncryptionSchema.BASE64: return Marshalls.raw_to_base64 (encrypt (str (input), key, method, 2).to_ascii ());
			# For HEX schema.
			elif schema == EncryptionSchema.HEXADECIMAL: return raw_to_hex (encrypt (str (input), key, method, 5));
			# For RAW schema.
			else:
				# Converts the input and key into a PoolByteArray.
				var data: Array = [str (input).to_ascii (), key.to_ascii ()];
				# Input and key length.
				var il: int = data [0].size (); var kl: int = data [1].size (); var op: PoolByteArray = PoolByteArray ();
				# Resizing the output.
				op.resize (il); var i: int = 0; var j: int = 0; var k: int = 0; var t: int = 0; var s: PoolByteArray = PoolByteArray (); s.resize (256);
				# Key schedule.
				for c in range (256):
					i = c; s [i] = i;
				for q in range (256):
					i = q; j = (j + s [i] + data [1] [(i % kl)]) & 0xff; t = s [i]; s [i] = s [j]; s [j] = t;
				# De/encrypts input => output. Optionally drop some initial keystream bytes.
				i = 0; j = 0; k = 0;
				for p in range (il):
					i = (i + 1) & 0xff; j = (j + s [i]) & 0xff; t = s [i]; s [i] = s [j]; s [j] = t;
					if (p >= 0):
						op [k] = data [0] [k] ^ s [(s [i] + s [j]) & 0xff]; k += 1;
				# Returns the final value.
				return bytes_to_text (op);
		# For CHACHA method.
		else:
			# For BASE64 schema.
			if schema <= EncryptionSchema.BASE64: return Marshalls.raw_to_base64 (encrypt (str (input), key, method, 2).to_ascii ());
			# For HEX schema.
			elif schema == EncryptionSchema.HEXADECIMAL: return raw_to_hex (encrypt (str (input), key, method, 5));
			# For RAW schema.
			else:
				# Converts the input into a PoolByteArray.
				input = str (input).to_ascii (); var ky: PoolByteArray = PoolByteArray ([]);
				var op: PoolByteArray = PoolByteArray ([]); op.resize (len (input)); var i: int = 0;
				var chacha: _ChaChaBlockCipher = _ChaChaBlockCipher.new (); var ki: int = 64; var ct: int = 1;
				while (i < len (input)):
					# Generates new key material for each block.
					if (ki == 64):
						ky = chacha.chacha20_block (key.to_ascii (), "AhjlNp3Mu8ycTfQ$".to_ascii (), ct, 20); ki = 0; ct += 1;
					# Xor key with input to get output.
					op [i] = (input [i] ^ ky [ki]); i += 1; ki += 1;
				# Returns the final value.
				return bytes_to_text (op);
	# Returns a null value whether there are some errors.
	return "Null";

"""
	@Description: Decrypts an input buffer with a key. This method support severals encryption methods.
	@Parameters:
		Variant input: A buffer that will be decrypted. It would have contained an encrypted value. His length must be a multiple of 16. 
		String key: Contains the key that had been used to encrypt the passed input last time with encrypt function (16/32 bytes).
		int method: which method had been used to encrypt the passed input last time with encrypt function ? The possibles values are:
			-> MegaAssets.EncryptionMethod.AES or 0: Data decryption with AES method.
			-> MegaAssets.EncryptionMethod.ARCFOUR or 1: Data decryption with ARCFOUR method.
			-> MegaAssets.EncryptionMethod.CHACHA or 2: Data decryption with CHACHA method.
		int schema: Contains the way that had been used to encrypt the passed input last time with encrypt function. The possibles values are:
			-> MegaAssets.EncryptionSchema.BASE64 or 0: Data decryption with Base64 schema.
			-> MegaAssets.EncryptionSchema.HEXADECIMAL or 1: Data decryption with HexADECIMAL schema.
			-> MegaAssets.EncryptionSchema.RAW or 2: Data decryption with Raw schema.
"""
static func decrypt (input: String, key: String = String (''), method: int = EncryptionMethod.CHACHA, schema: int = EncryptionSchema.BASE64):
	# The key length is out of range.
	if len (key) > 32: _output ("The key size must be 128/256 bits (16/32 bytes).", Message.ERROR);
	# No errors detected.
	else:
		# Corrects the passed key.
		if len (key) < 16: while key.length () < 16: key += ' '; elif len (key) > 16 && len (key) < 32: while key.length () < 32: key += ' ';
		# For AES method.
		if method <= EncryptionMethod.AES:
			# For BASE64 schema.
			if schema <= EncryptionSchema.BASE64: return decrypt (Marshalls.base64_to_raw (input).get_string_from_ascii (), key, method, 2);
			# For HEX schema.
			elif schema == EncryptionSchema.HEXADECIMAL: return decrypt (hex_to_raw (input), key, method, 2);
			# For RAW schema.
			else:
				# Converts the given encrypted value into a PoolByteArray.
				var encrypted_bytes: PoolByteArray = input.to_ascii ();
				# Converts the passed key into a valid PoolByteArray format.
				var keys: Array = _key_schedule (key.to_ascii ()); var decrypted_bytes: PoolByteArray = PoolByteArray ([]);
				# Starting decryption.
				for i in range (int (encrypted_bytes.size () / 16.0)):
					# Initializes cipher and plain blocks bytes.
					var cipher_block: PoolByteArray = encrypted_bytes.subarray ((i * 16), (i * 16 + 15));
					var plain_block: PoolByteArray = _decrypt_block (cipher_block, keys); decrypted_bytes.append_array (plain_block);
				# Calculates encrypted bytes padding since encryption.
				var padding_value: int = decrypted_bytes [(len (decrypted_bytes) - 1)] if (len (decrypted_bytes) - 1) >= 0 else 0;
				# The padding is not valid.
				if padding_value < 1 or padding_value > 16: _output ("Decryption failed: The input length is too big.", Message.WARNING);
				# Otherwise.
				else:
					# Calculates the next padding.
					var padding: PoolByteArray = decrypted_bytes.subarray ((decrypted_bytes.size () - padding_value), (decrypted_bytes.size () - 1));
					# Gets padding value and check his validation.
					for i in padding: if i != padding_value: _output ("Decryption failed: inconsistent padding value.", Message.WARNING);
					decrypted_bytes = decrypted_bytes.subarray (0, (decrypted_bytes.size () - (padding_value - 1)));
				# Returns the decrypted value as PoolByteArray format.
				return get_variant (bytes_to_text (decrypted_bytes));
		# For CHACHA and ARCFOUR method.
		else:
			# For BASE64 schema.
			if schema <= EncryptionSchema.BASE64: return decrypt (Marshalls.base64_to_raw (input).get_string_from_ascii (), key, method, 2);
			# For HEX schema.
			elif schema == EncryptionSchema.HEXADECIMAL: return decrypt (hex_to_raw (input), key, method, 5);
			# For RAW schema.
			else: return get_variant (encrypt (input, key, method, 2));

"""
	@Description: Hashs an input buffer with a key. This method support severals hashing methods.
	@Parameters:
		Variant input: A buffer that will be hashed.
		String key: Contains the key that will be used to hash the passed input (16 bytes).
		int method: which method that will be used to hash the passes input ? The possibles values are:
			-> MegaAssets.HashingMethod.SIP or 0: Data hashing with SIP method.
			-> MegaAssets.HashingMethod.SHA256 or 1: Data hashing with SHA256 method.
			-> MegaAssets.HashingMethod.HASHMAC_SHA256 or 2: Data hashing with HASHMAC method.
			-> MegaAssets.HashingMethod.GODOT_SHA256 or 3: Data hashing with Godot SHA256 method.
			-> MegaAssets.HashingMethod.MD5 or 4: Data hashing with MD5 method.
			-> MegaAssets.HashingMethod.AES or 5: Data hashing with AES method.
			-> MegaAssets.HashingMethod.ARCFOUR or 6: Data hashing with ARCFOUR method.
			-> MegaAssets.HashingMethod.CHACHA or 7: Data hashing with SIP method.
		int schema: Contains the way that will be used to hash the passed input. The possibles values are:
			-> MegaAssets.EncryptionSchema.BASE64 or 0: Data hashing with Base64 schema.
			-> MegaAssets.EncryptionSchema.HEXADECIMAL or 1: Data hashing with Hex schema.
			-> MegaAssets.EncryptionSchema.RAW or 2: Data hashing with Raw schema.
"""
static func hash_var (input, key: String = String (''), method: int = HashingMethod.SIP, schema: int = EncryptionSchema.BASE64) -> String:
	# The key length is out of range.
	if len (key) > 16: _output ("The key size must be 128 bits (16 bytes).", Message.ERROR);
	# No errors detected.
	else:
		# Transfroms the input into a string format.
		input = str (input); if key.length () < 16: while key.length () < 16: key += ' ';
		# For SIP, SHA256, HASHMACSHA256, AES.
		if method <= HashingMethod.SIP || method == HashingMethod.SHA256 || method == HashingMethod.HASHMAC_SHA256 || method == HashingMethod.AES:
			# For BASE64 schema.
			if schema <= EncryptionSchema.BASE64: return Marshalls.raw_to_base64 (hash_var (input, key, method, 2).to_ascii ());
			# For HEX schema.
			elif schema == EncryptionSchema.HEXADECIMAL: return raw_to_hex (hash_var (input, key, method, 2));
		# For SIP method.
		if method <= HashingMethod.SIP:
			# For RAW schema.
			if schema >= EncryptionSchema.RAW:
				# Converts the input and his key into a PoolByteArray.
				var data: Array = Array ([input.to_ascii (), key.to_ascii ()]);
				# Intialization.
				var v0: int = 0x736f6d65707; var v1: int = 0x646f72616e6; var v2: int = 0x6c7967656e6; var v3: int = 0x74656462797; var k0: int = 0; var k1: int = 0;
				for i in range (8):
					k0 = (k0 << 8) | data [1] [(7 - i)]; k1 = (k1 << 8) | data [1] [(7 - i + 8)];
				v3 ^= k1; v2 ^= k0; v1 ^= k1; v0 ^= k0; if (8 == 16): v1 ^= 0xee; var m: int;
				# Main processing.
				var ilen: int = data [0].size (); var imax: int = (ilen - (ilen % 8)); var ilft: int = (ilen & 7);
				for i in range (0, imax, 8):
					m = ((data [0] [i] & 0xff) | ((data [0] [(i + 1)] & 0xff) << 8) | ((data [0] [(i + 2)] & 0xff) << 16) |
					((data [0] [(i + 3)] & 0xff) << 24) | ((data [0] [(i + 4)] & 0xff) << 32) | ((data [0] [(i + 5)] & 0xff) << 40)
					| ((data [0] [(i + 6)] & 0xff) << 48) | ((data [0] [(i + 7)] & 0xff) << 56)); v3 ^= m;
					for _j in range (2):
						v0 += v1; v1 = rotl64 (v1, 13); v1 ^= v0; v0 = rotl64 (v0, 32); v2 += v3; v3 = rotl64 (v3, 16); v3 ^= v2; v0 += v3; v3 = rotl64 (v3, 21);
						v3 ^= v0; v2 += v1; v1 = rotl64 (v1, 17); v1 ^= v2; v2 = rotl64 (v2, 32);
					v0 ^= m;
				# Final block processing.
				var b: int = 0; for i in range (ilft, 0, -1): b = (b << 8) | (data [0] [(imax + i - 1)] & 0xff); b |= (ilen & 0xff) << 56; v3 ^= b;
				for _j in range (2):
					v0 += v1; v1 = rotl64 (v1, 13); v1 ^= v0; v0 = rotl64 (v0, 32); v2 += v3; v3 = rotl64 (v3, 16); v3 ^= v2; v0 += v3; v3 = rotl64 (v3, 21); v3 ^= v0;
					v2 += v1; v1 = rotl64 (v1, 17); v1 ^= v2; v2 = rotl64 (v2, 32);
				v0 ^= b; if (8 == 16): v2 ^= 0xee; else: v2 ^= 0xff;
				# Dround loop.
				for _j in range (4):
					v0 += v1; v1 = rotl64 (v1, 13); v1 ^= v0; v0 = rotl64 (v0, 32); v2 += v3; v3 = rotl64 (v3, 16); v3 ^= v2; v0 += v3; v3 = rotl64 (v3, 21); v3 ^= v0;
					v2 += v1; v1 = rotl64 (v1, 17); v1 ^= v2; v2 = rotl64 (v2, 32);
				b = (v0 ^ v1 ^ v2 ^ v3);
				# Creates and returns the hash.
				var op: PoolByteArray = PoolByteArray (); for i in _U64_SHIFTS_INV: op.append ((b >> i) & 0xff);
				if (8 == 16):
					v1 ^= 0xdd;
					for _j in range (4):
						v0 += v1; v1 = rotl64 (v1, 13); v1 ^= v0; v0 = rotl64 (v0, 32); v2 += v3; v3 = rotl64 (v3, 16); v3 ^= v2; v0 += v3;
						v3 = rotl64 (v3, 21); v3 ^= v0; v2 += v1; v1 = rotl64 (v1, 17); v1 ^= v2; v2 = rotl64 (v2, 32);
					b = (v0 ^ v1 ^ v2 ^ v3); for i in _U64_SHIFTS_INV: op.append ((b >> i) & 0xff);
				# If an error has been detected on hashing.
				if op.size () != 8: _output ("An error has been detected on hashing.", Message.WARNING);
				# Otherwise.
				else: return bytes_to_text (op);
		# For SHA256 method.
		elif method == HashingMethod.SHA256:
			# For RAW schema.
			if schema >= EncryptionSchema.RAW:
				# Converts the given input into a PoolBytesArray.
				input = input.to_ascii ();
				# First 32 bits of the fractional parts of the square roots of the first 8 primes 2..19.
				var h0: int = 0x6a09e667; var h1: int = 0xbb67ae85; var h2: int = 0x3c6ef372; var h3: int = 0xa54ff53a;
				var h4: int = 0x510e527f; var h5: int = 0x9b05688c; var h6: int = 0x1f83d9ab; var h7: int = 0x5be0cd19;
				# Pre-process (pad) the input. Remember PoolByteArray is passed by value so it is safe to modify the input.
				var l: int = (input.size () * 8); input.append (0x80); while ((input.size () + 8) & 0x3f != 0): input.append (0x00);
				# Appends L as a 64-bit big-endian integer, making the total post-processed length a multiple of 512 bits.
				for i in _U64_SHIFTS: input.append ((l >> i) & 0xff);
				# If an error has been detected.
				if input.size () & 0x3f != 0: _output ("An error has been detected on hashing.", Message.WARNING);
				# Otherwise.
				else:
					# Process the message in successive 512 bits chunks.
					var w: Array = Array ([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
						0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
					]); var j: int; var s0: int; var s1: int; var t1: int; var t2: int; var ch: int; var mj: int;
					for pos in range (0, input.size (), 64):
						# Creates a 64-entry message schedule array w [0..63] of 32-bit words.
						for i in range (0, 16):
							j = pos + (i * 4); w [i] = ((input [j] << 24) | (input [(j + 1)] << 16) | (input [(j + 2)] << 8) | input [(j + 3)]); pass;
						# Extends the first 16 words into the remaining 48 words w [16..63] of the message schedule array.
						for i in range (16, 64):
							s0 = rotr32 (w [(i - 15)],  7) ^ rotr32 (w [(i - 15)], 18) ^ (w [(i - 15)] >>  3);
							s1 = rotr32 (w [(i - 2)], 17) ^ rotr32 (w [(i - 2)], 19) ^ (w [(i - 2)] >> 10); w [i] = ((w [(i - 16)] + s0 + w [(i - 7)] + s1) & _B32); pass;
						# Initializes working variables to current hash value.
						var a: int = h0; var b: int = h1; var c: int = h2; var d: int = h3; var e: int = h4; var f: int = h5; var g: int = h6; var h: int = h7;
						# Compression function main loop.
						for i in range (64):
							s1 = (rotr32 (e, 6) ^ rotr32 (e, 11) ^ rotr32 (e, 25)); ch = ((e & f) ^ ((~e) & g)); t1 = ((h + s1 + ch + _RK [i] + w [i]) & _B32);
							s0 = (rotr32 (a, 2) ^ rotr32 (a, 13) ^ rotr32 (a, 22)); mj = ((a & b) ^ (a & c) ^ (b & c)); t2 = (s0 + mj) & _B32; h = g; g = f; f = e;
							e = ((d + t1) & _B32); d = c; c = b; b = a; a = ((t1 + t2) & _B32); pass;
						# Updates the hash value with the compressed chunk.
						h0 = (h0 + a) & _B32; h1 = (h1 + b) & _B32; h2 = (h2 + c) & _B32; h3 = (h3 + d) & _B32;
						h4 = (h4 + e) & _B32; h5 = (h5 + f) & _B32; h6 = (h6 + g) & _B32; h7 = (h7 + h) & _B32; pass;
					# Produces the final hash value (big-endian).
					var digest: PoolByteArray = PoolByteArray (); digest.resize (32); j = 0;
					for i in _U32_SHIFTS:
						digest [j] = (h0 >> i) & 0xff; digest [(j + 4)] = (h1 >> i) & 0xff; digest [(j + 8)] = (h2 >> i) & 0xff;
						digest [(j + 12)] = (h3 >> i) & 0xff; digest [(j + 16)] = (h4 >> i) & 0xff;
						digest [(j + 20)] = (h5 >> i) & 0xff; digest [(j + 24)] = (h6 >> i) & 0xff; digest [(j + 28)] = (h7 >> i) & 0xff; j += 1;
					# Returns the final value.
					return bytes_to_text (digest);
		# For HASHMAC SHA256 method.
		elif method == HashingMethod.HASHMAC_SHA256:
			# For RAW schema.
			if schema >= EncryptionSchema.RAW:
				# Keys shorter than hash size are padded to hash size by padding with zeros on the right.
				if key.length () > 64: key = hash_var (key, '', 1, 2); var ky: PoolByteArray = key.to_ascii (); while (ky.size () < 64): ky.append (0x00);
				# If an error has been detected.
				if ky.size () != 64: _output ("An error has been detected on hashing.", Message.WARNING);
				# Otherwise.
				else:
					# Generates inner and outer padding keys.
					var outer: PoolByteArray = PoolByteArray (); var inner: PoolByteArray = PoolByteArray ();
					for i in range (ky.size ()):
						# Updates outer and inner.
						outer.append (ky [i] ^ 0x5c); inner.append (ky [i] ^ 0x36);
					# Contains the entry to passed.
					var entry: String = hash_var (bytes_to_text (inner + input.to_ascii ()), '', 1, 2);
					# Returns the final result.
					return hash_var (bytes_to_text (outer + entry.to_ascii ()), '', 1, 2);
		# For Godot SHA256 and MD5 methods.
		elif method == HashingMethod.GODOT_SHA256: return input.sha256_text (); elif method == HashingMethod.MD5: return input.md5_text ();
		# For AES method.
		elif method == HashingMethod.AES:
			# BASE64 schema.
			if schema <= EncryptionSchema.BASE64: return encrypt (input, key, EncryptionMethod.AES, EncryptionSchema.BASE64);
			# HEX schema.
			elif schema == EncryptionSchema.HEXADECIMAL: return encrypt (input, key, EncryptionMethod.AES, EncryptionSchema.HEXADECIMAL);
			# RAW schema.
			else: return encrypt (input, key, EncryptionMethod.AES, EncryptionSchema.RAW);
		# For ARCFOUR method.
		elif method == HashingMethod.ARCFOUR:
			# BASE64 schema.
			if schema <= EncryptionSchema.BASE64: return encrypt (input, key, EncryptionMethod.ARCFOUR, EncryptionSchema.BASE64);
			# HEX schema.
			elif schema == EncryptionSchema.HEXADECIMAL: return encrypt (input, key, EncryptionMethod.ARCFOUR, EncryptionSchema.HEXADECIMAL);
			# RAW schema.
			else: return encrypt (input, key, EncryptionMethod.ARCFOUR, EncryptionSchema.RAW);
		# For CHACHA method.
		else:
			# BASE64 schema.
			if schema <= EncryptionSchema.BASE64: return encrypt (input, key, EncryptionMethod.CHACHA, EncryptionSchema.BASE64);
			# HEX schema.
			elif schema == EncryptionSchema.HEXADECIMAL: return encrypt (input, key, EncryptionMethod.CHACHA, EncryptionSchema.HEXADECIMAL);
			# RAW schema.
			else: return encrypt (input, key, EncryptionMethod.CHACHA, EncryptionSchema.RAW);
	# Returns a fake value.
	return "Null";

"""
	@Description: Returns the conresponding type from his string format. The supported types are: Integer, Float, Vector2, Vector3,
		String, Boolean, Array and Dictionary. This method can take nested dictionaires and arrays.
	@Parameters:
		String variant: Contains a variant as string format.
"""
static func get_variant (variant: String):
	# Removes left and right spaces.
	variant = str_replace (variant, PoolStringArray (["\n", "\t", "\a", "\b", "\r", "\v", "\f"]), '').lstrip (' ').rstrip (' ');
	# If the variant value is not empty.
	if variant != null and not variant.empty ():
		# For integer format.
		if variant.find (' ') == -1 and is_full_numbers (variant.lstrip ('-').lstrip ('+')): return int (variant);
		# For vector format.
		elif variant.begins_with ('(') and variant.ends_with (')') and variant.find (',') != -1:
			# Corrects the vector representation.
			variant = variant.lstrip ('(').rstrip (')'); var vect: PoolStringArray = variant.split (',');
			# For vector2 fand vector3 ormat.
			if vect.size () == 2: return Vector2 (float (vect [0]), float (vect [1])); else: return Vector3 (float (vect [0]), float (vect [1]), float (vect [2]));
		# For boolean format.
		elif variant.to_lower () == "false" or variant.to_lower () == "true": return (true if variant.to_lower () == "true" else false);
		# For float format.
		elif variant.find ('.') != -1 && is_full_numbers (variant.split ('.') [0].lstrip ('-').lstrip ('+')) && is_full_numbers (variant.split ('.') [1]):
			# Splits the current string in two parts.
			var nums: PoolStringArray = variant.split ('.');
			# Returns the final result.
			return float (str (int (nums [0].lstrip (' ').rstrip (' '))) + '.' + str (int (nums [1].lstrip (' ').rstrip (' '))));
		# For exp float format.
		elif variant.find ('e') != -1 && is_number (get_variant (variant.split ('e') [0])) && is_number (get_variant (variant.split ('e') [1])):
			# Returns the final result after calculating.
			return float (variant);
		# For an array.
		elif variant.begins_with ('[') and variant.ends_with (']'):
			# Contains the final result.
			var result: Array = Array ([]); variant [0] = ''; variant [(len (variant) - 1)] = '';
			# The array is not empty.
			if variant.find (',') != -1:
				# Checks the given array.
				if variant.count ('[') == variant.count (']') && variant.count ('{') == variant.count ('}') && variant.count ('(') == variant.count (')'):
					# Initializes elements.
					variant = variant.replace (", ", ','); var state: int = -1; var nested: int = 0; var varit: PoolStringArray = variant.split (',');
					# Converts the passed variant into an array.
					for j in varit.size ():
						# Removes left and right spaces.
						varit [j] = varit [j].lstrip (' ').rstrip (' '); varit [j] = ' ' if varit [j].empty () else varit [j];
						# Contains the last character of the current string and gets the preview index.
						var last_chr: String = varit [j] [(varit [j].length () - 1)]; var k: int = (j - 1);
						# Checks all main characters from the current string.
						var re: Array = Array ([varit [j].find ('(') != -1, varit [j].find_last (')') != -1, varit [j].find ('[') != -1,
							varit [j].find_last (']') != -1, varit [j].find ('{') != -1, varit [j].find_last ('}') != -1
						]);
						# For other variables.
						if state == -1 && !re [0] && !re [1] && !re [2] && !re [3] && !re [4] && !re [5]: result.append (get_variant (varit [j]));
						# For vectors.
						if state == -1 || state == 0:
							# Checks whether there are some characters '(' and ')' on the current string and generate the vector.
							nested += varit [j].count ('(') if varit [j] [0] == '(' else 0; nested -= varit [j].count (')') if last_chr == ')' else 0;
							varit [j] = (varit [k] + ',' + varit [j]) if state == 0 && k >= 0 && nested >= 0 && varit [k] [0] == '(' else varit [j];
							# Vector is detected.
							state = 0 if nested > 0 else -1; last_chr = varit [j] [(varit [j].length () - 1)];
							if nested == 0 && varit [j] [0] == '(' && last_chr == ')': result.append (get_variant (varit [j]));
						# For nested list.
						if state == -1 || state == 1:
							# Checks whether there are some characters '[' and ']' on the current string and generate array.
							nested += varit [j].count ('[') if varit [j] [0] == '[' else 0; nested -= varit [j].count (']') if last_chr == ']' else 0;
							varit [j] = (varit [k] + ',' + varit [j]) if state == 1 && k >= 0 && nested >= 0 && varit [k] [0] == '[' else varit [j];
							# Nested array is detected.
							state = 1 if nested > 0 else -1; if nested == 0 && varit [j] [0] == '[' && last_chr == ']': result.append (get_variant (varit [j]));
						# For nested dictionary.
						if state == -1 || state == 2:
							# Checks whether there are some characters '{' and '}' on the current string and generate dictionary.
							nested += varit [j].count ('{') if varit [j].find ('{') != -1 else 0; nested -= varit [j].count ('}') if varit [j].find_last ('}') != -1 else 0;
							varit [j] = (varit [k] + ',' + varit [j]) if state == 2 && k >= 0 && nested >= 0 && varit [k].find ('{') != -1 else varit [j];
							# Nested dictionary is detected.
							state = 2 if nested > 0 else -1; if nested == 0 && varit [j] [0] == '{' && last_chr == '}': result.append (get_variant (varit [j]));
					# Returns the final result.
					return result;
				# Otherwise.
				else: _output ("Syntax error. Check your value and make sure that it respects basics types nomenclatures.", Message.ERROR);
			# Checks variant size.
			else: return Array ([get_variant (variant)]) if variant.length () > 0 else result;
		# For a dictionary.
		elif variant.begins_with ('{') and variant.ends_with ('}'):
			# Corrects the given dictionary by putting some markers.
			variant = variant.replace (", ", ','); variant = variant.replace ('{', "{@dic:'',");
			variant = variant.replace ('}', ",@dic:''}"); variant = variant.replace ('[', "[@ary,");
			variant = variant.replace (']', ",@ary]"); variant = variant.replace ('=', ':');
			variant = variant.replace ('{', '['); variant = variant.replace ('}', ']');
			variant = variant.replace (':', ','); var real_array: Array = get_variant (variant);
			real_array.pop_back (); real_array.pop_back (); real_array.pop_front (); real_array.pop_front ();
			# Generating the corresponding dictionary.
			var result: Dictionary = Dictionary ({}); for i in range (0, (real_array.size () - 1), 2):
				# The current key is an array.
				if real_array [i] is Array:
					# For an nested array.
					if real_array [i] [0] == "@ary":
						# Removes array borders.
						real_array [i].pop_front (); real_array [i].pop_back ();
					# Common configurations.
					real_array [i] = str (real_array [i]); real_array [i] = real_array [i].replace ("[@ary, ", '[');
					real_array [i] = real_array [i].replace (", @ary]", ']'); real_array [i] = real_array [i].replace ("[@dic, '', ", '{');
					real_array [i] = real_array [i].replace (", @dic, '']", '}'); real_array [i] = get_variant (real_array [i]);
				# The current value is an Array.
				if real_array [(i + 1)] is Array:
					# For an nested array.
					if real_array [(i + 1)] [0] == "@ary":
						# Removes array borders.
						real_array [(i + 1)].pop_front (); real_array [(i + 1)].pop_back ();
					# Common configurations.
					real_array [(i + 1)] = str (real_array [(i + 1)]); real_array [(i + 1)] = real_array [(i + 1)].replace ("[@ary, ", '[');
					real_array [(i + 1)] = real_array [(i + 1)].replace (", @ary]", ']'); real_array [(i + 1)] = real_array [(i + 1)].replace ("[@dic, '', ", '{');
					real_array [(i + 1)] = real_array [(i + 1)].replace (", @dic, '']", '}'); real_array [(i + 1)] = get_variant (real_array [(i + 1)]);
				# Adds the final result to the created dictionary.
				result [real_array [i]] = real_array [(i + 1)];
			# Returns the final result.
			return result;
		# For string format.
		else: return variant;
	# Otherwise.
	else: return null;

"""
	@Description: Creates a file on the computer disk that contains the given data. This method can only save a dictionary.
	@Parameters:
		Dictionary data: Contains all useful data that will be saved.
		String path: Where want you create the save file ?
		Node obj: Which knot will be considered to make the differents operations ?
		String ky: What is password of data ? The key size must be in range (0, 32).
		int mhd: What is your prefered security method ? The supported methods are:
			-> MegaAssets.SecurityMethod.NONE or 0: No security will be applied to data.
			-> MegaAssets.SecurityMethod.AES or 1: Data encryption with AES method.
			-> MegaAssets.SecurityMethod.ARCFOUR or 2: Data encryption with ARCFOUR method.
			-> MegaAssets.SecurityMethod.CHACHA or 3: Data encryption with CHACHA method.
			-> MegaAssets.SecurityMethod.BINARY or 4: Coding data to 2 bits.
			-> MegaAssets.SecurityMethod.HEXADECIMAL or 5: Coding data to 16 bits.
			-> MegaAssets.SecurityMethod.OCTAL or 6: Coding data to 8 bits.
			-> MegaAssets.SecurityMethod.GODOT or 7: Data encryption with GODOT method.
		int lv: Which security level want you use ? The supported levels are:
			-> MegaAssets.SecurityLevel.SIMPLE or 0: Simple level security.
			-> MegaAssets.SecurityLevel.NORMAL or 1: Normal level security.
			-> MegaAssets.SecurityLevel.ADVANCED or 2: Advanced level security.
		Dictionary call: Contains information on the method to be executed while saving a file. This dictionary supports keys following:
			-> String | NodePath source: Contains the address from method to targeted. The presence of this key is not mandatory. In this case, we
				considered that the referred method is on the one found in the "obj" parameter.
			-> String method: Contains the name of the method to executed. The use of this key is mandatory.
			Note that the method to be executed must have three (03) parameters namely:
			-> String path: Will contain the name of the file in backup course.
			-> int progress: Will contain the current progress of file being backed up.
			-> Variant error: Will contain the error triggered at during data backup. This parameter will return you a dictionary containing
				the keys: "message", "code" and "type" or null if no error occurred during data saving.
		int chek: What is your prefered method for the checksum of the save file ? The supported methods are:
			-> MegaAssets.Checksum.NONE or 0: No checksum will be generated.
			-> MegaAssets.Checksum.MD5 or 1: The checksum will be generated by using MD5 encryption method.
			-> MegaAssets.Checksum.SHA256 or 2: The checksum will be generated by using SHA256 encryption method.
		float delay: What is the timeout before saving data ?
"""
static func serialize (data: Dictionary, path: String, obj: Node, ky: String = String (''), mhd: int = 7, lv: int = 1, call: Dictionary = Dictionary ({}),
chek: int = Checksum.NONE, delay: float = 0.0) -> void:
	# Checks data size.
	if data.size () > 0:
		# Corrects path separator and removes all spaces.
		path = path.replace ('\\', '/').replace (' ', '');
		# Checks path value.
		if !path.empty () && path.find_last ('.') != -1:
			# Checks the passed file extension.
			if not path.ends_with (".cfg"):
				# Waiting for user delay.
				if !Engine.editor_hint && delay > 0.0: yield (obj.get_tree ().create_timer (delay), "timeout");
				# Creates all useful folders to generate the save file.
				create_folders_from (path, obj); var file: File = File.new (); var fname: String = path.get_file ();
				# If GODOT method is not choosed or choosed.
				if mhd != SecurityMethod.GODOT || mhd == SecurityMethod.GODOT and lv <= SecurityLevel.SIMPLE:
					# Opens the file as WRITE mode.
					if file.open (path, File.WRITE) != OK: _callback_manager (call, Array ([path, 0, Dictionary ({
						"message": ("Failed to open {" + fname + "}."), "code": ERR_FILE_CANT_OPEN, "type": Message.WARNING
					})]), true, obj);
				# Otherwise.
				else:
					# For Normal level.
					if lv == SecurityLevel.NORMAL:
						 # Opens the file in WRITE encrypted mode.
						if file.open_encrypted_with_pass (path, File.WRITE, ky) != OK: _callback_manager (call, Array ([path, 0, Dictionary ({
							"message": ("Failed to open {" + fname + "}."), "code": ERR_FILE_CANT_OPEN, "type": Message.WARNING
						})]), true, obj);
					# Otherwise.
					else:
						# Corrects the passed key.
						while ky.length () < 32: ky += ' ';
						# Opens the file in WRITE encrypted mode.
						if file.open_encrypted (path, File.WRITE, ky.to_ascii ()) != OK: _callback_manager (call, Array ([path, 0, Dictionary ({
							"message": ("Failed to open {" + fname + "}."), "code": ERR_FILE_CANT_OPEN, "type": Message.WARNING
						})]), true, obj);
				# Checks whether Godot method is choosed.
				if mhd == SecurityMethod.GODOT:
					# Updates saving progress callback.
					_callback_manager (call, Array ([path, 0, null]), false, obj);
					# Checks level value.
					if lv <= SecurityLevel.SIMPLE:
						# If the extension of the save file is (.json).
						if path.ends_with (".json"): file.store_line (JSON.print (data, "\t")); else: file.store_line (str (data));
					# Otherwise.
					else:
						# If the extension of the save file is (.json).
						if path.ends_with (".json"): file.store_var (JSON.print (data, "\t")); else: file.store_var (data);
					# Updates saving progress callback.
					_callback_manager (call, Array ([path, 100, null]), false, obj);
				# Godot method is not choosed.
				else:
					# Contains all data as json format.
					var json_data: PoolStringArray = PoolStringArray ([]); var keys: Array = data.keys (); var progress: float = 0.0;
					# Stores each data into the save file from the given dictionary.
					for k in range (keys.size ()):
						# Item value must not be equal to @path or @subpath.
						if str (keys [k]).find ("@path") == -1 and str (keys [k]).find ("@subpath") == -1:
							# Contains the final data that will be saved.
							if !Engine.editor_hint: yield (obj.get_tree (), "idle_frame"); var inpt: String = (str (keys [k]) + "~||~" + str (data [keys [k]]));
							# Calculates the saving progress.
							progress = (float (k * 100) / len (keys));
							# For AES method.
							if mhd == SecurityMethod.AES:
								# For simple level.
								if lv <= SecurityLevel.SIMPLE: inpt = encrypt (inpt, ky, EncryptionMethod.AES, EncryptionSchema.BASE64);
								# For normal level.
								elif lv == SecurityLevel.NORMAL:
									# Encrypts the given data to Hex and codes the encrypted data to binary.
									inpt = encrypt (inpt, ky, EncryptionMethod.AES, EncryptionSchema.HEXADECIMAL); inpt = _string_to_bit (inpt, 2, false);
								# For advanced level.
								else:
									# Encrypts the given data to RAW and codes the encrypted data to hexadecimal.
									inpt = encrypt (inpt, ky, EncryptionMethod.AES, EncryptionSchema.RAW); inpt = _string_to_bit (inpt, 16, true);
							# For ARCFOUR method.
							elif mhd == SecurityMethod.ARCFOUR:
								# For simple level.
								if lv <= SecurityLevel.SIMPLE: inpt = encrypt (inpt, ky, EncryptionMethod.ARCFOUR, EncryptionSchema.BASE64);
								# For normal level.
								elif lv == SecurityLevel.NORMAL:
									# Encrypts the given data to Hex and codes the encrypted data to binary.
									inpt = encrypt (inpt, ky, EncryptionMethod.ARCFOUR, EncryptionSchema.HEXADECIMAL); inpt = _string_to_bit (inpt, 2, false);
								# For advanced level.
								else:
									# Encrypts the given data to RAW and codes the encrypted data to hexadecimal.
									inpt = encrypt (inpt, ky, EncryptionMethod.ARCFOUR, EncryptionSchema.RAW); inpt = _string_to_bit (inpt, 16, true);
							# For CHACHA method.
							elif mhd == SecurityMethod.CHACHA:
								# For simple level.
								if lv <= SecurityLevel.SIMPLE: inpt = encrypt (inpt, ky, EncryptionMethod.CHACHA, EncryptionSchema.BASE64);
								# For normal level.
								elif lv == SecurityLevel.NORMAL:
									# Encrypts the given data to Hex and codes the encrypted data to binary..
									inpt = encrypt (inpt, ky, EncryptionMethod.CHACHA, EncryptionSchema.HEXADECIMAL); inpt = _string_to_bit (inpt, 2, false);
								# For advanced level.
								else:
									# Encrypts the given data to RAW and codes the encrypted data to hexadecimal.
									inpt = encrypt (inpt, ky, EncryptionMethod.CHACHA, EncryptionSchema.RAW); inpt = _string_to_bit (inpt, 16, true);
							# For BINARY method.
							elif mhd == SecurityMethod.BINARY:
								# For simple level.
								if lv <= SecurityLevel.SIMPLE: inpt = _string_to_bit (inpt, 2, false);
								# For normal level.
								elif lv == SecurityLevel.NORMAL: inpt = _string_to_bit (inpt, 2, true);
								# For advanced level.
								else:
									# Encrypts the given data to BASE64 and codes the encrypted data to binary.
									inpt = encrypt (inpt, ky, EncryptionMethod.AES, EncryptionSchema.BASE64); inpt = _string_to_bit (inpt, 2, true);
							# For HEXADECIMAL method.
							elif mhd == SecurityMethod.HEXADECIMAL:
								# For simple level.
								if lv <= SecurityLevel.SIMPLE: inpt = _string_to_bit (inpt, 16, false);
								# For normal level.
								elif lv == SecurityLevel.NORMAL: inpt = _string_to_bit (inpt, 16, true);
								# For advanced level.
								else:
									# Encrypts the given data to BASE64 and codes the encrypted data to hexadecimal.
									inpt = encrypt (inpt, ky, EncryptionMethod.ARCFOUR, EncryptionSchema.BASE64); inpt = _string_to_bit (inpt, 16, true);
							# For OCTAL method.
							elif mhd == SecurityMethod.OCTAL:
								# For simple level.
								if lv <= SecurityLevel.SIMPLE: inpt = _string_to_bit (inpt, 8, false);
								# For normal level.
								elif lv == SecurityLevel.NORMAL: inpt = _string_to_bit (inpt, 8, true);
								# For advanced level.
								else:
									# Encrypts the given data to HEX and codes the encrypted data to octal.
									inpt = encrypt (inpt, ky, EncryptionMethod.CHACHA, EncryptionSchema.BASE64); inpt = _string_to_bit (inpt, 8, true);
							# If the extension of the save file is (.json).
							if path.ends_with (".json"): json_data.append (inpt); else: file.store_line (inpt);
							# Updates saving progress callback.
							_callback_manager (call, Array ([path, int (progress), null]), false, obj);
					# For json format.
					if path.ends_with (".json"): file.store_string (JSON.print (json_data, "\t")); file.close ();
					# There are a security.
					if chek > Checksum.NONE:
						# Creating the checksum file.
						var fchecksum: File = File.new ();
						# Opens the checksum file as WRITE mode.
						if fchecksum.open ((OS.get_user_data_dir () + '/' + fname.split ('.') [0] + ".sum"), File.WRITE) != OK:
							# Error message.
							_callback_manager (call, Array ([path, int (progress), Dictionary ({
								"message": ("Failed to generate the checksum file of {" + fname + "}."), "code": ERR_FILE_CANT_WRITE, "type": Message.ERROR
							})]), true, obj);
						# No errors detected.
						else:
							# For MD5 checksum generation.
							if chek == Checksum.MD5: fchecksum.store_line (file.get_md5 (path));
							# For SHA256 checksum generation.
							elif chek >= Checksum.SHA256: fchecksum.store_line (file.get_sha256 (path));
							# Updates saving progress callback.
							fchecksum.close (); _callback_manager (call, Array ([path, 100, null]), false, obj);
					# Otherwise.
					else: _callback_manager (call, Array ([path, 100, null]), false, obj);
			# Otherwise.
			else: _output ("(.cfg) extension isn't supported on this method.", Message.ERROR);
		# Warning message.
		else: _output ("Invalid path.", Message.WARNING);
	# Warning message.
	else: _output ("The data mustn't be empty.", Message.WARNING);

"""
	@Description: Returns a deserialized dictionary from a save file. Pay attention ! The file will have to be generated by using "serialize ()" method.
	@Parameters:
		String path: Where found the save file ?
		String key: What is password of the file data ? The key size must be in range (0, 32).
		Node obj: Which knot will be considered to make the differents operations ?
		int mhd: What is the security method that has been used to save data ? The supported methods are:
			-> MegaAssets.SecurityMethod.NONE or 0: No security will be applied to data.
			-> MegaAssets.SecurityMethod.AES or 1: Data encryption with AES method.
			-> MegaAssets.SecurityMethod.ARCFOUR or 2: Data encryption with ARCFOUR method.
			-> MegaAssets.SecurityMethod.CHACHA or 3: Data encryption with CHACHA method.
			-> MegaAssets.SecurityMethod.BINARY or 4: Coding data to 2 bits.
			-> MegaAssets.SecurityMethod.HEXADECIMAL or 5: Coding data to 16 bits.
			-> MegaAssets.SecurityMethod.OCTAL or 6: Coding data to 8 bits.
			-> MegaAssets.SecurityMethod.GODOT or 7: Data encryption with GODOT method.
		int level: Which security levels has been used on the save file ? The supported levels are:
			-> MegaAssets.SecurityLevel.SIMPLE or 0: Simple level security.
			-> MegaAssets.SecurityLevel.NORMAL or 1: Normal level security.
			-> MegaAssets.SecurityLevel.ADVANCED or 2: Advanced level security.
		Dictionary call: Contains information on the method to be executed while loading a file. This dictionary supports keys following:
			-> String | NodePath source: Contains the address of the method to targeted. The presence of this key is not mandatory. In this case,
				we consider that the referred method is on the one in the "obj" parameter.
			-> String method: Contains the name of the method to executed. The use of this key is mandatory.
			Note that the method to be executed must have five (05) parameters namely:
			-> String path: Will contain the path of the file in loading.
			-> bool is_loading: Is the current file actually loading ?
			-> int progress: Will contain the number of item (s) already loaded into memory.
			-> Dictionary result: Will contain all data loaded from the file in question.
			-> Variant error: Will contain the error triggered at during data loading. This parameter will return you a dictionary containing
				the keys: "message", "code" and "type" or null if no error occurred during data backup.
		int check: What is the checksum that has been used on the save file ? The supported methods are:
			-> MegaAssets.Checksum.NONE or 0: No checksum will be generated.
			-> MegaAssets.Checksum.MD5 or 1: The checksum will be generated by using MD5 encryption method.
			-> MegaAssets.Checksum.SHA256 or 2: The checksum will be generated by using SHA256 encryption method.
"""
static func deserialize (path: String, obj: Node, key: String = '', mhd: int = 7, level: int = 1, call: Dictionary = Dictionary ({}), check: int = 0) -> Dictionary:
	# Corrects path separator and removes all spaces.
	path = path.replace ('\\', '/').replace (' ', '');
	# Checks path value.
	if !path.empty () && path.find_last ('.') != -1:
		# Checks the given file extension.
		if not path.ends_with (".cfg"):
			# Creates a file his checksum file.
			var file: File = File.new (); var fchecksum: File = File.new (); var fname: String = path.get_file ();
			# Updates loading progress callback with the created decrypted data.
			var decrypted_data: Dictionary = Dictionary ({}); _callback_manager (call, Array ([path, true, 0, decrypted_data, null]), false, obj);
			# If the path to the target save file is invalid.
			if not file.file_exists (path): _callback_manager (call, Array ([path, false, 0, decrypted_data, Dictionary ({
				"message": ("The file {" + fname + "} doesn't exists."), "code": ERR_FILE_NOT_FOUND, "type": Message.WARNING
			})]), true, obj);
			# Checks Godot Method.
			elif mhd == SecurityMethod.GODOT and level > SecurityLevel.SIMPLE:
				# Contains the save file state.
				var file_state: int = -1;
				# Normal level.
				if level == SecurityLevel.NORMAL: file_state = file.open_encrypted_with_pass (path, File.READ, key);
				# Advanced level.
				else:
					# Corrects the passed key and open the file in encrypted READ mode.
					while key.length () < 32: key += ' '; file_state = file.open_encrypted (path, File.READ, key.to_ascii ());
				# There are some warning.
				if file_state != OK:
					# If the file is corrupted.
					if file_state == ERR_FILE_CORRUPT: _callback_manager (call, Array ([path, false, 0, decrypted_data, Dictionary ({
						"message": ("The file {" + fname + "} is corrupted."), "code": ERR_FILE_CORRUPT, "type": Message.WARNING
					})]), true, obj);
					# Warning message.
					else: _callback_manager (call, Array ([path, false, 0, decrypted_data, Dictionary ({
						"message": ("Failed to open {" + fname + "}."), "code": ERR_FILE_CANT_OPEN, "type": Message.WARNING
					})]), true, obj);
				# No errors detected.
				else:
					# For json format.
					if path.ends_with (".json"): decrypted_data = JSON.parse (file.get_var ()).result; else: decrypted_data = file.get_var (); file.close ();
					# Updates loading progress callback.
					_callback_manager (call, Array ([path, false, decrypted_data.size (), decrypted_data, null]), false, obj); return decrypted_data;
			# Opens the file as READ mode.
			elif file.open (path, File.READ) != OK: _callback_manager (call, Array ([path, false, 0, decrypted_data, Dictionary ({
				"message": ("Failed to open {" + fname + "}."), "code": ERR_FILE_CANT_OPEN, "type": Message.WARNING
			})]), true, obj);
			# Godot method is choosed.
			elif mhd == SecurityMethod.GODOT and level <= SecurityLevel.SIMPLE:
				# If json format is used.
				if path.ends_with (".json"):
					# Returns the final value.
					decrypted_data = JSON.parse (file.get_as_text ()).result; file.close ();
					# Updates loading progress callback.
					_callback_manager (call, Array ([path, false, decrypted_data.size (), decrypted_data, null]), false, obj); return decrypted_data;
				# For other format.
				else:
					# Generates the corresponding data of the passed string.
					decrypted_data = get_variant (file.get_as_text ());
					# Updates loading progress callback.
					_callback_manager (call, Array ([path, false, decrypted_data.size (), decrypted_data, null]), false, obj); return decrypted_data;
			# Opens the target checksum file as READ mode.
			elif check > Checksum.NONE && fchecksum.open ((OS.get_user_data_dir () + '/' + fname.split ('.') [0] + ".sum"), File.READ) != OK:
				# Warning message.
				_callback_manager (call, Array ([path, false, 0, decrypted_data, Dictionary ({
					"message": ("The checksum file of {" + fname + "} is not defined."), "code": ERR_FILE_NOT_FOUND, "type": Message.WARNING
				})]), true, obj);
			# For MD5 checksum.
			elif check == Checksum.MD5 && file.get_md5 (path) != fchecksum.get_line ():
				# Warning message.
				_callback_manager (call, Array ([path, false, 0, decrypted_data, Dictionary ({
					"message": ("Failed to load {" + fname + "}. This file is corrupted."), "code": ERR_FILE_CORRUPT, "type": Message.WARNING
				})]), true, obj);
			# For SHA256 checksum.
			elif check >= Checksum.SHA256 && file.get_sha256 (path) != fchecksum.get_line ():
				# Warning message.
				_callback_manager (call, Array ([path, false, 0, decrypted_data, Dictionary ({
					"message": ("Failed to load {" + fname + "}. This file is corrupted."), "code": ERR_FILE_CORRUPT, "type": Message.WARNING
				})]), true, obj);
			# No warnings detected.
			else:
				# Contains all json data.
				var json_data: PoolStringArray = PoolStringArray ([]); var line: String = ''; var js_idx: int = 0; var js_dt_count: int = 0;
				# If the save file is a json file.
				if path.ends_with (".json"):
					# Contains all json data and updates the line value.
					json_data = JSON.parse (file.get_as_text ()).result; line = json_data [0]; js_dt_count = json_data.size ();
				# Otherwise.
				else: line = file.get_line ();
				# While there are some data.
				while path.ends_with (".json") && js_idx < js_dt_count or !path.ends_with (".json") && !line.empty ():
					# Contains the final decrypted value of a datum.
					var data = null;
					# No security has put.
					if mhd <= SecurityMethod.NONE or mhd > SecurityMethod.OCTAL: data = line;
					# For AES method.
					elif mhd == SecurityMethod.AES:
						# For simple level.
						if level <= SecurityLevel.SIMPLE: data = decrypt (line, key, EncryptionMethod.AES, EncryptionSchema.BASE64);
						# For normal level.
						elif level == SecurityLevel.NORMAL:
							# Decoding the encrypted line and decrypting of the given decoded data.
							data = _string_from_bit (line, 2, false); data = decrypt (data, key, EncryptionMethod.AES, EncryptionSchema.HEXADECIMAL);
						# For advanced level.
						else:
							# Decoding the encrypted line and decrypting of the given decoded data.
							data = _string_from_bit (line, 16, true); data = decrypt (data, key, EncryptionMethod.AES, EncryptionSchema.RAW);
					# For ARCFOUR method.
					elif mhd == SecurityMethod.ARCFOUR:
						# For simple level.
						if level <= SecurityLevel.SIMPLE: data = decrypt (line, key, EncryptionMethod.ARCFOUR, EncryptionSchema.BASE64);
						# For normal level.
						elif level == SecurityLevel.NORMAL:
							# Decoding the encrypted line and decrypting of the given decoded data.
							data = _string_from_bit (line, 2, false); data = decrypt (data, key, EncryptionMethod.ARCFOUR, EncryptionSchema.HEXADECIMAL);
						# For advanced level.
						else:
							# Decoding the encrypted line and decrypting of the given decoded data.
							data = _string_from_bit (line, 16, true); data = decrypt (data, key, EncryptionMethod.ARCFOUR, EncryptionSchema.RAW);
					# For CHACHA method.
					elif mhd == SecurityMethod.CHACHA:
						# For simple level.
						if level <= SecurityLevel.SIMPLE: data = decrypt (line, key, EncryptionMethod.CHACHA, EncryptionSchema.BASE64);
						# For normal level.
						elif level == SecurityLevel.NORMAL:
							# Decoding the encrypted line and decrypting of the given decoded data.
							data = _string_from_bit (line, 2, false); data = decrypt (data, key, EncryptionMethod.CHACHA, EncryptionSchema.HEXADECIMAL);
						# For advanced level.
						else:
							# Decoding the encrypted line and decrypting of the given decoded data.
							data = _string_from_bit (line, 16, true); data = decrypt (data, key, EncryptionMethod.CHACHA, EncryptionSchema.RAW);
					# For BINARY method.
					elif mhd == SecurityMethod.BINARY:
						# For simple level.
						if level <= SecurityLevel.SIMPLE: data = _string_from_bit (line, 2, false);
						# For normal level.
						elif level == SecurityLevel.NORMAL: data = _string_from_bit (line, 2, true);
						# For advanced level.
						else:
							# Decoding the encrypted line and decrypting of the given decoded data.
							data = _string_from_bit (line, 2, true); data = decrypt (data, key, EncryptionMethod.AES, EncryptionSchema.BASE64);
					# For HEXADECIMAL method.
					elif mhd == SecurityMethod.HEXADECIMAL:
						# For simple level.
						if level <= SecurityLevel.SIMPLE: data = _string_from_bit (line, 16, false);
						# For normal level.
						elif level == SecurityLevel.NORMAL: data = _string_from_bit (line, 16, true);
						# For advanced level.
						else:
							# Decoding the encrypted line and decrypting of the given decoded data.
							data = _string_from_bit (line, 16, true); data = decrypt (data, key, EncryptionMethod.ARCFOUR, EncryptionSchema.BASE64);
					# For OCTAL method.
					elif mhd == SecurityMethod.OCTAL:
						# For simple level.
						if level <= SecurityLevel.SIMPLE: data = _string_from_bit (line, 8, false);
						# For normal level.
						elif level == SecurityLevel.NORMAL: data = _string_from_bit (line, 8, true);
						# For advanced level.
						else:
							# Decoding the encrypted line and decrypting of the given decoded data.
							data = _string_from_bit (line, 8, true); data = decrypt (data, key, EncryptionMethod.CHACHA, EncryptionSchema.BASE64);
					# Getting the decrypted key and value.
					data = data.split ("~||~"); if data is PoolStringArray && data.size () == 2: decrypted_data [get_variant (data [0])] = get_variant (data [1]);
					# For json file format.
					if path.ends_with (".json"):
						# Updates the line value.
						js_idx += 1; if js_idx < js_dt_count: line = json_data [js_idx];
					# For other file format and updates loading progress callback.
					else: line = file.get_line (); _callback_manager (call, Array ([path, true, decrypted_data.size (), Dictionary ({}), null]), false, obj);
				# Closes all files and returns the final result.
				js_idx = 0; file.close (); fchecksum.close ();
				# Updates loading progress callback.
				_callback_manager (call, Array ([path, false, decrypted_data.size (), decrypted_data, null]), false, obj); return decrypted_data;
		# Otherwise.
		else: _output ("(.cfg) extension isn't supported on this method.", Message.ERROR);
	# Warning message.
	else: _output ("Invalid path.", Message.WARNING); return Dictionary ({});

"""
	@Description: Generates a color.
	@Parameters:
		bool use_alpha: Do you want to generate some colors without alpha color ?
"""
static func generate_color (use_alpha: bool = false) -> Color:
	# Makes a randomize and returns the final result.
	randomize (); return Color8 ((randi () % 255), (randi () % 255), (randi () % 255), ((randi () % 255) if use_alpha else 255));

"""
	@Description: Returns severals nodes from their path.
	@Parameters:
		PoolStringArray nodes_paths: Contains all target nodes paths.
		Node object: Which knot will be considered to make the differents operations ?
		bool reversed: Do you want to inverse treatment ?
"""
static func get_nodes (nodes_paths: PoolStringArray, object: Node, reversed: bool = false) -> Array:
	# Contains the nodes paths size
	var size: int = nodes_paths.size ();
	# If the nodes paths array is not null.
	if size > 0:
		# Contains all found nodes from their path.
		var nodes: Array = Array ([]);
		# Gets all target nodes.
		for v in range ((0 if !reversed else (size - 1)), (size if !reversed else -1), (1 if !reversed else -1)):
			# Contains the found node.
			var node: Node = object.get_node_or_null (NodePath (nodes_paths [v])); if node is Node: nodes.append (node);
		# Returns the final result.
		return nodes;
	# Returns a fake value.
	else: return Array ([]);

"""
	@Description: Adds a/some node(s) to other node. The specified node(s) has no parent before adding.
	@Parameters:
		PoolStringArray | String | NodePath nodes: Contains a/some node(s) path(s).
		String | NodePath other: Contains the node path which the target node(s) will be added.
		Node object: Which knot will be considered to make the differents operations ?
		bool reversed: Do you want to inverse treatment ?
		float delay: What is the timeout before adding node ?
		float interval: What is the timeout each add ?
"""
static func add_children (nodes, other, object: Node, reversed: bool = false, delay: float = 0.0, interval: float = 0.0) -> void:
	# The game is running.
	if !Engine.editor_hint:
		# If the game is not initialised.
		if delay == 0.0 && !is_game_initialised (): yield (object.get_tree (), "idle_frame");
		# Otherwise.
		elif delay > 0.0: yield (object.get_tree ().create_timer (delay), "timeout");
	# Converts "nodes" parameter into a PoolStringArray.
	nodes = PoolStringArray (Array ([nodes]) if not is_array (nodes) else Array (nodes));
	# Gets the real passed node.
	other = object.get_node_or_null (other) if other is String or other is NodePath else other;
	# Checks the current node value.
	if other is Node:
		# Adds all passed node(s) to other node(s).
		for l in range ((0 if !reversed else (nodes.size () - 1)), (len (nodes) if !reversed else -1), (1 if !reversed else -1)):
			# Filters each item from the node list.
			nodes [l] = object.get_node_or_null (nodes [l]) if nodes [l] is String or nodes [l] is NodePath else nodes [l];
			# Checks the current node value.
			if nodes [l] is Node:
				# Waiting for user interval time.
				if !Engine.editor_hint && interval > 0.0: yield (object.get_tree ().create_timer (interval), "timeout");
				# Ckecks parent of the current node.
				if nodes [l].get_parent () == null: other.add_child (nodes [l]); else: _output (('{' + nodes [l].name + "} node has a parent already."), Message.WARNING);
			# Otherwise.
			else: _output (("The current node doesn't exists::index " + str (l)), Message.ERROR);
	# Otherwise.
	else: _output ("The container node doesn't exists.", Message.ERROR);

"""
	@Description: Generates a/some name(s).
	@Parameters:
		int smin: Contains the min length of the name to generated.
		int smax: Contains the max length of the name to generated.
		int count: Contains total name count.
"""
static func name_generation (smin: int = 3, smax: int = 7, count: int = 1):
	# Contains all generated names.
	var generated_names: PoolStringArray = PoolStringArray ([]);
	# Generating names.
	for _s in range (count):
		# Contains the generated word length.
		var generated_length: int = 0; randomize (); generated_length = int (round (randi () % ((smax - smin) + smin)));
		# Contains the generated name value.
		var generated_name: String = ''; var last_letter: String = ''; var index: int = 0; var state: String = "INITIAL";
		# While the index value is less than generated length.
		while index < generated_length:
			# Contains possible transitions and objects.
			var transitions = Array ([]); var obj = Array ([]); for item in _TRANSITION [state]: transitions.append (item);
			# If the max length is less than 3.
			if (index - generated_length) < 3:
				# Updading transitions.
				transitions.erase ("COMPOSE"); transitions.erase ("DOUBLE_CONSONNE"); transitions.erase ("DOUBLE_VOYELLE"); randomize ();
			# Contains the state of the current index.
			var state_index = round (randi () % transitions.size ()); var stat = transitions [state_index]; var letters_list = _LETTERS [stat]; randomize ();
			# Contains the current index letter and updates obj.
			var letter_index = round (randi () % letters_list.size ()); obj = [stat, letters_list [letter_index]]; state = obj [0]; last_letter = obj [1];
			# Generating name letter.
			generated_name += last_letter; index += last_letter.length ();
		# Returns the generated name value.
		generated_names.append (generated_name.capitalize ());
	# Contains the generated names size and returns the final value.
	var size: int = generated_names.size (); if count <= 0: return null; elif size == 1: return generated_names [0]; else: return generated_names;

"""@Description: Returns author name of "Godot Mega Assets" framework."""
static func get_author_name () -> String: return "Obrymec";

"""@Description: Returns license name of "Godot Mega Assets" framework."""
static func get_used_license () -> String: return "MIT";

"""@Description: Returns web link of "Godot Mega Assets" framework."""
static func get_source () -> String: return "https://godot-mega-assets.herokuapp.com/home";

"""@Description: Returns editor version where "Godot Mega Assets" framework is compatible."""
static func get_compatibility () -> String: return "Godot 3.x.x";

"""@Description: Returns this class version."""
static func get_version () -> String: return "0.1.3";

"""@Description: What is the supported platforms of this module ?"""
static func get_supported_platforms () -> String: return "ANDROID || IOS || MACOSX || UWP || HTML5 || WINDOWS || LINUX";

"""@Description: What is the supported dimensions of this module ?"""
static func get_supported_dimensions () -> String: return "2D || 3D";

"""@Description: Returns the class name of this class."""
func get_class () -> String: return "MegaAssets";

"""
	@Description: Saves a dictionary in a config file.
	@Parameters:
		Dictionary data: Contains all data that will be saved.
		String path: Where do you want to save data ?
		Node object: Which knot will be considered to make the differents operations ?
		String key: Contains the password of saved data. The key size must be in range (0, 32).
		int security: Do you want to put a security to config file ? The possible values are:
			-> MegaAssets.SecurityLevel.SIMPLE or 0: Simple level security.
			-> MegaAssets.SecurityLevel.NORMAL or 1: Normal level security.
			-> MegaAssets.SecurityLevel.ADVANCED or 2: Advanced level security.
		Dictionary call: Contains information on the method to be executed while saving a file. This dictionary supports keys following:
			-> String | NodePath source: Contains the address from method to targeted. The presence of this key is not mandatory. In this case, we
				considered that the referred method is on the one found in the "object" parameter.
			-> String method: Contains the name of the method to executed. The use of this key is mandatory.
			Note that the method to be executed must have three (03) parameters namely:
			-> String path: Will contain the path of the file in backup course.
			-> int progress: Will contain the current progress of file being backed up.
			-> Variant error: Will contain the error triggered at during data backup. This parameter will return you a dictionary containing
				the keys: "message", "code" and "type" or null if no error occurred during data saving.
		float delay: What is the timeout before saving data ?
"""
static func save_config_file (data: Dictionary, path: String, object: Node, key: String = '', security: int = 0, call: Dictionary = {}, delay: float = 0.0) -> void:
	# If data isn't empty.
	if data.size () > 0:
		# Waiting for user delay and corrects path separator and removes all spaces.
		if !Engine.editor_hint && delay > 0.0: yield (object.get_tree ().create_timer (delay), "timeout"); path = path.replace ('\\', '/').replace (' ', '');
		# If path value is not empty.
		if not path.empty ():
			# Checks path value.
			if path.find_last ('.') != -1:
				# Checks file extension.
				if path.ends_with (".cfg"):
					# Creates all useful folders to generate config file.
					create_folders_from (path, object); var config_file: ConfigFile = ConfigFile.new ();
					# Contains the file name.
					var fname: String = path.get_file (); var keys: Array = data.keys (); var progress: float = 0.0;
					# Generating config file.
					for k in keys.size ():
						# Waiting for game idle frames.
						yield (object.get_tree (), "idle_frame");
						# If the key is a section.
						if typeof (data [keys [k]]) == TYPE_DICTIONARY and data [keys [k]].size () > 0:
							# Updates config file.
							for item in data [keys [k]]: config_file.set_value (str (keys [k]), str (item), data [keys [k]] [item]);
						# Updates the saving progress and runs the callback.
						progress = (float (k * 100) / len (keys)); _callback_manager (call, Array ([path, int (progress), null]), false, object);
					# If there are no security actived.
					if security <= SecurityLevel.SIMPLE:
						# Saves the config file normaly.
						if config_file.save (path) != OK: _callback_manager (call, Array ([path, int (progress), Dictionary ({
							"message": ("Failed to create {" + fname + "}."), "code": ERR_FILE_CANT_WRITE, "type": Message.WARNING
						})]), true, object);
						# Otherwise.
						else: _callback_manager (call, Array ([path, 100, null]), false, object);
					# Normal security.
					elif security == SecurityLevel.NORMAL:
						# Saves the config file with AES-256 encryption method.
						if config_file.save_encrypted_pass (path, key) != OK: _callback_manager (call, Array ([path, int (progress), Dictionary ({
							"message": ("Failed to create {" + fname + "}."), "code": ERR_FILE_CANT_WRITE, "type": Message.WARNING
						})]), true, object);
						# Otherwise.
						else: _callback_manager (call, Array ([path, 100, null]), false, object);
					# Advanced security.
					else:
						# Corrects the passed key.
						while key.length () < 32: key += ' ';
						# Saves the config file with AES-256 encryption method using 32 bits.
						if config_file.save_encrypted (path, key.to_ascii ()) != OK: _callback_manager (call, Array ([path, int (progress), Dictionary ({
							"message": ("Failed to create {" + fname + "}."), "code": ERR_FILE_CANT_WRITE, "type": Message.WARNING
						})]), true, object);
						# Otherwise.
						else: _callback_manager (call, Array ([path, 100, null]), false, object);
				# Error message.
				else: _output ("The extension of the config file must be (.cfg) format.", Message.ERROR);
			# Warning message.
			else: _output ("The file is not defined.", Message.WARNING);
		# Warning message.
		else: _output ("The path is not defined.", Message.WARNING);
	# Warning message.
	else: _output ("Data shouldn't be empty.", Message.WARNING);

"""
	@Description: Loads a config file.
	@Parameters:
		String path: Where find the config file ? Pay attention ! The file will have to be generated by using "save_config_file ()" method.
		Node object: Which knot will be considered to make the differents operations ?
		String key: What is the password that has been used on saving data ? The key size must be in range (0, 32).
		int security: Have you put a security to config file last time ? The possible values are:
			-> MegaAssets.SecurityLevel.SIMPLE or 0: Simple level security.
			-> MegaAssets.SecurityLevel.NORMAL or 1: Normal level security.
			-> MegaAssets.SecurityLevel.ADVANCED or 2: Advanced level security.
		Dictionary call: Contains information on the method to be executed while loading a file. This dictionary supports keys following:
			-> String | NodePath source: Contains the address of the method to targeted. The presence of this key is not mandatory. In this case,
				we consider that the referred method is on the one in the "object" parameter.
			-> String method: Contains the name of the method to executed. The use of this key is mandatory.
			Note that the method to be executed must have four (04) parameters namely:
			-> String path: Will contain the path of the file in loading.
			-> int progress: Will contain the loading progress.
			-> Dictionary result: Will contain all data loaded from the file in question.
			-> Variant error: Will contain the error triggered at during data loading. This parameter will return you a dictionary containing
				the keys: "message", "code" and "type" or null if no error occurred during data backup.
"""
static func load_config_file (path: String, object: Node, key: String = '', security: int = SecurityLevel.SIMPLE, call: Dictionary = Dictionary ({})) -> Dictionary:
	# Corrects path separator and removes all spaces.
	path = path.replace ('\\', '/').replace (' ', '');
	# If path value is not empty.
	if not path.empty ():
		# Checks path value.
		if path.find_last ('.') != -1:
			# Checks file extension.
			if path.ends_with (".cfg"):
				# Contains the file name.
				var fname: String = path.get_file (); var result: Dictionary = Dictionary ({});
				# Checks whether the config file exists.
				if File.new ().file_exists (path):
					# Contains the config file.
					var config_file: ConfigFile = ConfigFile.new (); var conf_state;
					# If no security has been enabled.
					if security <= SecurityLevel.SIMPLE: conf_state = config_file.load (path);
					# Normal security has been put.
					elif security == SecurityLevel.NORMAL: conf_state = config_file.load_encrypted_pass (path, key);
					# Advanced security has been put.
					else:
						# Corrects the passed key and load the config file with AES-256 encryption method.
						while key.length () < 32: key += ' '; conf_state = config_file.load_encrypted (path, key.to_ascii ());
					# Loads the config file.
					if conf_state != OK:
						# If the file is corrupted.
						if conf_state == ERR_FILE_CORRUPT: _callback_manager (call, Array ([path, 0, result, Dictionary ({
							"message": ("The file {" + fname + "} is corrupted."), "code": ERR_FILE_CORRUPT, "type": Message.WARNING
						})]), true, object);
					# No errors detected.
					else:
						# Contains the file data as section format.
						var sections: PoolStringArray = config_file.get_sections ();
						# Generating result.
						for t in sections.size ():
							# Contains all values of a section.
							var section_values: Dictionary = Dictionary ({});
							# Assembling keys.
							for ky in config_file.get_section_keys (sections [t]): section_values [ky] = config_file.get_value (sections [t], ky, null);
							# Updates the section.
							result [sections [t]] = section_values;
							# Updates loading progress callback.
							_callback_manager (call, Array ([path, int (float (t * 100) / len (sections)), Dictionary ({}), null]), false, object);
						# Updates loading progress callback and returns the final result.
						_callback_manager (call, Array ([path, 100, result, null]), false, object); return result;
				# Warning message.
				else: _callback_manager (call, Array ([path, 0, result, Dictionary ({
					"message": ("The file {" + fname + "} doesn't exists."), "code": ERR_FILE_NOT_FOUND, "type": Message.WARNING
				})]), true, object);
			# Error message.
			else: _output ("The extension of the config file must be (.cfg) format.", Message.ERROR);
		# Warning message.
		else: _output ("The file is not defined.", Message.WARNING);
	# Warning message.
	else: _output ("The path is not defined.", Message.WARNING); return Dictionary ({});

"""
	@Description: Creates the neccesary folders include in a path.
	@Parameters:
		String path: Contains the directories tree.
		Node object: Which knot will be considered to make the differents operations ?
		float delay: What is the timeout before creating folders ?
"""
static func create_folders_from (path: String, object: Node, delay: float = 0.0) -> void:
	# Corrects path separator and removes all spaces.
	path = path.replace ('\\', '/').replace (' ', '');
	# If the path is not empty.
	if not path.empty ():
		# Waiting for the given delay.
		if !Engine.editor_hint && delay > 0.0: yield (object.get_tree ().create_timer (delay), "timeout");
		# Contains all root directories that will be created.
		var directory: Directory = Directory.new (); var root_folders: String = path.get_base_dir ();
		# Checks root folders existance.
		if !directory.dir_exists (root_folders) && directory.make_dir_recursive (root_folders) != OK:
			# Error message.
			_output ("Failed to create folders. The access may be denied. Check your path.", Message.ERROR);
	# Warning message.
	else: _output ("The path is not defined.", Message.WARNING);

"""
	@Description: Converts an/some input(s) into a dictionary format.
	@Parameters:
		Variant input: Contains contains the value that will be parsed into a dictionary.
		bool reversed: Do you want to inverse treatment ?
"""
static func any_to_dic (input, reversed: bool = false) -> Dictionary:
	# Parses the given parameter into an array.
	input = Array ([input]) if !is_array (input) else input; var result: Dictionary = Dictionary ({});
	# Generating the final dic.
	for index in range ((0 if !reversed else (len (input) - 1)), (len (input) if !reversed else -1), (1 if !reversed else -1)):
		# If the current obj is a dictionary.
		if input [index] is Dictionary: for key in input [index]: result [key] = input [index] [key]; else: result [index] = input [index];
	# Returns the final result.
	return result;

"""
	@Description: Converts an/some input(s) into an array format.
	@Parameters:
		Variant input: Contains contains the value that will be parsed into an array.
		int dic_property: which property want to put into the returned array ? (Only whether the passed input is a Dictionary). The supported values are:
			-> MegaAssets.DictionaryProperty.KEYS or 0: Target the dictionary keys.
			-> MegaAssets.DictionaryProperty.VALUES or 1: Target the dictionary values.
			-> MegaAssets.DictionaryProperty.BOTH or 2: Target the dictionary keys and values.
"""
static func any_to_array (input, dic_property: int = 1) -> Array:
	# If the input is already an array.
	if is_array (input): return input;
	# Otherwise.
	else:
		# For a string.
		if input is String:
			# Removes left and right spaces and adding each character from the given string to characters array.
			input = input.lstrip (' ').rstrip (' '); var characters: Array = Array ([]); for chr in input: characters.append (chr); return characters;
		# For a dictionary.
		elif input is Dictionary:
			# For dictionary key only.
			var array: Array = Array ([]); if dic_property <= DictionaryProperty.KEYS: for key in input: array.append (key);
			# For dictionary value only.
			elif dic_property == DictionaryProperty.VALUES: for value in input: array.append (input [value]);
			# For both.
			else:
				# Takes the key and value from the passed dictionary.
				for both in input:
					# Puts this data into the array.
					array.append (both); array.append (input [both]);
			# Returns the final result.
			return array;
		# Otherwise.
		else: return Array ([input]);

"""
	@Description: Triggers one or more event(s) at a time.
	@Parameters:
		Array data: Table of dictionaries managing configurations related to the different events that we want to support. The
			dictionary(ies) from this table, support the following keys:
			-> String event: Contains the name of the event to sets off.
			-> float delay: What is the dead time before calling the event in question.
			-> Array params: Contains the values ​​of the various parameters of the event in question.
		Node node: Contains the node from which the event(s) will be triggered.
		int scope: Contains the value of events scope. The supported values are:
			-> MegaAssets.WornEvents.NONE or 0: None scope.
			-> MegaAssets.WornEvents.CHILDREN_ONLY or 1: For node children only.
			-> MegaAssets.WornEvents.PARENTS_ONLY or 2: For node parent only.
			-> MegaAssets.WornEvents.ALL or 3: For all nodes around of the target node.
		bool recursive: Will the scope of the event(s) be recursive ?
		float delay: What is the timeout before throw ?
"""
static func raise_event (data, scope: Node, mode: int = WornEvents.PARENT_ONLY, recursive: bool = true, delay: float = 0.0, _run: bool = true) -> void:
	# The specified mode is not null.
	if mode > WornEvents.NONE:
		# The game is running.
		if !Engine.editor_hint:
			# If the game is not initialised.
			if delay == 0.0 && !is_game_initialised (): yield (scope.get_tree (), "idle_frame");
			# A delay has been given.
			elif delay > 0.0: yield (scope.get_tree ().create_timer (delay), "timeout");
		# Converting data into an Array.
		data = Array ([data]) if not data is Array else data;
		# Checks run value.
		if _run:
			# Runs the main node events.
			for datum in data: if datum is Dictionary: _event_manger (datum, scope);
		# For the children only.
		if mode == WornEvents.CHILDREN_ONLY:
			# Getting all children of the given scope.
			for child in scope.get_children ():
				# Runs all given events.
				for datum in data: if datum is Dictionary: _event_manger (datum, child);
				# Is it a recursive program ?
				if recursive and child.get_child_count () > 0: raise_event (data, child, mode, recursive, -1.0, false);
		# For the parents only.
		elif mode == WornEvents.PARENTS_ONLY:
			# Contains the parent of the curent node.
			var parent = scope.get_parent ();
			# Is it a child of the given parent ?
			if !recursive and parent is Node:
				# Runs all given events.
				for datum in data: if datum is Dictionary: _event_manger (datum, parent);
			# Otherwise.
			elif recursive:
				# While the current parent is a Node.
				while parent is Node:
					# Runs all given events and gets the parent of the current parent.
					for datum in data: if datum is Dictionary: _event_manger (datum, parent); parent = parent.get_parent ();
		# For all elements [parent(s) and child(ren)] node(s).
		else:
			# For the scope parent and children.
			raise_event (data, scope, WornEvents.PARENTS_ONLY, recursive, -1.0, false); raise_event (data, scope, WornEvents.CHILDREN_ONLY, recursive, -1.0, false);

"""
	@Description: Returns the real time (HH : MM : SS) from seconds.
	@Parameters:
		int seconds: Contains the time as seconds format.
		String divider: Contains a divider that will be used to separate each number of the returned time.
		bool parse: Do you want to return the result as a PoolIntArray ?
"""
static func get_time_from (seconds: int, divider: String = ':', parse: bool = false):
	# Contains the approximative minute(s) value.
	var pre_sec := (seconds / 60.0); var minutes = int (pre_sec) if str (pre_sec).find ('.') == -1 else int (str (pre_sec).split ('.') [0]);
	# Contains the real remaining seconds.
	seconds = (seconds - (minutes * 60)); var pre_min = (minutes / 60.0);
	# Contains the real hour(s) and remaining minute(s).
	var hours = int (pre_min) if str (pre_min).find ('.') == -1 else int (str (pre_min).split ('.') [0]); minutes = (minutes - (hours * 60));
	# Checks parse value.
	if parse: return PoolIntArray ([hours, minutes, seconds]);
	# Otherwise.
	else:
		# Converts hours into string format and minutes into string format.
		hours = ('0' + str (hours)) if len (str (hours)) == 1 else str (hours); minutes = ('0' + str (minutes)) if len (str (minutes)) == 1 else str (minutes);
		# Converts seconds into string format.
		var second: String = ('0' + str (seconds)) if len (str (seconds)) == 1 else str (seconds);
		# Returns the final value.
		return (hours + 'h' + divider + minutes + "min" + divider + second + 's');

"""
	@Description: Returns the corresponding type name of the given input.
	@Parameters:
		Variant input: Contains an object.
"""
static func get_type (input) -> String:
	# Checking input value type.
	if typeof (input) == TYPE_NIL || input == null: return "Nil"; elif typeof (input) == TYPE_BOOL: return "bool";
	elif typeof (input) == TYPE_INT: return "int"; elif typeof (input) == TYPE_REAL: return "float";
	elif str (input) == '' || typeof (input) == TYPE_STRING: return "String"; elif typeof (input) == TYPE_VECTOR2: return "Vector2";
	elif typeof (input) == TYPE_RECT2: return "Rect2"; elif typeof (input) == TYPE_VECTOR3: return "Vector3";
	elif typeof (input) == TYPE_NODE_PATH: return "NodePath"; elif typeof (input) == TYPE_PLANE: return "Plane";
	elif typeof (input) == TYPE_QUAT: return "Quat"; elif typeof (input) == TYPE_TRANSFORM: return "Transform";
	elif typeof (input) == TYPE_AABB: return "AABB"; elif typeof (input) == TYPE_BASIS: return "Basis";
	elif typeof (input) == TYPE_COLOR: return "Color"; elif typeof (input) == TYPE_ARRAY: return "Array";
	elif typeof (input) == TYPE_TRANSFORM2D: return "Transform2D"; elif typeof (input) == TYPE_RID: return "Rid";
	elif typeof (input) == TYPE_DICTIONARY: return "Dictionary"; elif typeof (input) == TYPE_RAW_ARRAY: return "PoolByteArray";
	elif typeof (input) == TYPE_INT_ARRAY: return "PoolIntArray"; elif typeof (input) == TYPE_REAL_ARRAY: return "PoolRealArray";
	elif typeof (input) == TYPE_STRING_ARRAY: return "PoolStringArray"; elif typeof (input) == TYPE_VECTOR2_ARRAY: return "PoolVector2Array";
	elif typeof (input) == TYPE_VECTOR3_ARRAY: return "PoolVector3Array"; elif typeof (input) == TYPE_COLOR_ARRAY: return "PoolColorArray";
	elif typeof (input) == TYPE_OBJECT: return input.get_class (); else: return "Unknown";

"""
	@Description: Determinates whether an input is an array. Include PoolStringArray, PoolRealArray, etc...
	@Parameters:
		Variant input: Contains a variable type value.
"""
static func is_array (input) -> bool: return input is Array || input is PoolStringArray || input is PoolIntArray || input is PoolByteArray || input is PoolRealArray\
	|| input is PoolVector2Array || input is PoolVector3Array || input is PoolColorArray;

"""
	@Description: Returns the corresponding line(s) from a search from a file. This function doesn't support normal and advanced level of Godot method.
	@Parameters:
		String | PoolStringArray inp: What do you which found from the target file ?
		String path: Where found the file ?
		String key: What is password of the file ? The key size must be in range (0, 32).
		bool case: Do you want to make a search with insensitive case ?
		int count: How many result(s) will be returned ? If this value is less than 0, you will get all found values.
		int mhd: What is the security method that have been used on the file ? The supported methods are:
			-> MegaAssets.SecurityMethod.NONE or 0: No security will be applied to data.
			-> MegaAssets.SecurityMethod.AES or 1: Data encryption with AES method.
			-> MegaAssets.SecurityMethod.ARCFOUR or 2: Data encryption with ARCFOUR method.
			-> MegaAssets.SecurityMethod.CHACHA or 3: Data encryption with CHACHA method.
			-> MegaAssets.SecurityMethod.BINARY or 4: Coding data to 2 bits.
			-> MegaAssets.SecurityMethod.HEXADECIMAL or 5: Coding data to 16 bits.
			-> MegaAssets.SecurityMethod.OCTAL or 6: Coding data to 8 bits.
			-> MegaAssets.SecurityMethod.GODOT or 7: Data encryption with GODOT method.
		int level: which security levels has been used on the file ? The supported levels are:
			-> MegaAssets.SecurityLevel.SIMPLE or 0: Simple level security.
			-> MegaAssets.SecurityLevel.NORMAL or 1: Normal level security.
			-> MegaAssets.SecurityLevel.ADVANCED or 2: Advanced level security.
"""
static func file_find (inp, path: String, key: String = String (''), case: bool = false, count: int = 1, mhd: int = SecurityMethod.GODOT, level: int = 1):
	# Corrects path separator and removes all spaces.
	path = path.replace ('\\', '/').replace (' ', ''); inp = PoolStringArray (Array ([inp]) if not is_array (inp) else Array (inp));
	# Checks path value.
	if !path.empty () && path.find_last ('.') != -1:
		# Creates a file instance and the final results.
		var file: File = File.new (); var fname: String = path.get_file (); var results: PoolStringArray = PoolStringArray ([]); var fstate: int = -1;
		# If the path to the target save file is invalid.
		if not file.file_exists (path): _output (("The file {" + fname + "} doesn't exists."), Message.WARNING);
		# For Godot normal encryption method.
		elif mhd == SecurityMethod.GODOT && level == SecurityLevel.NORMAL: fstate = file.open_encrypted_with_pass (path, File.READ, key);
		# For Godot advanced encryption method.
		elif mhd == SecurityMethod.GODOT && level >= SecurityLevel.ADVANCED:
			# Corrects the passed key and open the file in encrypted READ mode.
			while len (key) < 32: key += ' '; fstate = file.open_encrypted (path, File.READ, key.to_ascii ());
		# Opens the file as READ mode.
		elif file.open (path, File.READ) != OK: _output (("Failed to open {" + fname + "}."), Message.WARNING);
		# There are some warning.
		if mhd == SecurityMethod.GODOT && level > SecurityLevel.SIMPLE && fstate != OK:
			# If the file is corrupted.
			if fstate == ERR_FILE_CORRUPT: _output (("The file {" + fname + "} is corrupted."), Message.WARNING);
			# Warning message.
			else: _output (("Failed to open {" + fname + "}."), Message.WARNING);
		# Contains the first line of opened file.
		var line: String = file.get_line ();
		# While there are some data.
		while len (line) > 0 and count != 0:
			# Filters the current line to get more view.
			var filter: String = str_replace (line, PoolStringArray (["\n", "\t", "\a", "\b", "\r", "\v", "\f", '{', '}', '[', ']', ',', '"']), '');
			# Checks line value.
			if filter.length () > 0:
				# Contains the final result.
				var data = null;
				# No security has put.
				if mhd <= SecurityMethod.NONE or mhd > SecurityMethod.OCTAL: data = filter;
				# For AES method.
				elif mhd == SecurityMethod.AES:
					# For simple level.
					if level <= SecurityLevel.SIMPLE: data = decrypt (filter, key, EncryptionMethod.AES, EncryptionSchema.BASE64);
					# For normal level.
					elif level == SecurityLevel.NORMAL:
						# Decoding the encrypted line and decrypting of the given decoded data.
						data = _string_from_bit (filter, 2, false); data = decrypt (data, key, EncryptionMethod.AES, EncryptionSchema.HEXADECIMAL);
					# For advanced level.
					else:
						# Decoding the encrypted line and decrypting of the given decoded data.
						data = _string_from_bit (filter, 16, true); data = decrypt (data, key, EncryptionMethod.AES, EncryptionSchema.RAW);
				# For ARCFOUR method.
				elif mhd == SecurityMethod.ARCFOUR:
					# For simple level.
					if level <= SecurityLevel.SIMPLE: data = decrypt (filter, key, EncryptionMethod.ARCFOUR, EncryptionSchema.BASE64);
					# For normal level.
					elif level == SecurityLevel.NORMAL:
						# Decoding the encrypted line and decrypting of the given decoded data.
						data = _string_from_bit (filter, 2, false); data = decrypt (data, key, EncryptionMethod.ARCFOUR, EncryptionSchema.HEXADECIMAL);
					# For advanced level.
					else:
						# Decoding the encrypted line and decrypting of the given decoded data.
						data = _string_from_bit (filter, 16, true); data = decrypt (data, key, EncryptionMethod.ARCFOUR, EncryptionSchema.RAW);
				# For CHACHA method.
				elif mhd == SecurityMethod.CHACHA:
					# For simple level.
					if level <= SecurityLevel.SIMPLE: data = decrypt (filter, key, EncryptionMethod.CHACHA, EncryptionSchema.BASE64);
					# For normal level.
					elif level == SecurityLevel.NORMAL:
						# Decoding the encrypted line and decrypting of the given decoded data.
						data = _string_from_bit (filter, 2, false); data = decrypt (data, key, EncryptionMethod.CHACHA, EncryptionSchema.HEXADECIMAL);
					# For advanced level.
					else:
						# Decoding the encrypted line and decrypting of the given decoded data.
						data = _string_from_bit (filter, 16, true); data = decrypt (data, key, EncryptionMethod.CHACHA, EncryptionSchema.RAW);
				# For BINARY method.
				elif mhd == SecurityMethod.BINARY:
					# For simple level.
					if level <= SecurityLevel.SIMPLE: data = _string_from_bit (filter, 2, false);
					# For normal level.
					elif level == SecurityLevel.NORMAL: data = _string_from_bit (filter, 2, true);
					# For advanced level.
					else:
						# Decoding the encrypted line and decrypting of the given decoded data.
						data = _string_from_bit (filter, 2, true); data = decrypt (data, key, EncryptionMethod.AES, EncryptionSchema.BASE64);
				# For HEXADECIMAL method.
				elif mhd == SecurityMethod.HEXADECIMAL:
					# For simple level.
					if level <= SecurityLevel.SIMPLE: data = _string_from_bit (filter, 16, false);
					# For normal level.
					elif level == SecurityLevel.NORMAL: data = _string_from_bit (filter, 16, true);
					# For advanced level.
					else:
						# Decoding the encrypted line and decrypting of the given decoded data.
						data = _string_from_bit (filter, 16, true); data = decrypt (data, key, EncryptionMethod.ARCFOUR, EncryptionSchema.BASE64);
				# For OCTAL method.
				elif mhd == SecurityMethod.OCTAL:
					# For simple level.
					if level <= SecurityLevel.SIMPLE: data = _string_from_bit (filter, 8, false);
					# For normal level.
					elif level == SecurityLevel.NORMAL: data = _string_from_bit (filter, 8, true);
					# For advanced level.
					else:
						# Decoding the encrypted line and decrypting of the given decoded data.
						data = _string_from_bit (filter, 8, true); data = decrypt (data, key, EncryptionMethod.CHACHA, EncryptionSchema.BASE64);
				# Converts the current data as string format.
				data = str (data); var dup_dat: String = data.to_lower () if case else data;
				# Searches each input into the given decrypted string.
				for val in inp:
					# If ever a value from input is inside of the current line.
					if dup_dat.find (str (val).to_lower () if case else str (val)) != -1:
						# Adds it to results array and get out of the for loop.
						results.append (data); break;
				# Whether the results size is equal to count.
				if count > 0 and results.size () == count: break;
			# Go to the next line.
			line = file.get_line ();
		# Returns the final value.
		if results.size () <= 0: return null; elif results.size () == 1: return results [0]; else: return results;
	# Warning message.
	else: _output ("Invalid path.", Message.WARNING); return null;

"""
	@Description: Returns the corresponding index of an item from an array or dictionary.
	@Parameters:
		Variant datum: Contains a key or value.
		Array | Dictionary | String input: Contains the search field.
		bool reversed: Do you want to inverse treatment ?
		bool recursive: Do you want to use a recursive program to find index of a element ?
"""
static func get_index_of (datum, input, reversed: bool = false, recursive: bool = true) -> int:
	# The given input is a dictionary or array.
	if input is Dictionary:
		# Getting dictionary keys and starting value of the for loop.
		var keys: Array = input.keys ();
		# Checks elements from the passed input.
		for m in range ((0 if !reversed else (len (keys) - 1)), (len (keys) if !reversed else -1), (1 if !reversed else -1)):
			# The current element is equal to the passed element.
			if typeof (keys [m]) == typeof (datum) && keys [m] == datum or typeof (input [keys [m]]) == typeof (datum) && input [keys [m]] == datum: return m;
			# The current element key is a dictionary.
			elif recursive && keys [m] is Dictionary && get_index_of (datum, keys [m], reversed, recursive) > -1: return m;
			# The current element key is an array.
			elif recursive && is_array (keys [m]) && get_index_of (datum, keys [m], reversed, recursive) > -1: return m;
			# The current element value is a dictionary.
			elif recursive && input [keys [m]] is Dictionary && get_index_of (datum, input [keys [m]], reversed, recursive) > -1: return m;
			# The current element value is an array.
			elif recursive && is_array (input [keys [m]]) && get_index_of (datum, input [keys [m]], reversed, recursive) > -1: return m;
	# For an array.
	elif is_array (input):
		# Checks elements from the passed input.
		for k in range ((0 if !reversed else (len (input) - 1)), (len (input) if !reversed else -1), (1 if !reversed else -1)):
			# The current element is equal to the passed element.
			if typeof (input [k]) == typeof (datum) && input [k] == datum: return k;
			# The current element is a dictionary.
			elif recursive && input [k] is Dictionary && get_index_of (datum, input [k], reversed, recursive) > -1: return k;
			# The current element is an array.
			elif recursive && is_array (input [k]) && get_index_of (datum, input [k], reversed, recursive) > -1: return k;
	# For a string.
	elif input is String and datum is String: return input.find (datum) if !reversed else input.find_last (datum); return -1;

"""
	@Description: Parses a date. If no date passed, the current system date will be used.
	@Parameters:
		String format: What is your prefered date format ? Your date format must contains characters: 'd' to show
			the day, 'm' to show month and 'y' to show year. Eg: "dd-mm-yy", "yy/mm/dd", "mm:yy", "dd yy", etc...
		Dictionary date: Contains the date data. These data must have some keys: day, month and year.
		bool to_letter: Do you want to get the corresponding month name of the passed month number ?
		bool parse: Will return the date as Dictionnary format with an additional key (bixestile).
"""
static func parse_date (format: String = "dd-mm-yy", date: Dictionary = Dictionary ({}), to_letter: bool = false, parse: bool = false):
	# Removes right and left spaces.
	format = str_replace (format, PoolStringArray (["\n", "\t", "\a", "\b", "\r", "\v", "\f", ' ']), '');
	# Checks the passed format value.
	if !format.empty ():
		# Gets the now date whether no date is specify.
		date = OS.get_date (true) if len (date) <= 0 else date; date.bixestile = (((date.year % 4) == 0) || ((date.year % 400) == 0)) if date.has ("year") else false;
		# Gets position index of target characters.
		var entry: Array = Array ([format.find ('d'), format.find ('m'), format.find ('y')]);
		var sp: String = ' '; var ent: PoolStringArray = PoolStringArray (['', '', '', '']); 
		# Searches a separator.
		for chr in format:
			if chr != 'd' && chr != 'm' && chr != 'y':
				sp = chr; break;
		# For the day position.
		if entry [0] != -1:
			if entry [0] < entry [1] && entry [0] < entry [2]: ent [0] = 'd'; elif entry [0] > entry [1] && entry [0] < entry [2]: ent [1] = 'd';
			elif entry [0] < entry [1] && entry [0] > entry [2]: ent [1] = 'd'; elif entry [0] > entry [1] && entry [0] > entry [2]: ent [2] = 'd';
		# For the month position.
		if entry [1] != -1:
			if entry [1] < entry [0] && entry [1] < entry [2]: ent [0] = 'm'; elif entry [1] > entry [0] && entry [1] < entry [2]: ent [1] = 'm';
			elif entry [1] < entry [0] && entry [1] > entry [2]: ent [1] = 'm'; elif entry [1] > entry [0] && entry [1] > entry [2]: ent [2] = 'm';
		# For the year position.
		if entry [2] != -1:
			if entry [2] < entry [0] && entry [2] < entry [1]: ent [0] = 'y'; elif entry [2] > entry [0] && entry [2] < entry [1]: ent [1] = 'y';
			elif entry [2] < entry [0] && entry [2] > entry [1]: ent [1] = 'y'; elif entry [2] > entry [0] && entry [2] > entry [1]: ent [2] = 'y';
		# For string format.
		if to_letter and date.has ("month") and date.month is int:
			# For week months.
			match date.month:
				1: date.month = "January"; 2: date.month = "February"; 3: date.month = "March"; 4: date.month = "April";
				5: date.month = "May"; 6: date.month = "June"; 7: date.month = "July"; 8: date.month = "August";
				9: date.month = "September"; 10: date.month = "October"; 11: date.month = "November"; 12: date.month = "December";
		# Parsing results.
		for idx in range (3):
			if ent [idx] == 'd': ent [idx] = str (date.day) if date.has ("day") else ''; elif ent [idx] == 'm': ent [idx] = str (date.month) if date.has ("month") else '';
			elif ent [idx] == 'y': ent [idx] = str (date.year) if date.has ("year") else ''; ent [idx] = ent [idx].insert (0, '0') if len (ent [idx]) == 1 else ent [idx];
			ent [idx] = (ent [idx] + (sp if ent [idx].length () > 0 else ''));
		# Generating the parsed date.
		ent [3] = (ent [0] + ent [1] + ent [2]); var last: int = (ent [3].length () - 1); if last > -1 && ent [3] [last] == sp: ent [3] [last] = '';
		# Returns the final value.
		if parse: return date; elif !parse && ent [3].length () > 0: return ent [3]; return null;
	# Otherwise.
	else: return null;

"""
	@Description: Runs severals actions pre-configured from a dictionary.
	@Parameters:
		Dictionary data: Contains all configurations about the targets slots. The possible values are:
			-> String | NodePath source: What is the source where action will be done ? If the source key is
				not defined, the current object instance will be used.
			-> String action: Contains the target action. (Required). Eg: "run_slot ({action = "my_var"})",
				"run_slot ({action = "my_var", value = "?other_var"})", etc...
			-> Variant value: Contains a property value. Use this key when the action affect a property. If this key
				is not defined, you will get value of the target property.
			-> Variant params = Array ([]): Contains all arguments values of the target method. Use this key when the action
				affect a method. If this key isn't defined, the target method will be called with no parameters.
		It is also possible instead of a fixed value: whether at the level of a property or parameters of a method,
		to refer the value of another property or of a result returned by a method recursively. To be able to do this,
		we must use a character string in which we will have a special character: '?' placed in front of the name of the
		property or of the method whose value will be used as the value to be assigned to said property or parameter
		of the method in question. When you want to act on a method during a trigger, it is necessary to put at the
		end, the characters "()" in order to distinguish between a property and a method. By default, the events of a
		module impose conditions that must be respected by all elements under their responsibility. It is possible to
		break these requirements by disabling them with the require key. This option is only available on destructible modules.
		Node object: Which knot will be considered to make the differents operations ?
"""
static func run_slot (data: Dictionary, object: Node):
	# Checks source and action keys existance.
	var src = (data.source if data.source is String || data.source is NodePath else object.get_path ()) if data.has ("source") else object.get_path ();
	src = object.get_node_or_null (NodePath (str (src))); var act: String = data.action if data.has ("action") && data.action is String else '';
	# Checks source value.
	if src is Node:
		# Checks action value.
		if !act.empty ():
			# Checks errors.
			if act.count ("()") <= 1:
				# The action key is not a method.
				if not act.ends_with ("()"):
					# Checks value key existance.
					var val = data.value if data.has ("value") else null;
					# The value is defined.
					if val != null:
						# The val of the value key is a string format.
						if val is String:
							# Checks errors.
							if val.count ("()") <= 1 and val.count ('?') <= 1:
								# The current value call other value from other variable.
								if val.lstrip (' ').begins_with ('?'):
									# Removes '?' character with left and right spaces.
									val = val.split ('?'); val = val [1] if not val [1].empty () else '';
									# The value of this value is not a method.
									if not val.ends_with ("()"): set_var (act, get_var (val, src), src);
									# It's a method.
									else:
										# Checks parameters key presence to generate the corresponding dictionary.
										var ndic: Dictionary = Dictionary ({source = src, action = val, params = data.params
										# Sets value of the current referenced property to the method result.
										}) if data.has ("params") else Dictionary ({source = src, action = val}); set_var (act, run_slot (ndic, object), src);
								# Sets value of the referenced property normaly.
								else: set_var (act, val, src);
							# Error message.
							else: _output ("Invalid syntax.", Message.ERROR);
						# The value of the value key is a dictionary.
						elif val is Dictionary:
							# The passed dictionary is a keyword supported by this method or sets value of the referenced property normaly.
							if val.has ("action"): set_var (act, run_slot (val, object), src); else: set_var (act, val, src);
						# Sets value of the referenced property normaly.
						else: set_var (act, val, src);
					# Returns the final value from the referenced property.
					return get_var (act, src);
				# The action is a method.
				else:
					# Checks parameters key existance.
					var params: Array = (Array (data.params) if is_array (data.params) else Array ([data.params])) if data.has ("params") else Array ([]);
					# Checks whether there are some specials values.
					for t in params.size ():
						# If the current parameter call specials treatments.
						if params [t] is String && len (params [t]) > 0 && params [t].lstrip (' ').begins_with ('?'):
							# Corrects the current paramerter syntax.
							var cur_val = params [t].split ('?'); cur_val = cur_val [1] if !cur_val [1].empty () else '';
							# Generates a dictionary to this parameter.
							params [t] = run_slot (Dictionary ({source = object.get_path (), action = cur_val}), object);
						# If the current parameter is a dictionary.
						elif params [t] is Dictionary && params [t].has ("action"): params [t] = run_slot (params [t], object);
					# Calls the given method.
					return invoke (act.split ("()") [0], params, src);
			# Error message.
			else: _output ("Invalid syntax.", Message.ERROR);
		# Error message.
		else: _output ("Missing action.", Message.ERROR); return null;

"""
	@Description: Runs severals slots pre-configured from an array of dictionary.
	@Parameters:
		Dictionary | Array data: Contains all configurations about the targets slots. The dictionaries of this array support
			some keys that have been already describe on "run_slot ()" method definition. All elements of this array
			must be some dictionaries. It's very important to know that.
		Node object: Which knot will be considered to make the differents operations ?
		float delay: What is the timeout before running slots ?
"""
static func run_slots (data, object: Node, delay: float = 0.0) -> void:
	# Waiting for the given delay.
	if !Engine.editor_hint && delay > 0.0: yield (object.get_tree ().create_timer (delay), "timeout");
	# Converting data into an array and runs all passed actions.
	data = Array ([data]) if not is_array (data) else Array (data); for datum in data: if datum is Dictionary: run_slot (datum, object);

"""
	@Description: Returns the joystick category from his name.
	@Parameters:
		String jname: Contains the target gamepad name.
		Node object: Which knot will be considered to make the differents operations ?
		String path: Contains the path to additional gamepads list file. You can find this file at "res://addons/mega_ssets/nodes/base/mega_assets/gamepads.cfg".
			Use this parameter whether you want to add some gamepads with their category. To add some gamepads, use this nomenclature:
			[gamepads]
			ControllerName1 = ControllerCategory1 (Playstation, Xbox, Nitendo, etc...)
			ControllerName2 = ControllerName2 (Playstation, Xbox, Nitendo, etc...)
			...
			ControllerNameN = ControllerCategoryN (Playstation, Xbox, Nitendo, etc...)
"""
static func get_joy_category (jname: String, object: Node, path: String = "res://nodes/base/mega_assets/gamepads.cfg", _nme = null, _data = null) -> String:
	# Collects informations about detected controller.
	jname = (jname if _nme == null else _nme).to_lower (); _data = _check_joy_keys (jname) if _data == null else _data;
	# The detected controller is a Playstation gamepad.
	if _data [0] || _data [1] || _data [2] || _data [3] || _data [4] || _data [5] || _data [6]: return "PlayStation Controller";
	# The detected controller is an Xbox or Nintendo gamepad.
	elif _data [10] || _data [11] || _data [12] || _data [13]: return "Xbox Controller"; elif _data [14] || _data [15] || _data [18]: return "Nintendo Controller";
	# The detected controller is a Wii or Sega gamepad.
	elif _data [16] || _data [22]: return "Wii Controller"; elif _data [17] || _data [20] || _data [21]: return "Sega Controller";
	# The detected controller is a Neogeo gamepad.
	elif _data [19]: return "Neogeo Controller";
	# A path has been specified.
	elif not path.lstrip (' ').rstrip (' ').empty ():
		# Loads gamepads configurations.
		var configs: Dictionary = load_config_file (path, object);
		# Some gamepads has been specified.
		if configs.has ("gamepads") && configs.gamepads is Dictionary:
			# Checks others gamepads.
			for ky in configs.gamepads:
				# Converts the current key into a string format.
				var key: String = str (ky).to_lower ().lstrip ('_').rstrip ('_');
				# '_' characters have been detected.
				if key.find ('_') != -1:
					# Getting keywords.
					for keyword in key.split ('_'): if jname.find (keyword) != -1: return get_joy_category ('', object, '', configs.gamepads [ky], null);
				# The current joystick name is inside of the given gamepad name.
				elif jname.find (key) != -1: return get_joy_category ('', object, '', configs.gamepads [ky], null);
			# Unknown gamepad.
			return "Unknown Controller";
		# Unknown gamepad.
		else: return "Unknown Controller";
	# Unknown controller.
	else: return "Unknown Controller";

"""
	@Description: Returns all detected gamepads names.
	@Parameters:
		int limit: How many gamepads would you want to detect ?
"""
static func get_detected_controllers_names (limit: int = -1):
	# No limit is given or no gamepads found.
	if limit < 0: return get_connected_controllers_names (); elif limit == 0: return null;
	# Some limits have been found.
	else:
		# Getting connected controllers names.
		var gamepads: PoolStringArray = get_connected_controllers_names (); var results: PoolStringArray = PoolStringArray ([]);
		# The given limit is less that the gamepads size.
		if limit <= gamepads.size (): for p in range (0, limit, 1): results.append (gamepads [p]);
		# Otherwise.
		else: for p in range (0, gamepads.size (), 1): results.append (gamepads [p]);
		# Returns the final result.
		if results.size () <= 0: return null; elif results.size () == 1: return results [0]; else: return results;

"""
	@Description: Returns the corresponding key name of an input event from this controller. This feature doesn't work at any time.
	@Parameters:
		InputEvent key_event: Contains an input event key.
		Node object: Which knot will be considered to make the differents operations ?
		int device: Which joystick device listen ? (Only on joystick controller).
		String path: Contains the path pointing to the controller configuration file (.cfg) not recognized by this method. To edit this
			file, you must follow the following nomenclature:
			[gamepads]
			ControllerName1 = ControllerCategory1 (Playstation, Xbox, Nitendo, etc...)
			ControllerName2 = ControllerName2 (Playstation, Xbox, Nitendo, etc...)
			...
			ControllerNameN = ControllerCategoryN (Playstation, Xbox, Nitendo, etc...)
"""
static func get_input_key_translation (key_event: InputEvent, object: Node, device: int = 0, path: String = "res://nodes/base/mega_assets/gamepads.cfg") -> String:
	# For the keyboard controller.
	if key_event is InputEventKey: return OS.get_scancode_string (key_event.scancode);
	# For the mouse controller.
	elif key_event is InputEventMouse:
		# For mouse buttons.
		if key_event is InputEventMouseButton:
			# Checks mouse button.
			if key_event.button_index == BUTTON_LEFT: return "Mouse Button Left"; elif key_event.button_index == BUTTON_RIGHT: return "Mouse Button Right";
			elif key_event.button_index == BUTTON_MIDDLE || key_event.button_mask == BUTTON_MASK_MIDDLE: return "Mouse Button Middle";
			elif key_event.button_index == BUTTON_WHEEL_UP: return "Mouse Wheel Up"; elif key_event.button_index == BUTTON_WHEEL_DOWN: return "Mouse Wheel Down";
			elif key_event.button_index == BUTTON_WHEEL_LEFT: return "Mouse Wheel Left"; elif key_event.button_index == BUTTON_WHEEL_RIGHT: return "Mouse Wheel Right";
			elif key_event.button_index == BUTTON_XBUTTON1 || key_event.button_mask == BUTTON_MASK_XBUTTON1: return "Mouse XButton 1";
			elif key_event.button_index == BUTTON_XBUTTON2 || key_event.button_mask == BUTTON_MASK_XBUTTON2: return "Mouse XButton 2";
		# For mouse motions.
		elif key_event is InputEventMouseMotion:
			# Checks mouse axis.
			if key_event.relative [0] > 0.0: return "+MouseX"; elif key_event.relative [0] < 0.0: return "-MouseX";
			if key_event.relative [1] > 0.0: return "+MouseY"; elif key_event.relative [1] < 0.0: return "-MouseY";
	# For joystick buttons.
	elif key_event is InputEventJoypadButton:
		# Collects informations about detected controller.
		var jn: String = Input.get_joy_name (device).to_lower (); var data: Array = _check_joy_keys (jn);
		var cn: String = Input.get_joy_button_string (key_event.button_index if key_event.button_index < JOY_BUTTON_MAX else 0);
		cn = "Max" if key_event.button_index == JOY_BUTTON_MAX else cn; var cat: String = get_joy_category (jn, object, path, null, data);
		# The detected controller is a Playstation gamepad.
		if cat == "PlayStation Controller":
			# Checks the button name.
			if cn == "L3" && data [7] || cn == "L2" && data [7]: return "Beat The Drum Left";
			elif cn == "R3" && data [7] || cn == "R2" && data [7]: return "Beat The Drum Right";
			elif cn == "L3" && data [8] || cn == "L2" && data [8]: return "Beat The Drum Left";
			elif cn == "R3" && data [8] || cn == "R2" && data [8]: return "Beat The Drum Right";
			elif cn == "Start" && data [9]: return "Options"; elif cn == "Face Button Top" && data [0]: return '1';
			elif cn == "Face Button Right" && data [0]: return '2'; elif cn == "Face Button Bottom" && data [0]: return '3';
			elif cn == "Face Button Left" && data [0]: return '4'; elif cn == "Face Button Right": return 'O';
			elif cn == "Select" && data [9]: return "Share"; elif cn == "Face Button Bottom": return 'X';
			elif cn == "Face Button Top": return "Triange"; elif cn == 'L': return "L1"; elif cn == 'R': return "R1";
			elif cn == "Face Button Left": return "Square"; else: return cn;
		# The detected controller is an Xbox gamepad.
		elif cat == "Xbox Controller":
			# Checks the button name.
			if cn == "Select" && data [12]: return "View"; elif cn == "Start" && data [12]: return "Menu";
			elif cn == "Select" && data [11]: return "Back"; elif cn == "L2" || key_event.get ("axis") == JOY_AXIS_6: return "LT";
			elif cn == 'L': return "LB"; elif cn == 'R': return "RB"; elif cn == "R2" || key_event.get ("axis") == JOY_AXIS_7: return "RT";
			elif cn == "Face Button Top": return 'Y'; elif cn == "Face Button Bottom": return 'A'; elif cn == "Select": return "Back";
			elif cn == "Face Button Left": return 'X'; elif cn == "Face Button Right": return 'B'; else: return cn;
		# The detected controller is a Nintendo, Wii, Sega, Neogeo gamepad.
		elif cat == "Nintendo Controller" || cat == "Wii Controller" || cat == "Sega Controller" || cat == "Neogeo Controller":
			if cn == "Face Button Bottom" && data [22]: return '2'; elif cn == "Face Button Left" && data [22]: return '1';
			elif cn == 'L' && data [22]: return 'C'; elif cn == "L2" && data [22] || key_event.get ("axis") == JOY_AXIS_6 && data [22]: return "Z+";
			elif cn == "Face Button Top" && data [18]: return 'C'; elif cn == "Face Button Right" && data [18]: return 'D';
			elif cn == "Face Button Left" && data [18]: return 'A'; elif cn == "Select" && data [18]: return "Select";
			elif cn == "Start" && data [18]: return "Start"; elif cn == 'R' && data [19]: return 'C';
			elif cn == "Select" && data [19]: return "Select"; elif cn == "Start" && data [19]: return "Start";
			elif cn == 'L' && data [20]: return 'Z'; elif cn == "Select" && data [20]: return "Mode";
			elif cn == 'R' && data [20]: return 'C'; elif cn == "Start" && data [20]: return "Start";
			elif cn == "L3" && data [7] || cn == "L2" && data [7]: return "Beat The Drum Left";
			elif cn == "R3" && data [7] || cn == "R2" && data [7]: return "Beat The Drum Right";
			elif cn == "L3" && data [8] || cn == "L2" && data [8]: return "Beat The Drum Left";
			elif cn == "R3" && data [8] || cn == "R2" && data [8]: return "Beat The Drum Right";
			elif cn == "Face Button Top": return 'X'; elif cn == "Face Button Bottom": return 'B';
			elif cn == "Face Button Left": return 'Y'; elif cn == "Face Button Right": return 'A';
			elif cn == "L2" || key_event.get ("axis") == JOY_AXIS_6: return "ZL"; elif cn == "R2" || key_event.get ("axis") == JOY_AXIS_7: return "ZR";
			elif cn == "Select": return "---"; elif cn == "Start": return "-|-"; else: return cn;
		# Unknown gamepad.
		else: return cn;
	# For joystick motions.
	elif key_event is InputEventJoypadMotion:
		# Collects informations about detected controller.
		var ax: float = get_filtered_joy_axis (Input.get_joy_axis (device, key_event.axis));
		var axn: String = ("-Axis" + str (key_event.axis)) if ax < 0.0 else ("+Axis" + str (key_event.axis)); axn = '' if ax == 0.0 else axn;
		# Checks controller axis.
		if axn == "-Axis0" && ax < 0.0: return "-Left Analog X"; elif axn == "+Axis0" && ax > 0.0: return "+Left Analog X";
		elif axn == "-Axis1" && ax < 0.0: return "-Left Analog Y"; elif axn == "+Axis1" && ax > 0.0: return "+Left Analog Y";
		elif axn == "-Axis2" && ax < 0.0: return "-Right Analog X"; elif axn == "+Axis2" && ax > 0.0: return "+Right Analog X";
		elif axn == "-Axis3" && ax < 0.0: return "-Right Analog Y"; elif axn == "+Axis3" && ax > 0.0: return "+Right Analog Y";
		elif axn == "-Axis6" && ax < 0.0: return "-Left Trigger"; elif axn == "+Axis6" && ax < 0.0: return "+Left Trigger";
		elif axn == "-Axis7" && ax < 0.0: return "-Right Trigger"; elif axn == "+Axis7" && ax < 0.0: return "+Right Trigger"; else: return "Null";
	# Unknown input.
	return "Null";

"""
	@Description: Determinates whether an input event is a button.
	@Parameters:
		InputEvent key_event: Contains an input event key.
"""
static func is_button (key_event: InputEvent) -> bool: return key_event is InputEventKey || key_event is InputEventMouseButton || key_event is InputEventJoypadButton;

"""
	@Description: Determinates whether an input event is an axis.
	@Parameters:
		InputEvent key_event: Contains an input event key.
"""
static func is_axis (key_event: InputEvent) -> bool: return key_event is InputEventMouseMotion || key_event is InputEventJoypadMotion;

"""
	@Description: Returns a filtered axis of a joystick axis.
	@Parameters:
		float axis: Contains a joystick axis value.
"""
static func get_filtered_joy_axis (axis: float) -> float:
	# Converts the real basic axis into string format and finds whether a comma and full stop.
	var asx = str (axis); if asx.find ('.') != -1: asx = asx.split ('.'); elif asx.find (',') != -1: asx = asx.split (',');
	# Filtering the parsed axis and returns the filtered axis value.
	asx = (asx [0] + '.' + asx [1] [0]) if asx is PoolStringArray else asx; asx = asx as float; return abs (asx) if asx == 0.0 else asx;

"""
	@Description: Determinates whether an input event is pressed.
	@Parameters:
		InputEvent key_event: Contains an input event key.
		int device: Which joystick device listen ? (Only on joystick controller).
"""
static func is_key_or_axis_pressed (key_event: InputEvent, device: int = 0) -> bool:
	# For the keyboard buttons.
	if key_event is InputEventKey: return Input.is_key_pressed (key_event.scancode);
	# For mouse buttons.
	elif key_event is InputEventMouseButton: return Input.is_mouse_button_pressed (key_event.button_index);
	# For joystick butttons.
	elif key_event is InputEventJoypadButton: return Input.is_joy_button_pressed (device, key_event.button_index);
	# For mouse axis.
	elif key_event is InputEventMouseMotion && key_event.relative != Vector2.ZERO: return true;
	# For joystick axis.
	elif key_event is InputEventJoypadMotion && get_filtered_joy_axis (key_event.axis_value) != 0.0: return true; else: return false;

"""@Description: Returns all connected joypads to the computer."""
static func get_connected_controllers_names () -> PoolStringArray:
	# Contains all connected controllers.
	var conts: PoolStringArray = PoolStringArray ([]); conts.append ("Mouse"); conts.append ("Keyboard");
	# Gets all connected joypads ids.
	for joy_id in Input.get_connected_joypads (): conts.append (Input.get_joy_name (joy_id)); return conts;

"""
	@Description: Returns the corresponding keycode of a pressed key as string format.
	@Parameters:
		InputEvent key_event: Contains an input event key.
		int device: Which joystick device listen ? (Only on joystick controller).
		bool mask: Do you want to get mouse keycode from a mask ? (Only on mouse controller).
"""
static func get_keycode_string (key_event: InputEvent, device: int = 0, mask: bool = false) -> String:
	# For the keyboard controller.
	if key_event is InputEventKey: return OS.get_scancode_string (key_event.scancode);
	# For the mouse controller.
	elif key_event is InputEventMouse:
		# For mouse buttons.
		if key_event is InputEventMouseButton:
			# The key is the mouse wheel.
			if key_event.button_index == BUTTON_WHEEL_UP: return "-ScrollY"; elif key_event.button_index == BUTTON_WHEEL_DOWN: return "+ScrollY";
			elif key_event.button_index == BUTTON_WHEEL_LEFT: return "-ScrollX"; elif key_event.button_index == BUTTON_WHEEL_RIGHT: return "+ScrollX";
			else: return ("Mouse" + str (key_event.button_index)) if !mask else ("Mouse" + str (key_event.button_mask));
		# For mouse motions.
		elif key_event is InputEventMouseMotion:
			# Checks mouse axis.
			if key_event.relative.x > 0.0 and key_event.relative.y > 0.0: return "+MouseX++MouseY";
			elif key_event.relative.x < 0.0 and key_event.relative.y < 0.0: return "-MouseX+-MouseY";
			elif key_event.relative.x < 0.0 and key_event.relative.y > 0.0: return "-MouseX++MouseY";
			elif key_event.relative.x > 0.0 and key_event.relative.y < 0.0: return "+MouseX+-MouseY";
			elif key_event.relative.x < 0.0: return "-MouseX"; elif key_event.relative.x > 0.0: return "+MouseX";
			elif key_event.relative.y < 0.0: return "-MouseY"; elif key_event.relative.y > 0.0: return "+MouseY";
			elif key_event.relative.x == 0.0: return "MouseX"; elif key_event.relative.y == 0.0: return "MouseY"; else: return "MouseX+MouseY";
	# For joystick buttons.
	elif key_event is InputEventJoypadButton: return ("Joy" + str (key_event.button_index));
	# For joystick motions.
	elif key_event is InputEventJoypadMotion:
		# Collects informations about detected controller.
		var ax: float = get_filtered_joy_axis (Input.get_joy_axis (device, key_event.axis));
		var axn: String = ("-Axis" + str (key_event.axis)) if ax < 0.0 else (("+Axis" + str (key_event.axis)) if ax > 0.0 else ("Axis" + str (key_event.axis)));
		# Returns the final result.
		return axn;
	# Returns a fake value.
	return "Null";

"""
	@Description: Moves a kinematic body from a root motion transform of a skeleton.
	@Parameters:
		Dictionary data:
			-> AnimationTree animtree: Contains an AnimationTree node instance.
			-> KinematicBody body: Contains a KinematicBody node instance.
			-> float delta: Contains the elapsed time since the previous frame.
			-> float gravity = -9.8: Contains the gravity value.
			-> float weight = 1.0: What is the weight of the body ?
			-> bool correction = false: Do you want to correct root motion transform ? Useful for bad importations.
			-> int trdir = MegaAssets.Axis._XZ: Contains the root motion translation direction.
			-> int rtdir = MegaAssets.Axis.Y: Contains the root motion rotation direction.
			-> int axis = MegaAssets.Axis.YZ: What is the axis permutation case ? (only on correction is enabled).
			-> Vector3 scale: Contains the scale of the root motion transform basis. 
		float delay: What is the timeout before starting root motion ?
"""
func apply_root_motion (data: Dictionary, delay: float = 0.0) -> void:
	# The game is running.
	if !Engine.editor_hint:
		# If the game is not initialised.
		if delay == 0.0 && !self.is_game_initialised (): yield (self.get_tree (), "idle_frame");
		# Otherwise.
		elif delay > 0.0: yield (self.get_tree ().create_timer (delay), "timeout");
	# Contains the default gravity.
	var default_gravity: Vector3 = (ProjectSettings.get_setting ("physics/3d/default_gravity") * ProjectSettings.get_setting ("physics/3d/default_gravity_vector"));
	# Geting the animation tree value.
	data.animtree = (data.animtree if data.animtree is AnimationTree else null) if data.has ("animtree") else null;
	# Geting the Kinematicbody value.
	data.body = (data.body if data.body is KinematicBody else null) if data.has ("body") else null;
	# Getting the passed delta value.
	data.delta = float (data.delta) if data.has ("delta") and self.is_number (data.delta) else 0.0;
	# Getting the object weight.
	data.weight = float (data.weight) if data.has ("weight") and self.is_number (data.weight) else 1.0;
	# Getting translation direction.
	data.trdir = (int (data.trdir) if self.is_number (data.trdir) else self.Axis._Z) if data.has ("trdir") else self.Axis._XZ;
	# Getting rotation direction.
	data.rtdir = (int (data.rtdir) if self.is_number (data.rtdir) else self.Axis.Y) if data.has ("rtdir") else self.Axis.Y;
	# Getting correction value.
	data.correction = data.correction if data.has ("correction") and data.correction is bool else false;
	# Getting gravity value.
	data.gravity = float (data.gravity * data.weight) if data.has ("gravity") and self.is_number (data.gravity) else (default_gravity.y * data.weight);
	# Getting axis permutation case.
	data.axis = (int (data.axis) if self.is_number (data.axis) else self.Axis.YZ) if data.has ("axis") else self.Axis.YZ;
	# The found node is an animation tree.
	if data.animtree is AnimationTree:
		# The found node is a kinematic body.
		if data.body is KinematicBody:
			# Gets the current root bone motion.
			var rtm: Transform = data.animtree.get_root_motion_transform (); var rad: Quat = rtm.basis.get_rotation_quat ();
			# Getting scale vector.
			data.scale = any_to_vector3 (data.scale) if data.has ("scale") else rtm.basis.get_scale ();
			# Some corrections should be applyed.
			if data.correction:
				# For XY permutation.
				if data.axis <= self.Axis.XY:
					# Root motion translation and rotation on x, y and z axis.
					rtm = self._root_motion_manager (data, rtm.origin.y, rtm.origin.x, rtm.origin.z, rad.y, rad.x, rad.z, rad.w);
					# Corrects root motion transform speed.
					rtm.origin = (rtm.origin if data.trdir == self.Axis.Z or data.trdir == self.Axis._Z else (rtm.origin * 0.13));
				# For XZ permutation.
				elif data.axis == self.Axis.XZ:
					# Root motion translation and rotation on x, y and z axis.
					rtm = self._root_motion_manager (data, rtm.origin.z, rtm.origin.y, rtm.origin.x, rad.z, rad.y, rad.x, rad.w);
					# Corrects root motion transform speed.
					rtm.origin = (rtm.origin if data.trdir == self.Axis.Y or data.trdir == self.Axis._Y else (rtm.origin * 0.13));
				# For YZ permutation.
				else:
					# Root motion translation and rotation on x, y and z axis.
					rtm = self._root_motion_manager (data, rtm.origin.x, rtm.origin.z, rtm.origin.y, rad.x, rad.z, rad.y, rad.w);
					# Corrects root motion transform speed.
					rtm.origin = (rtm.origin if data.trdir == self.Axis.X or data.trdir == self.Axis._X else (rtm.origin * 0.13));
			# Otherwise.
			else: rtm = self._root_motion_manager (data, rtm.origin.x, rtm.origin.y, rtm.origin.z, rad.x, rad.y, rad.z, rad.w);
			# Calculates animation velocity.
			__cash__.velocity.y += (data.gravity * data.delta); var body_transform: Transform = data.body.get_global_transform ();
			# Removes KinematicBody position and calculates the body transform from the root motion.
			body_transform.origin *= Vector3.ZERO; body_transform *= rtm; var body_velocity: Vector3 = (body_transform.origin / data.delta);
			# Moves the target KinematicBody with animation root motion speed.
			body_velocity.y = self.__cash__.velocity.y; __cash__.velocity = data.body.move_and_slide (body_velocity, Vector3.UP);
			# Gets the mesh scale.
			var scaler: Vector3 = data.scale if data.scale is Vector3 else rtm.basis.get_scale ();
			# Calculates orientation and clears mesh transform data.
			__cash__.orientation *= rtm; __cash__.orientation.origin = Vector3.ZERO;
			# Apply animation rotation or quaternion.
			data.body.global_transform.basis = self.__cash__.orientation.basis.orthonormalized ().scaled (scaler);
		# Error message.
		else: self._output ("Missing a KinematicBody instance.", self.Message.ERROR);
	# Error message.
	else: self._output ("Missing an AnimationTree instance.", self.Message.ERROR);

"""
	@Description: Apply a camera effect with some ajustments. This effects are provided by the framework.
	@Parameters:
		String | NodePath pvt: Contains the privot of effect. (Only on Sprite and TextureRect).
		int eft: What is the effect that will be applyed ?
		Node obj: Which knot will be considered to make the differents operations ?
		int lyt: Do you want to put a layout to your effect ? The possibles values are:
			MegaAssets.Disposal.NONE or 0: No layout will be applyed.
			MegaAssets.Disposal.CENTER or 1: Center effect.
			MegaAssets.Disposal.TOP or 2: Move effect to screen top.
			MegaAssets.Disposal.RIGHT or 3: Move effect to screen right.
			MegaAssets.Disposal.BOTTOM or 4: Move effect to screen bottom.
			MegaAssets.Disposal.LEFT or 5: Move effect to screen left.
		int fus: Do you want to resize your effect ? The possibles values are:
			MegaAssets.FullMonitor.NONE or 0: No resize will be applyed.
			MegaAssets.FullMonitor.HORIZONTAL or 1: Horizontal resize.
			MegaAssets.FullMonitor.VERTICAL or 2: Vertical resize.
			MegaAssets.FullMonitor.BOTH or 3: Horizontal and vertical resize.
		float delay: What is the timeout before apply the target effect ?
"""
static func apply_camera_effect (pvt: NodePath, eft: int, obj: Node, lyt: int = 0, fus: int = 0, delay: float = 0.0) -> void:
	# The game is running.
	if !Engine.editor_hint:
		# If the game is not initialised.
		if delay == 0.0 && !is_game_initialised (): yield (obj.get_tree (), "idle_frame"); elif delay > 0.0: yield (obj.get_tree ().create_timer (delay), "timeout");
	# Gets the real node of privot.
	var privot = obj.get_node_or_null (pvt);
	# Checks privot type.
	if privot is TextureRect || privot is Sprite:
		# Contains the game viewport.
		var app: Viewport = obj.get_viewport ();
		# The effect targets an object of control node.
		if privot is TextureRect:
			# For horizontal fullscreen.
			if fus == FullMonitor.HORIZONTAL:
				privot.rect_position.x = 0; privot.rect_size.x = app.size.x;
			# For vertical fullscreen.
			elif fus == FullMonitor.VERTICAL:
				privot.rect_position.y = 0; privot.rect_size.y = app.size.y;
			# For vertical and horizontal fullscreen.
			elif fus >= FullMonitor.BOTH:
				privot.rect_position = Vector2.ZERO; privot.rect_size = app.size;
			# Layout center.
			if lyt == Disposal.CENTER && privot.rect_size.x < app.size.x: privot.rect_position.x = ((app.size.x / 2) - (privot.rect_size.x / 2));
			if lyt == Disposal.CENTER && privot.rect_size.y < app.size.y: privot.rect_position.y = ((app.size.y / 2) - (privot.rect_size.y / 2));
			# Layout top.
			elif lyt == Disposal.TOP && privot.rect_size.y < app.size.y: privot.rect_position.y = 0;
			# Layout right.
			elif lyt == Disposal.RIGHT && privot.rect_size.x < app.size.x: privot.rect_position.x = (app.size.x - privot.rect_size.x);
			# Layout bottom.
			elif lyt == Disposal.BOTTOM && privot.rect_size.y < app.size.y: privot.rect_position.y = (app.size.y - privot.rect_size.y);
			# Layout left.
			elif lyt >= Disposal.LEFT && privot.rect_size.x < app.size.x: privot.rect_position.x = 0;
		# Otherwise.
		else:
			# Configures the passed sprite instance.
			if fus > FullMonitor.NONE || lyt > Disposal.NONE:
				privot.centered = true; privot.offset = Vector2.ZERO; privot.region_enabled = false;
			# For horizontal, vertical fullscreen.
			if fus == FullMonitor.HORIZONTAL: privot.scale.x = app.size.x; elif fus == FullMonitor.VERTICAL: privot.scale.y = app.size.y;
			# For vertical and horizontal fullscreen.
			elif fus >= FullMonitor.BOTH: privot.scale = Vector2 ((app.size.x / 63), (app.size.y / 63));
			# Layout center.
			if lyt == Disposal.CENTER: privot.position = Vector2 (((app.size.x / 2) - (privot.scale.x / 2)), ((app.size.y / 2) - (privot.scale.y / 2)));
			# Layout top.
			elif lyt == Disposal.TOP && privot.scale.y < app.size.y: privot.position.y = (privot.scale.y * 32);
			# Layout right.
			elif lyt == Disposal.RIGHT && privot.scale.x < app.size.x: privot.position.x = (app.size.x - (privot.scale.x * 32));
			# Layout bottom.
			elif lyt == Disposal.BOTTOM && privot.scale.y < app.size.y: privot.position.y = (app.size.y - (privot.scale.y * 32));
			# Layout left.
			elif lyt >= Disposal.LEFT && privot.scale.x < app.size.x: privot.position.x = (privot.scale.x * 32);
		# Contains an instance of an effect and apply the default image.
		var effect = null; var root_folders: String = "res://nodes/camera/camera_effects/"; privot.texture = load (root_folders + "white.png");
		# Checks given effect that will be loaded.
		if eft == CameraEffect.NONE:
			privot.material = null; return;
		elif eft == CameraEffect.SIMLE_BLUR: effect = load (root_folders + "simple_blur.shader");
		elif eft == CameraEffect.VIGNETTE: effect = load (root_folders + "vignette.shader");
		elif eft == CameraEffect.PIXELIZE: effect = load (root_folders + "pixelize.shader");
		elif eft == CameraEffect.WHIRL: effect = load (root_folders + "whirl.shader");
		elif eft == CameraEffect.SEPIA: effect = load (root_folders + "sepia.shader");
		elif eft == CameraEffect.NEGATIVE: effect = load (root_folders + "negative.shader");
		elif eft == CameraEffect.CONTRASTED: effect = load (root_folders + "contrasted.shader");
		elif eft == CameraEffect.NORMALIZED: effect = load (root_folders + "normalized.shader");
		elif eft == CameraEffect.BCS: effect = load (root_folders + "bcs.shader");
		elif eft == CameraEffect.MIRAGE: effect = load (root_folders + "mirage.shader");
		elif eft == CameraEffect.OLD_FILM: effect = load (root_folders + "old_film.shader");
		elif eft == CameraEffect.STATIC_CRT: effect = load (root_folders + "static_crt.shader");
		elif eft == CameraEffect.MOSAIC: effect = load (root_folders + "mosaic2d.shader");
		elif eft == CameraEffect.LCD: effect = load (root_folders + "lcd.shader");
		elif eft == CameraEffect.GAMEBOY: effect = load (root_folders + "gameboy_filter.shader");
		elif eft == CameraEffect.TONE_COMIC: effect = load (root_folders + "tone_comic.shader");
		elif eft == CameraEffect.INVERT: effect = load (root_folders + "invert.shader");
		elif eft == CameraEffect.TV: effect = load (root_folders + "tv.shader");
		elif eft == CameraEffect.VHS: effect = load (root_folders + "vhs.shader");
		elif eft == CameraEffect.VHS_GLITCH: effect = load (root_folders + "vhs_glitch.shader");
		elif eft == CameraEffect.VHS_PAUSE: effect = load (root_folders + "vhs_pause.shader");
		elif eft == CameraEffect.VHS_SIMPLE_GLITCH: effect = load (root_folders + "vhs_simple_glitch.shader");
		elif eft == CameraEffect.BW: effect = load (root_folders + "bw.shader");
		elif eft == CameraEffect.BETTER_CC: effect = load (root_folders + "better_cc.shader");
		elif eft == CameraEffect.COLOR_PRECISION: effect = load (root_folders + "color_precision.shader");
		elif eft == CameraEffect.GRAIN: effect = load (root_folders + "grain.shader");
		elif eft == CameraEffect.LENS_DISTORTION: effect = load (root_folders + "lens_distortion.shader");
		elif eft == CameraEffect.SHARPNESS: effect = load (root_folders + "sharpness.shader");
		elif eft == CameraEffect.SIMPLE_GRAIN: effect = load (root_folders + "simple_grain.shader");
		elif eft == CameraEffect.RANDOM_NOISE: effect = load (root_folders + "random_noise.shader");
		elif eft == CameraEffect.SCANLINES: effect = load (root_folders + "scanlines.shader");
		elif eft == CameraEffect.GLITCH: effect = load (root_folders + "glitch.shader");
		elif eft == CameraEffect.CRT_SCREEN: effect = load (root_folders + "crt_screen.shader");
		elif eft == CameraEffect.SIMPLE_CRT: effect = load (root_folders + "simple_crt.shader");
		elif eft == CameraEffect.SIMPLE_GLITCH: effect = load (root_folders + "simple_glitch.shader");
		elif eft == CameraEffect.CRT_LOTTES: effect = load (root_folders + "crt_lottes.shader");
		elif eft == CameraEffect.ABERRATION: effect = load (root_folders + "aberration.shader");
		elif eft == CameraEffect.ADVANCED_MOSIC: effect = load (root_folders + "advanced_mosic.shader");
		elif eft == CameraEffect.ANIMATED_NOISE: effect = load (root_folders + "animated_noise.shader");
		elif eft == CameraEffect.AVERAGE: effect = load (root_folders + "average.shader");
		elif eft == CameraEffect.BACKGROUND: effect = load (root_folders + "background.shader");
		elif eft == CameraEffect.BINARY_CONVERSION: effect = load (root_folders + "binary_conversion.shader");
		elif eft == CameraEffect.BINARY_DEFAULT_MIX: effect = load (root_folders + "binary_default_mix.shader");
		elif eft == CameraEffect.COLOR_BLINDNESS: effect = load (root_folders + "color_blindness.shader");
		elif eft == CameraEffect.DEFAULT: effect = load (root_folders + "default.shader");
		elif eft == CameraEffect.EDGE_DEFAULT_MIX: effect = load (root_folders + "edge_default_mix.shader");
		elif eft == CameraEffect.EDGE_MOTION_MIX: effect = load (root_folders + "edge_motion_mix.shader");
		elif eft == CameraEffect.EDGE_PREWITT: effect = load (root_folders + "edge_prewitt.shader");
		elif eft == CameraEffect.SIMPLE_EDGE: effect = load (root_folders + "edge_simple.shader");
		elif eft == CameraEffect.EDGE_SOBEL: effect = load (root_folders + "edge_sobel.shader");
		elif eft == CameraEffect.MONOCHROME: effect = load (root_folders + "monochrome.shader");
		elif eft == CameraEffect.MOTION: effect = load (root_folders + "motion.shader");
		else: effect = load (root_folders + "simple_mosic.shader");
		# Updates object shader.
		var shader: ShaderMaterial = ShaderMaterial.new (); shader.shader = effect; privot.material = shader;
		# For vignette effect.
		if eft == CameraEffect.VIGNETTE: privot.material.set_shader_param ("vignette", load (root_folders + "vignette.png"));
		# For sepia effect.
		elif eft == CameraEffect.SEPIA: privot.material.set_shader_param ("base", Color.silver);
		# For old film effect.
		elif eft == CameraEffect.OLD_FILM:
			# Changes base color to gray and loads film grain texture.
			privot.material.set_shader_param ("base", Color.gray); privot.material.set_shader_param ("grain", load (root_folders + "filmgrain.png"));
			# Loads vignette texture.
			privot.material.set_shader_param ("vignette", load (root_folders + "vignette.png"));
		# For lcd effect.
		elif eft == CameraEffect.LCD: privot.material.set_shader_param ("residual_image_tex", 0);
		# For grain effect.
		elif eft == CameraEffect.GRAIN: privot.material.set_shader_param ("screen_size", app.size);
	# Error message.
	else: _output ("The passed node must be an instance of Sprite or TextureRect.", Message.ERROR);

"""
	@Description: Apply occlusion culling process from a single camera. The target object(s) must have some instances
		of the following class types: KinematicBody, RigidBody, Area, StaticBody, SoftBody.
	@Parameters:
		Node | String | NodePath cam: Contains an instance of a Camera class.
		Array nodes: Contains all target bodies. These bodies will be affected by the occlusion.
		float delta: What is he time from the preview game frame ?
		float acy: Contains the occlusion accuracy.
		float scans: Contains the occlusion scans per second.
		float delay: What is the timeout before starting occlusion culling ?
"""
func apply_occlusion_culling (cam, nodes: Array, delta: float, acy: float = 1000.0, scans: float = 10.0, delay: float = 0.0) -> void:
	# The game is running.
	if !Engine.editor_hint:
		# If the game is not initialised.
		if delay == 0.0 && !self.is_game_initialised (): yield (self.get_tree (), "idle_frame");
		# A delay has been given.
		elif delay > 0.0: yield (self.get_tree ().create_timer (delay), "timeout");
	# Gets the real camera node.
	cam = self.get_node_or_null (cam) if cam is String or cam is NodePath else cam;
	# Checks camera value.
	if cam is Camera:
		# Contains the ray screen position values.
		var nums: Array = Array ([0.0, 0.0, 0.0]);
		# Checks meshes value.
		if nodes.size () > 0:
			# Adjusts occlusion culling number.
			nums [0] =  (1 / (acy / 20)); nums [1] = (1 / (acy / 50)); nums [2] = (acy * 0.05);
			# Whether the time before a rescaning is greater than a certains value.
			if self.__cash__.occ_data [1] > (1.0 / scans):
				# Disables all given meshes visibility.
				for body in nodes: body.set ("visible", false); __cash__.occ_data [0] [0] = 1.0; __cash__.occ_data [0] [1] = 1.0;
				# Apply occlusion culling.
				var app: Viewport = self.get_viewport (); while ((app.size.y * nums [1]) * self.__cash__.occ_data [0] [1]) < app.size.y:
					# Gets impact point from camera to his target.
					var pt: Vector2 = Vector2 (((app.size.x * nums [0]) * self.__cash__.occ_data [0] [0]), ((app.size.y * nums [1]) * self.__cash__.occ_data [0] [1]));
					# Projects ray at this point from camera.
					var pos: Vector3 = cam.project_ray_origin (pt); var far: Vector3 = (pos + cam.project_ray_normal (pt) * cam.far);
					# Gets detected object by the occlusion.
					var obj: Dictionary = cam.get_world ().direct_space_state.intersect_ray (pos, far, Array ([cam]), true, true);
					# Whether the given object is defined and increases ray position.
					if !obj.empty () and nodes.has (obj.collider): obj.collider.set ("visible", true); __cash__.occ_data [0] [0] += 1;
					# Whether ray position is occlusion number.
					if self.__cash__.occ_data [0] [0] > nums [2]:
						# Updates ray position.
						__cash__.occ_data [0] [0] = 1.0; __cash__.occ_data [0] [1] += 1;
				# Resets the time before rescaning.
				__cash__.occ_data [1] = 0.0;
			# Updates the time before rescaning to delta.
			else: __cash__.occ_data [1] += delta;
	# Error message.
	else: self._output ("Missing a camera.", self.Message.ERROR);

"""
	@Description: Determinates whether a value is inside of the given range.
	@Parameters:
		float value: Contains a value.
		float minimum: Contains the minimum value.
		float maximum: Contains the maximum value.
"""
static func is_range (value: float, minimum: float, maximum: float) -> bool: return value >= minimum && value <= maximum;

"""
	@Description: Apply a stardard effect provided by the framework.
	@Parameters:
		String | NodePath pvt: Contains the privot of effect.
		int eft: What is the effect that will be applyed ?
		Node obj: Which knot will be considered to make the differents operations ?
		bool geo: Do you want to affect geometry override material property ?
		bool next: Do you want to affect "next_pass" property to add more effect to the same object ?
		int idx: Which materal affected ? (Only whether "geo" parameter is set to false).
		float delay: What is the timeout before apply the target effect ?
"""
static func apply_std_effect (pvt: NodePath, eft, obj: Node, geo: bool = true, next: bool = false, idx: int = 0, delay: float = 0.0, _nxt = null) -> void:
	# The game is running.
	if !Engine.editor_hint:
		# If the game is not initialised.
		if delay == 0.0 && !is_game_initialised (): yield (obj.get_tree (), "idle_frame"); elif delay > 0.0: yield (obj.get_tree ().create_timer (delay), "timeout");
	# Gets the real privot node instance.
	var privot = obj.get_node_or_null (pvt);
	# Checks privot value.
	if privot is Node2D || privot is GeometryInstance || privot is Control:
		# Checks privot type.
		if privot is Node2D && is_range (eft, 44, 72): _output ("Can't assign 3D effect to a Node2D type.", Message.ERROR);
		elif privot is Control && is_range (eft, 44, 72):  _output ("Can't assign 3D effect to a Control type.", Message.ERROR);
		elif privot is GeometryInstance && is_range (eft, 1, 43): _output ("Can't assign 2D effect to a GeometryInstance type.", Message.ERROR);
		elif privot is MeshInstance && !index_validation (idx, privot.get_surface_material_count ()): _output ("Material index is out of range.", Message.ERROR);
		# No errors detected.
		else:
			# Contains an instance of an effect.
			var effect = null; var root_folders: String = "res://nodes/general/standard_effects/";
			# Checks given effect that will be loaded.
			if eft <= StandardEffect.NONE:
				if privot is Node2D or privot is Control: privot.material = null; elif privot is MeshInstance && !geo: privot.set_surface_material (idx, null);
				else: privot.material_override = null; return;
			elif eft == StandardEffect.BAKED_SPRITE_GLOW2D: effect = load (root_folders + "baked_sprite_glow2d.shader");
			elif eft == StandardEffect.BILINEAR_FILTERING2D: effect = load (root_folders + "bilinear_filtering2d.shader");
			elif eft == StandardEffect.BILLBOARD2D: effect = load (root_folders + "billboard2d.shader");
			elif eft == StandardEffect.BLUR2D: effect = load (root_folders + "blur2d.shader");
			elif eft == StandardEffect.CHROMATIC_ABERATION2D: effect = load (root_folders + "chromatic_aberation2d.shader");
			elif eft == StandardEffect.CLOUD2D: effect = load (root_folders + "cloud2d.shader");
			elif eft == StandardEffect.COMPOSE2D: effect = load (root_folders + "compose2d.shader");
			elif eft == StandardEffect.CROSSHAIR2D: effect = load (root_folders + "crosshair2d.shader");
			elif eft == StandardEffect.DISSOLVE2D: effect = load (root_folders + "dissolve2d.shader");
			elif eft == StandardEffect.DISTORTION2D: effect = load (root_folders + "distortion2d.shader");
			elif eft == StandardEffect.FADE2D: effect = load (root_folders + "fade2d.shader");
			elif eft == StandardEffect.FLAT_OUTLINE2D: effect = load (root_folders + "flat_outline2d.shader");
			elif eft == StandardEffect.GAUSSIAN_BLUR2D: effect = load (root_folders + "gaussian_blur2d.shader");
			elif eft == StandardEffect.GAUSSIAN_BLUR2D_OPTIMIZED: effect = load (root_folders + "gaussian_blur_optimized2d.shader");
			elif eft == StandardEffect.GRADIENT2D: effect = load (root_folders + "gradient2d.shader");
			elif eft == StandardEffect.GRADIENT_SHIFT2D: effect = load (root_folders + "gradient_shift2d.shader");
			elif eft == StandardEffect.GRAYSCALE2D: effect = load (root_folders + "grayscale2d.shader");
			elif eft == StandardEffect.INLINE2D: effect = load (root_folders + "inline2d.shader");
			elif eft == StandardEffect.INOUTLINE2D: effect = load (root_folders + "inoutline2d.shader");
			elif eft == StandardEffect.INVERT2D: effect = load (root_folders + "invert2d.shader");
			elif eft == StandardEffect.NEGATIVE2D: effect = load (root_folders + "negative2d.shader");
			elif eft == StandardEffect.OUTLINE2D: effect = load (root_folders + "outline2d.shader");
			elif eft == StandardEffect.PIXELIZE2D: effect = load (root_folders + "pixelize2d.shader");
			elif eft == StandardEffect.PIXEL_OUTLINE2D: effect = load (root_folders + "pixel_outline2d.shader");
			elif eft == StandardEffect.POINTILISM2D: effect = load (root_folders + "pointilism2d.shader");
			elif eft == StandardEffect.PSYCHADELIC2D: effect = load (root_folders + "psychadelic2d.shader");
			elif eft == StandardEffect.REFLECTION2D: effect = load (root_folders + "reflection2d.shader");
			elif eft == StandardEffect.SEPIA2D: effect = load (root_folders + "sepia2d.shader");
			elif eft == StandardEffect.SHADOW2D: effect = load (root_folders + "shadow2d.shader");
			elif eft == StandardEffect.SHINY2D: effect = load (root_folders + "shiny2d.shader");
			elif eft == StandardEffect.SHOCKWAVE2D: effect = load (root_folders + "shockwave2d.shader");
			elif eft == StandardEffect.SIMPLE_DISSOLVE2D: effect = load (root_folders + "simple_dissolve2d.shader");
			elif eft == StandardEffect.SOBEL_EDGE2D: effect = load (root_folders + "sobel_edge2d.shader");
			elif eft == StandardEffect.STACKED2D: effect = load (root_folders + "stacked2d.shader");
			elif eft == StandardEffect.SWIRL2D: effect = load (root_folders + "swirl2d.shader");
			elif eft == StandardEffect.VIGNETTE2D: effect = load (root_folders + "vignette2d.shader");
			elif eft == StandardEffect.WATER2D: effect = load (root_folders + "water2d.shader");
			elif eft == StandardEffect.XRAY_MASK2D: effect = load (root_folders + "xray_mask2d.shader");
			elif eft == StandardEffect.ZOOM_BLUR2D: effect = load (root_folders + "zoom_blur2d.shader");
			elif eft == StandardEffect.XBRZ2D: effect = load (root_folders + "xBRZ2d.shader");
			elif eft == StandardEffect.SIMPLE_FIRE2D: effect = load (root_folders + "simple_fire2d.shader");
			elif eft == StandardEffect.OMNISCALE2D: effect = load (root_folders + "omniscale2d.shader");
			elif eft == StandardEffect.EASY_BLEND2D: effect = load (root_folders + "easy_blend2d.shader");
			elif eft == StandardEffect.AVANCED_TOON3D: effect = load (root_folders + "advanced_toon3d.shader");
			elif eft == StandardEffect.CEL3D: effect = load (root_folders + "cel3d.shader");
			elif eft == StandardEffect.CLOUD3D: effect = load (root_folders + "cloud3d.shader");
			elif eft == StandardEffect.COLOR_BLENDED3D: effect = load (root_folders + "color_blended3d.shader");
			elif eft == StandardEffect.CRISTAL3D: effect = load (root_folders + "cristal3d.shader");
			elif eft == StandardEffect.CURVATURE3D: effect = load (root_folders + "curvature3d.shader");
			elif eft == StandardEffect.DISSOLVE3D: effect = load (root_folders + "dissolve3d.shader");
			elif eft == StandardEffect.FLEXIBLE_TOON3D: effect = load (root_folders + "flexible_toon3d.shader");
			elif eft == StandardEffect.FORCE_FIELD3D: effect = load (root_folders + "force_field3d.shader");
			elif eft == StandardEffect.GRADIENT3D: effect = load (root_folders + "gradient3d.shader");
			elif eft == StandardEffect.MOSAIC3D: effect = load (root_folders + "mosaic3d.shader");
			elif eft == StandardEffect.OUTLINE3D: effect = load (root_folders + "outline3d.shader");
			elif eft == StandardEffect.OVERDRAW3D: effect = load (root_folders + "overdraw3d.shader");
			elif eft == StandardEffect.PLANET_ATMOSPHERE3D: effect = load (root_folders + "planet_atmosphere3d.shader");
			elif eft == StandardEffect.PSX_LIT3D: effect = load (root_folders + "psx_lit3d.shader");
			elif eft == StandardEffect.PULSE_GLOW3D: effect = load (root_folders + "pulse_glow3d.shader");
			elif eft == StandardEffect.REFRACTION3D: effect = load (root_folders + "refraction3d.shader");
			elif eft == StandardEffect.RIM3D: effect = load (root_folders + "rim3d.shader");
			elif eft == StandardEffect.SHOCKWAVE3D: effect = load (root_folders + "shockwave3d.shader");
			elif eft == StandardEffect.SIMPLE_FORCE_FIELD3D: effect = load (root_folders + "simple_force_field3d.shader");
			elif eft == StandardEffect.STYLIZED_LIQUID: effect = load (root_folders + "stylized_liquid3d.shader");
			elif eft == StandardEffect.WIND3D: effect = load (root_folders + "wind3d.shader");
			elif eft == StandardEffect.XRAY_GLOW3D: effect = load (root_folders + "xray_glow3d.shader");
			elif eft == StandardEffect.DEBANDING_MATERIAL3D: effect = load (root_folders + "debanding_material3d.shader");
			elif eft == StandardEffect.ADVANCED_DECAL3D: effect = load (root_folders + "advanced_decal3d.shader");
			elif eft == StandardEffect.SIMPLE_DECAL3D: effect = load (root_folders + "simple_decal3d.shader");
			elif eft == StandardEffect.SIMPLE_FIRE3D: effect = load (root_folders + "simple_fire3d.shader");
			elif eft == StandardEffect.LIGHT_RAYS3D: effect = load (root_folders + "light_rays3d.shader");
			else: effect = load (root_folders + "lens_flare3d.shader");
			# Updates object shader.
			var shader: ShaderMaterial = ShaderMaterial.new (); shader.shader = effect;
			# For dissolve 2d effect.
			if eft == StandardEffect.DISSOLVE2D: shader.set_shader_param ("noise_texture", load (root_folders + "fade_mask.png"));
			# For fade 2d effect.
			elif eft == StandardEffect.FADE2D: shader.set_shader_param ("fade_mask", load (root_folders + "fade_mask_soft.png"));
			# For simple dissolve 2d effect.
			elif eft == StandardEffect.SIMPLE_DISSOLVE2D: shader.set_shader_param ("dissolve_texture", load (root_folders + "fade_mask_soft.png"));
			# For dissolve 3d effect.
			elif eft == StandardEffect.DISSOLVE3D: shader.set_shader_param ("dissolve_texture", load (root_folders + "fade_mask_low_resolution.png"));
			# For force field 3d effect.
			elif eft == StandardEffect.FORCE_FIELD3D: shader.set_shader_param ("pattern_texture", load (root_folders + "hexagon_grid.png"));
			# For simple force field 3d effect.
			elif eft == StandardEffect.SIMPLE_FORCE_FIELD3D: shader.set_shader_param ("hex_tex", load (root_folders + "hexes.png"));
			# For lens flare 3d effect.
			elif eft == StandardEffect.LENS_FLARE3D: shader.set_shader_param ("lens_dirt", load (root_folders + "lens_dirt_default.jpeg"));
			# For next pass effect.
			if next:
				# Updates nxt ref value.
				_nxt = ((privot.material if privot is Node2D || privot is Control else (privot.get_surface_material (idx)\
					if privot is MeshInstance and !geo else privot.material_override)) if _nxt == null else _nxt);
				# Checks material value of the given object.
				if _nxt == null:
					# For a control or node2d or mesh instance.
					if privot is MeshInstance and !geo: privot.set_surface_material (idx, shader); elif privot is Node2D or privot is Control: privot.material = shader;
					# For geometry instance.
					else: privot.material_override = shader;
				# Otherwise.
				else:
					# The next of the given material is null.
					if _nxt.next_pass == null: _nxt.next_pass = shader; else: apply_std_effect (pvt, eft, obj, geo, next, idx, -1.0, _nxt.next_pass);
			# Otherwise.
			else:
				# For a control or node2d or mesh instance.
				if privot is MeshInstance and !geo: privot.set_surface_material (idx, shader); elif privot is Node2D or privot is Control: privot.material = shader;
				# For geometry instance.
				else: privot.material_override = shader;
	# Error message.
	else: _output ("The object type instance isn't supported on this method.", Message.ERROR);

"""
	@Description: Returns the bone transform from a target position. This method uses look at function to follow the given object.
	@Parameters:
		String | NodePath skn: Contains a skeleton node.
		String bme: What is the bone id to targeted.
		String | NodePath tar: What is the target of the given bone (Only on Spatial node).
		Node obj: Which knot will be considered to make the differents operations ?
		int dir: Which axis the look at will use ? The possibles values are:
			-> MegaAssets.Axis.X or 1: Use x axis.
			-> MegaAssets.Axis.Y or 2: Use y axis.
			-> MegaAssets.Axis.Z or 3: Use z axis.
			-> MegaAssets.Axis._X or 4: Use opposite of x axis.
			-> MegaAssets.Axis._Y or 5: Use opposite of y axis.
			-> MegaAssets.Axis._Z or 6: Use opposite of z axis.
		Vector3 oft: Contains the rotation offset of the target bone.
		int fze: Which axis will be froozen ? The possibles values are:
			-> MegaAssets.Axis.NONE or 0: No axis will be froozen.
			-> MegaAssets.Axis.X or 1: freeze x axis.
			-> MegaAssets.Axis.Y or 2: freeze y axis.
			-> MegaAssets.Axis.Z or 3: freeze z axis.
			-> MegaAssets.Axis._X or 4: freeze opposite of x axis.
			-> MegaAssets.Axis._Y or 5: freeze opposite of y axis.
			-> MegaAssets.Axis._Z or 6: freeze opposite of z axis.
			-> MegaAssets.Axis.XY or 7: freeze xy axis.
			-> MegaAssets.Axis.XZ or 8: freeze xz axis.
			-> MegaAssets.Axis.YZ or 9: freeze yz axis.
			-> MegaAssets.Axis._XY or 10: freeze opposite of xy axis.
			-> MegaAssets.Axis._XZ or 11: freeze opposite of xz axis.
			-> MegaAssets.Axis._YZ or 12: freeze opposite of yz axis.
			-> MegaAssets.Axis.XYZ or 13: freeze xyz axis.
			-> MegaAssets.Axis._XYZ or 14: freeze opposite of xyz axis.
"""
static func get_ik_look_at (skn: NodePath, bme: String, tar: NodePath, obj: Node, dir: int = 2, oft: Vector3 = Vector3.ZERO, fze: int = 0) -> Transform:
	# Gets the real skeleton and target nodes.
	var sktn = obj.get_node_or_null (skn); var targ = obj.get_node_or_null (tar);
	# Checks skeleton value.
	if sktn is Skeleton:
		# Gets the bone index.
		var bone_index: int = sktn.find_bone (bme);
		# If no bone is found.
		if bone_index == -1: _output (("The given bone name isn't defined on {" + sktn.name + "}."), Message.ERROR);
		# The bone is defined.
		else:
			# Checks target value.
			if targ is Spatial:
				# Gets the bone's global transform pose and converts our position relative to the skeleton's transform.
				var rest: Transform = sktn.get_bone_global_pose (bone_index); var target_pos = sktn.global_transform.xform_inv (targ.global_transform.origin);
				# Calls helper's "look_at" function with the chosen up axis.
				if dir <= Axis.X: rest = rest.looking_at (target_pos, Vector3.RIGHT); elif dir == Axis.Y: rest = rest.looking_at (target_pos, Vector3.UP);
				elif dir == Axis.Z: rest = rest.looking_at (target_pos, Vector3.FORWARD); elif dir == Axis._X: rest = rest.looking_at (target_pos, -Vector3.RIGHT);
				elif dir == Axis._Y: rest = rest.looking_at (target_pos, -Vector3.UP); else: rest = rest.looking_at (target_pos, -Vector3.FORWARD);
				# Gets the rotation euler of the bone and of this node.
				var rest_euler: Vector3 = rest.basis.get_euler (); var tar_euler: Vector3 = targ.global_transform.basis.orthonormalized ().get_euler ();
				# Flips the rotation euler if using negative rotation.
				if fze == Axis._X || fze == Axis._Y || fze == Axis._Z || fze == Axis._XY || fze == Axis._XZ || fze == Axis._YZ || fze == Axis._XYZ: tar_euler = -tar_euler;
				# Apply this node's rotation euler on each axis, with negative rotation whether possible and makes a new basis with the, potentially, changed euler angles.
				if fze == Axis.X: rest_euler = Vector3 (tar_euler.x, rest_euler.y, rest_euler.z);
				elif fze == Axis.Y: rest_euler = Vector3 (rest_euler.x, tar_euler.y, rest_euler.z);
				elif fze == Axis.Z: rest_euler = Vector3 (rest_euler.x, rest_euler.y, tar_euler.z);
				elif fze == Axis.XY: rest_euler = Vector3 (tar_euler.x, tar_euler.y, rest_euler.z);
				elif fze == Axis.XZ: rest_euler = Vector3 (tar_euler.x, rest_euler.y, tar_euler.z);
				elif fze == Axis.YZ: rest_euler = Vector3 (rest_euler.x, tar_euler.y, tar_euler.z);
				elif fze >= Axis.XYZ: rest_euler = Vector3 (tar_euler.x, tar_euler.y, tar_euler.z); rest.basis = Basis (rest_euler);
				# Apply rotation offset to the bone.
				if oft != Vector3.ZERO:
					# Makes the rotation with the given rotation offset.
					rest.basis = rest.basis.rotated (rest.basis.x, deg2rad (oft.x)); rest.basis = rest.basis.rotated (rest.basis.y, deg2rad (oft.y));
					rest.basis = rest.basis.rotated (rest.basis.z, deg2rad (oft.z));
				# Returns the final transform value.
				return rest;
			# Error message.
			else: _output ("Missing target.", Message.ERROR);
	# Error message.
	else: _output ("Missing skeleton node.", Message.ERROR); return Transform ();

"""
	@Description: Returns value of an index or key from an array or a dictionary.
	@Parameters:
		Variant id: Contains an id.
		Dictionary | Array | String input: Contains a dictionary or an array.
		int index: Which value of the given id would you get ?
		int count: Contains the result count. A negative value will return all found results.
		bool rev: Do you want to inverse treatment ?
		bool rec: Do you want to use a recursive program to get an id value ?
"""
static func get_id_value_of (id, input, index: int = -1, count: int = -1, rev: bool = false, rec: bool = true):
	# The given is not equal to zero.
	if count != 0:
		# Contains the input state type.
		var ste: Array = Array ([input is Dictionary, is_array (input)]);
		# The given input is a true array with an index.
		if input is String:
			# The count is less than zero.
			id = index if index >= 0 and not id is int else id;
			# Checks id value type.
			if id is int:
				# The count is less than zero.
				if count < 0: return input [id];
				# The given index is valid.
				elif index_validation (id, input.length ()):
					# Contains the filtered values and getting all values from the given interval.
					var filter: String = String (''); for u in range (id, (len (input) if count > ((len (input) - 1) - id) else (id + count)), 1): filter += input [u];
					# Checks direction.
					return filter if !rev else array_invert (filter);
			# Otherwise.
			else: return null;
		# An index has been specified.
		elif index >= 0:
			# Getting all values of the given id and converting result into an array.
			var result = get_id_value_of (id, input, -1, -1, rev, rec); result = Array ([result]) if not result is Array else result;
			# The count is less than zero.
			if count < 0: return result [index] if index_validation (index, result.size ()) else null;
			# The given index is valid.
			elif index_validation (index, result.size ()):
				# Contains the filtered values.
				var filter: Array = Array ([]);
				# Getting all values from the given interval.
				for g in range (index, (len (result) if count > ((len (result) - 1) - index) else (index + count)), 1): filter.append (result [g]);
				# Gets filtered size and returns the final result.
				var length: int = len (filter); if length <= 0: return null; elif length == 1: return filter [0]; else: return filter;
		# The given input is a dictionary.
		elif ste [0] || ste [1] && typeof (id) != TYPE_INT:
			# Contains all found values from the given input.
			var values: Array = Array ([]); id = id.lstrip (' ').rstrip (' ') if id is String else id;
			# Searches the given key in other nested dictionary.
			for item in input:
				# It found the given key.
				if ste [0] && typeof (item) == typeof (id) && item == id:
					# Checks count value.
					if count < 0 || count > 0 && len (values) < count: values.append (input [item]); else: break;
				# The value of the current key is a nested array or dictionary.
				elif rec && ste [0] && input [item] is Array || rec && ste [0] && input [item] is Dictionary\
				|| rec && ste [1] && item is Dictionary || rec && ste [1] && item is Array:
					# Contains all found values from the given array.
					var value = get_id_value_of (id, input [item] if ste [0] else item, -1, count, false, true);
					# The value isn't null.
					if value != null:
						# For an array type.
						if value is Array:
							# Getting all found keys values.
							for elmt in value:
								# Getting the found value.
								if count < 0 || count > 0 && len (values) < count: values.append (elmt); else: break;
						# For other type.
						elif count < 0 || count > 0 && len (values) < count: values.append (value); else: break;
			# Checks direction.
			values = values if !rev else array_invert (values);
			# Gets values size and returns the final result.
			var size: int = len (values); if size <= 0: return null; elif size == 1: return values [0]; else: return values;
		# The given input value is other data type.
		else: return input;
	# Otherwise.
	else: return null;

"""
	@Description: Returns a string format of the given input.
	@Parameters:
		Variant input: Contains an input.
"""
static func any_to_str (input) -> String:
	# For an array.
	if is_array (input):
		# Converting the given array to string.
		var string: String = String (''); for item in input: string += str (item); return string;
	# For a string.
	elif input is String: return input; else: return str (input);

"""
	@Description: Returns the inversed form of an array.
	@Parameters:
		Array | String array: Contains an array. This method accepts a string value.
"""
static func array_invert (array):
	# The given array is a true array.
	if is_array (array):
		# Returns the inverted array form.
		var inv: Array = Array ([]); for elmt in range ((array.size () - 1), -1, -1): inv.append (array [elmt]); return inv;
	# For a string.
	elif array is String:
		# Returns an inverted string.
		var string: String = String (''); for idx in range ((array.length () - 1), -1, -1): string += array [idx]; return string;
	# Returns the same value.
	else: return array;

"""
	@Description: What are the values ​​whose number of occurrences is immediately greater than the imposed limit ?
	@Parameters:
		Variant id: Contains an id.
		Variant input: Contains a dictionary or an array.
		int limit: What is the criterion for repeating values ?
"""
static func get_clones_of (id, input, limit: int = 1):
	# Corrects the given limit value and getting found values.
	limit = 1 if limit < 1 else limit; var found: Array = Array ([]); var values = get_id_value_of (id, input, -1);
	# The given values is an array.
	if values is Array:
		# Checks clones.
		for val in values: if values.count (val) > limit && !found.has (val): found.append (val);
	# Returns the final result.
	var sz: int = len (found); if sz <= 0: return null; elif sz == 1: return found [0]; else: return found;

"""
	@Description: Sets value of a given id on a dictionary or an array.
	@Parameters:
		Variant id: Contains an id.
		Variant value: Contains the new id value.
		Variant input: Contains a dictionary or an array.
		int index: which id reference would you want to change value ?
		bool typed: Do you want to check the type of the given id value before any change ?
		bool rec: Do you want to use a recursive program to set an id value ?
"""
static func set_id_value_of (id, value, input, index: int = -1, typed: bool = false, rec: bool = true): return _set_id_val (id, value, input, index, typed, rec, -1) [0];

"""
	@Description: Returns all possibles screen resolutions about desktop computers.
	@Parameters:
		bool to_string: Do you want to get a string format ?
"""
static func get_desktop_resolutions (to_string: bool = true):
	# String format is requested.
	if to_string: return PoolStringArray (["nHD_16-9 (640x360)", "SVGA_4-3 (800x600)", "XGA_4-3 (1024x768)", "WXGA_16-9 (1280x720)", "WXGA_16-10 (1280x800)",
		"SXGA_5-4 (1280x1024)", "HD_16-9 (1366x768)", "WXGA+_16-10 (1440x900)", "other_16-9 (1536x864)", "HD+_16-9 (1600x900)", "WSXGA+_16-10 (1680x1050)",
		"FHD_16-9 (1920x1080)", "WUXGA_16-10 (1920x1200)", "QWXGA_16-9 (2048x1152)", "2K_21-9 (2560x1080)", "QHD_16-9 (2560x1440)", "other_21-9 (3440x1440)",
		"4K_UHD_16-9 (3840x2160)", "8K_16-9 (7680x4320)"
	]);
	# Otherwise.
	else: return PoolVector2Array ([Vector2 (640, 360), Vector2 (800, 600), Vector2 (1024, 768), Vector2 (1280, 720), Vector2 (1280, 800), Vector2 (1280, 1024),
		Vector2 (1366, 768), Vector2 (1440, 900), Vector2 (1536, 864), Vector2 (1600, 900), Vector2 (1680, 1050), Vector2 (1920, 1080), Vector2 (1920, 1200),
		Vector2 (2048, 1152), Vector2 (2560, 1080), Vector2 (2560, 1440), Vector2 (3440, 1440), Vector2 (3840, 2160), Vector2 (7680, 4320)
	]);

"""
	@Description: Returns all possibles screen resolutions about ipad devices.
	@Parameters:
		bool to_string: Do you want to get a string format ?
"""
static func get_ipad_resolutions (to_string: bool = true):
	# String format is requested.
	if to_string: return PoolStringArray (["iPad1/2/Mini1_Landscape (1024x768)", "iPad3/4/Air/Air2_Landscape (2048x1536)", "iPad5th/Pro9.7_Landscape (2048x1536)",
		"iPadMini2/3/4_Landscape (2048x1536)", "iPadPro10.5_Landscape (2224x1668)", "iPadPro12.9/2017_Landscape (2732x2048)", "iPad1/2/Mini1_Portrait (768x1024)",
		"iPad3/4/Air/Air2_Portrait (1536x2048)", "iPad5th/Pro9.7_Portrait (1536x2048)", "iPadMini2/3/4_Portrait (1536x2048)", "iPadPro10.5_Portrait (1668x2224)",
		"iPadPro12.9/2017_Portrait (2048x2732)"
	]);
	# Otherwise.
	else: return PoolVector2Array ([Vector2 (1024, 768), Vector2 (2048, 1536), Vector2 (2224, 1668), Vector2 (2732, 2048), Vector2 (768, 1024), Vector2 (1536, 2048),
		Vector2 (1668, 2224), Vector2 (2048, 2732)
	]);

"""
	@Description: Returns all possibles screen resolutions about iphone devices.
	@Parameters:
		bool to_string: Do you want to get a string format ?
"""
static func get_iphone_resolutions (to_string: bool = true):
	# String format is requested.
	if to_string: return PoolStringArray (["iPhone2G/3G/3GS_Landscape (480x320)", "iPhone4/4S_Landscape (960x640)", "iPhoneXR_Landscape (1792x828)",
		"iPhone5/5S/5C/SE_Landscape (1136x640)", "iPhoneX/XS_Landscape (2436x1125)", "iPhone4/4S_Portrait (640x960)", "iPhone6/6S/7/8_Landscape (1334x750)",
		"iPhoneX/XS_Portrait (1125x2436)", "iPhoneXR_Portrait (828x1792)", "iPhone6/6S/7/8_Portrait (750x1334)", "iPhoneXSMax_Portrait (1242x2688)",
		"iPhoneXSMax_Landscape (2688x1242)", "iPhone2G/3G/3GS_Portrait (320x480)", "iPhone5/5S/5C/SE_Portrait (640x1136)", "iPhone6+/6S+/7+/8+_Portrait (1242x2208)",
		"iPhone6+/6S+/7+/8+_Landscape (2208x1242)"
	]);
	# Otherwise.
	else: return PoolVector2Array ([Vector2 (480, 320), Vector2 (960, 640), Vector2 (1792, 828), Vector2 (1136, 640), Vector2 (2436, 1125), Vector2 (640, 960),
		Vector2 (1134, 750), Vector2 (1125, 2436), Vector2 (828, 1792), Vector2 (750, 1334), Vector2 (1242, 2688), Vector2 (2688, 1242), Vector2 (320, 480),
		Vector2 (640, 1136), Vector2 (1242, 2208), Vector2 (2208, 1242)
	]);

"""
	@Description: Returns all possibles screen resolutions about android devices.
	@Parameters:
		bool to_string: Do you want to get a string format ?
"""
static func get_android_resolutions (to_string: bool = true):
	# String format is requested.
	if to_string: return PoolStringArray (["FWVGA_Portrait (480x854)", "WSVGA_Portrait (600x1024)", "WVGA_Landscape (800x480)", "HVGA_Portrait (320x480)",
		"WVGA_Portrait (480x800)", "HVGA_Landscape (480x320)", "WXGA_Portrait (800x1280)", "FWVGA_Landscape (854x480)", "WXGA_Landscape (1280x800)",
		"WSVGA_Landscape (1024x600)"
	]);
	# Otherwise.
	else: return PoolVector2Array ([Vector2 (480, 854), Vector2 (600, 1024), Vector2 (800, 480), Vector2 (320, 480), Vector2 (480, 800), Vector2 (480, 320),
		Vector2 (800, 1280), Vector2 (854, 480), Vector2 (1280, 800), Vector2 (1024, 600)
	]);

"""
	@Description: Returns initialised version of the passed input. Rid and Object types aren't supported on this method.
	@Parameters:
		Variant input: Containts a value. Pay Attention ! Your value must be in range of the base types.
"""
static func get_initialised_type (input):
	# Checking input value type.
	if typeof (input) == TYPE_NIL: return null; elif typeof (input) == TYPE_BOOL: return false;
	elif typeof (input) == TYPE_INT: return 0; elif typeof (input) == TYPE_REAL: return 0.0;
	elif typeof (input) == TYPE_STRING: return String (''); elif typeof (input) == TYPE_VECTOR2: return Vector2.ZERO;
	elif typeof (input) == TYPE_RECT2: return Rect2 (Vector2.ZERO, Vector2.ZERO);
	elif typeof (input) == TYPE_VECTOR3: return Vector3.ZERO; elif typeof (input) == TYPE_NODE_PATH: return NodePath ('');
	elif typeof (input) == TYPE_TRANSFORM2D: return Transform2D (); elif typeof (input) == TYPE_PLANE: return Plane (0.0, 0.0, 0.0, 0.0);
	elif typeof (input) == TYPE_QUAT: return Quat (0.0, 0.0, 0.0, 0.0); elif typeof (input) == TYPE_AABB: return AABB (Vector3.ZERO, Vector3.ZERO);
	elif typeof (input) == TYPE_BASIS: return Basis (Vector3.ZERO); elif typeof (input) == TYPE_TRANSFORM: return Transform (Basis (Vector3.ZERO));
	elif typeof (input) == TYPE_COLOR: return Color.black; elif typeof (input) == TYPE_ARRAY: return Array ([]);
	elif typeof (input) == TYPE_DICTIONARY: return Dictionary ({}); elif typeof (input) == TYPE_RAW_ARRAY: return PoolByteArray ([]);
	elif typeof (input) == TYPE_INT_ARRAY: return PoolIntArray ([]); elif typeof (input) == TYPE_REAL_ARRAY: return PoolRealArray ([]);
	elif typeof (input) == TYPE_STRING_ARRAY: return PoolStringArray ([]); elif typeof (input) == TYPE_VECTOR2_ARRAY: return PoolVector2Array ([]);
	elif typeof (input) == TYPE_VECTOR3_ARRAY: return PoolVector3Array ([]); elif typeof (input) == TYPE_COLOR_ARRAY: return PoolColorArray ([]); else: return null;

"""@Description: Returns Godot base types list."""
static func get_godot_base_types () -> PoolStringArray: return PoolStringArray (["Nil", "Bool", "Int", "Real", "String", "Vector 2", "Rect 2", "Vector 3",
	"Transform 2D", "Plane", "Quat", "Aabb", "Basis", "Transform", "Color", "Node Path", "Rid", "Object", "Dictionary", "Array", "Raw Array", "Int Array",
	"Real Array", "String Array", "Vector2 Array", "Vector3 Array", "Color Array"
]);

"""@Description: Returns Godot operators list."""
static func get_godot_operators () -> PoolStringArray: return PoolStringArray (["Equal", "Not Equal", "Less", "Less Equal", "Greater", "Greater Equal", "Add",
	"Substract", "Multiply", "Divide", "Negative", "Positive", "Module", "String Concat", "Shift Left", "Shift Right", "Bit And", "Bit Or", "Bit Xor", "Bit Negate",
	"And", "Or", "Xor", "Not", "In"
]);

"""
	@Description: Returns mouse controls list.
	@Parameters:
		bool keycode: Do you want to get only mouse keycodes ?
"""
func get_mouse_controls (keycode: bool = false):
	# Getting mouse controls names.
	if not keycode: return PoolStringArray (["Left Button 1", "Right Button 2", "Middle Button 3", "Scroll Up 4", "Scroll Down 5", "Scroll Left 6", "Scroll Right 7",
		"XButton1 8", "XButton2 9", "Mask Left Button 1", "Mask Right Button 2", "Mask Middle Button 4", "Mask XButton1 128", "Mask XButton2 256"
	]);
	# Getting mouse controls keycodes.
	else: return PoolIntArray ([1, 2, 3, 4, 5, 6, 7, 8, 9, 128, 256]);

"""
	@Description: Returns joystick controls list.
	@Parameters:
		bool keycode: Do you want to get only joystick keycodes ?
"""
func get_joystick_controls (keycode: bool = false):
	# Getting joystick controls names.
	if not keycode: return PoolStringArray (["Joystick Button 0", "Joystick Button 1", "Joystick Button 2", "Joystick Button 3", "Joystick Button 4", "Joystick Button 5",
		"Joystick Button 6", "Joystick Button 7", "Joystick Button 8", "Joystick Button 9", "Joystick Button 10", "Joystick Button 11", "Joystick Button 12",
		"Joystick Button 13", "Joystick Button 14", "Joystick Button 15", "Joystick Button 16", "Joystick Axis 0", "Joystick Axis 1", "Joystick Axis 2",
		"Joystick Axis 3", "Joystick Axis 6", "Joystick Axis 7"
	]);
	# Getting joystick controls keycodes.
	else: return PoolIntArray ([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16]);

"""
	@Description: Returns keyboard controls list.
	@Parameters:
		bool keycode: Do you want to get only keyboard keycodes ?
"""
func get_keyboard_controls (keycode: bool = false):
	# Contains all keyboard controls as bytes format and appending valid keyboard keycodes.
	var keys: PoolIntArray = PoolIntArray ([]); for k in range (32, 97): keys.append (k); for x in range (123, 127): keys.append (x);
	for z in range (160, 224): keys.append (z); keys.append (247); keys.append (255); keys.append (16777217); for n in range (16777218, 16777268): keys.append (n);
	for t in range (16777280, 16777320): if t != 16777301: keys.append (t); for q in range (16777346, 16777360): keys.append (q); keys.append (16777345);
	# No keycode required.
	if not keycode:
		# Containts all keyboard keycodes as string format and converting ascii keycode into string format.
		var keycodes: PoolStringArray = PoolStringArray ([]); for keycode in keys: keycodes.append (OS.get_scancode_string (keycode) + " (" + str (keycode) + ')');
		# Returns the final result.
		return keycodes;
	# Otherwise.
	else: return keys;

"""
	@Description: The given method or property name(s) is it defined ?
	@Parameters:
		String | PoolStringArray target: What is the target property or method name(s) ?
		Object object: Contains an instance of an object.
"""
static func is_undefined (target, object: Object) -> bool:
	# Converting the given target into a PoolStringArray.
	target = PoolStringArray (Array ([target]) if !is_array (target) else Array (target));
	# The passed object is not null.
	if object != null:
		# Getting property names.
		var data: Array = get_id_value_of ("name", object.get_property_list ());
		# Checks whether all targets is undefined.
		for elmt in target:
			# Corrects the current method or property name.
			elmt = str_replace (elmt, PoolStringArray (["\n", "\t", "\a", "\b", "\r", "\v", "\f", ' ', '(', ')', '[', ']', '{', '}', ',', ';', '.', '-']), '');
			# The given object has the given property or method name.
			if data.has (elmt): return false; elif get_id_value_of ("name", object.get_method_list ()).has (elmt): return false;
	# The given object has all given targets.
	return true;

"""
	@Description: Changes value of the given property with a large field.
	@Parameters:
		String pname: Contains the property name.
		Variant value: Contains the property value.
		Object object: Contains an instance of an object.
		float delay: What is the timeout before changing property value ? Note that if you give the reference of an object that is not
			the instance of a node, you will be exempt from using this parameter.
"""
static func set_var (pname: String, value, object: Object, delay: float = 0.0) -> void:
	# The given object reference is not null.
	if object != null:
		# The target property name is not empty.
		if not pname.empty ():
			# Waiting for the given delay.
			if !Engine.editor_hint && object is Node && delay > 0.0: yield (object.get_tree ().create_timer (delay), "timeout");
			# Getting script properties names and his prefix.
			var data: Array = get_id_value_of ("name", object.get_property_list ()); var prefix: String = get_object_prefix (object);
			# The given object is a MagaAssets module.
			if object.has_method ("is_dont_destroy_mode") and object.is_dont_destroy_mode () is bool:
				# The special "__props__" property is defined.
				if data.has ("__props__") && object.has_method ("set_prop") && object.get_prop (pname) != null: object.set_prop (pname, value);
				# The given property is directly defined on the script variables.
				elif pname in data: object.set (str_replace (pname, PoolStringArray (["\n", "\t", "\a", "\b", "\r", "\v", "\f"]), '').lstrip (' ').rstrip (' '), value);
				# Error message.
				else: _output (("Undefined {" + pname + "} property on {" + prefix + "} instance."), Message.ERROR);
			# The given property is directly defined on the script variables.
			elif pname in data: object.set (str_replace (pname, PoolStringArray (["\n", "\t", "\a", "\b", "\r", "\v", "\f"]), '').lstrip (' ').rstrip (' '), value);
			# Error message.
			else: _output (("Undefined {" + pname + "} property on {" + prefix + "} instance."), Message.ERROR);
		# Warning message.
		else: _output ("Missing property name.", Message.WARNING);
	# Warning message.
	else: _output ("Your object reference mustn't be null.", Message.WARNING);

"""
	@Description: Gets value of the given property with a large field.
	@Parameters:
		String pname: Contains the property name.
		Object object: Contains an instance of an object.
		bool dropdown: To return any dropdown value as string format and other value type.
"""
static func get_var (pname: String, object: Object, dropdown: bool = false):
	# The given object reference is not null.
	if object != null:
		# The target property name is not empty.
		if not pname.empty ():
			# Getting script properties names and his prefix.
			var data: Array = get_id_value_of ("name", object.get_property_list ()); var prefix: String = get_object_prefix (object);
			# The given object is a MagaAssets module.
			if object.has_method ("is_dont_destroy_mode") and object.is_dont_destroy_mode () is bool:
				# The special "__props__" property is defined.
				if data.has ("__props__") && object.has_method ("get_prop") && object.get_prop (pname) != null: return object.get_prop (pname, dropdown);
				# The given property is directly defined on the script variables.
				elif pname in data: return object.get (str_replace (pname, PoolStringArray (["\n", "\t", "\a", "\b", "\r", "\v", "\f"]), '').lstrip (' ').rstrip (' '));
				# Error message.
				else: _output (("Undefined {" + pname + "} property on {" + prefix + "} instance."), Message.ERROR);
			# The given property is directly defined on the script variables.
			elif pname in data: return object.get (str_replace (pname, PoolStringArray (["\n", "\t", "\a", "\b", "\r", "\v", "\f"]), '').lstrip (' ').rstrip (' '));
			# Error message.
			else: _output (("Undefined {" + pname + "} property on {" + prefix + "} instance."), Message.ERROR);
		# Warning message.
		else: _output ("Missing property name.", Message.WARNING);
	# Warning message.
	else: _output ("Your object reference mustn't be null.", Message.WARNING); return null;

"""
	@Description: Returns a basic object infos from his instance.
	@Parameters:
		Object object: Contains an instance of an object.
"""
static func get_object_prefix (object: Object) -> String:
	# Getting object name and returns it.
	return (object.get_class () + " [" + object.name + ']') if object is Node && !object.name.empty () else object.get_class ();

"""
	@Description: Returns usefull game statistiques.
	@Parameters:
		Node object: Which knot will be considered to make the differents operations ?
"""
static func get_game_statistiques (object: Node) -> Dictionary: return Dictionary ({
	"Fps": Engine.get_frames_per_second (), "Object Count": object.get_tree ().get_node_count (), "Frames": object.get_tree ().get_frame (),
	"Window Width": (str (OS.get_real_window_size ().x) + "px"), "Window Height": (str (OS.get_real_window_size ().y) + "px"),
	"Graphic Resolution size": (str (((4 * OS.window_size.x * OS.window_size.y) / 1024) / 1024) + "MB"),
	"Window Xposition": (str (OS.window_position.x) + "px"), "Window Yposition": (str (OS.window_position.y) + "px"),
	"Window Min Width": (str (OS.min_window_size.x) + "px"), "Window Max Width": (str (OS.max_window_size.x) + "px"),
	"Window Min Height": (str (OS.min_window_size.y) + "px"), "Window Max Height": (str (OS.max_window_size.y) + "px"),
	"FullScreen": OS.window_fullscreen, "VSync": OS.vsync_enabled, "VSync Compositor": OS.vsync_via_compositor,
	"Processor Optimization": OS.low_processor_usage_mode, "Window Borderless": OS.window_borderless, "Keep Screen": OS.keep_screen_on,
	"Window Resizable": OS.window_resizable, "Window Foreground": OS.is_window_always_on_top (), "Window Focused": OS.is_window_focused (),
	"Process ID": OS.get_process_id (), "Idle Frames": Engine.get_idle_frames (), "Frames Draw": Engine.get_frames_drawn (),
	"Pixels Count": int (OS.window_size.x * OS.window_size.y)
}) if !Engine.editor_hint else Dictionary ({});

"""@Description: Returns usefull computer statistiques."""
static func get_computer_statistiques () -> Dictionary: return Dictionary ({
	"Batery Percent": (str (OS.get_power_percent_left ()) + '%'), "Video Driver Count": OS.get_video_driver_count (), "Touch Screen": OS.has_touchscreen_ui_hint (),
	"Audio Driver Count": OS.get_audio_driver_count (), "Connected Controller(s)": get_connected_controllers_names ().join (", "),
	"Date System": (str (OS.get_date ().day) + '/' + str (OS.get_date ().month) + '/' + str (OS.get_date ().year)),
	"Time System": (str (OS.get_time ().hour) + "h:" + str (OS.get_time ().minute) + "min:" + str (OS.get_time ().second) + 's'),
	"Memory Usage": OS.get_dynamic_memory_usage (), "Connected Controller Count": get_connected_controllers_names ().size (),
	"Host OS Local": OS.get_locale (), "Model Name": OS.get_model_name (), "OS Name": OS.get_name (), "Screen Size": OS.get_screen_size (),
	"Batery Remaining Time": get_time_from (OS.get_power_seconds_left ()) if OS.get_power_seconds_left () != -1 else "Unknown",
	"Processor Count": OS.get_processor_count (), "Screen Count": OS.get_screen_count (), "Screen DPI": OS.get_screen_dpi (),
	"Screen Max Scale": OS.get_screen_max_scale (), "Static Memory Usage": OS.get_static_memory_usage (),
	"Tablet Driver Count": OS.get_tablet_driver_count (), "Thread Caller ID": OS.get_thread_caller_id (),
	"Time Zone": (str (OS.get_time_zone_info ().bias) + '/' + OS.get_time_zone_info ().name), "Unique ID": OS.get_unique_id ()
}) if !Engine.editor_hint else Dictionary ({});

"""
	@Description: Displays in real time the different data contained in the parameter data. Calls this method on game runtime only.
	@Parameters:
		bool visible: Should we display the data from the debugging ?
		Node object: Which knot will be considered to make the differents operations ?
		Dictionary data: Contains the various data to displayed for debugging.
		Dictionary options: Contains the different configurations for managing the display of data. This dictionary supports the keys following:
			-> bool vspace = false: Do you want add a vertical space on all materialized data ?
			-> int mrgtop = 0: Add a margin in top of the represented data set.
			-> int mrgleft = 0: Add a margin to left of all the data represented.
			-> int mrgright = 0: Add a margin to right of all the data represented.
			-> int mrgbottom = 0: Add a margin in bottom of the set of data represented.
			-> int charcount = -1: characters do you want displayed on all displayed data ?
			-> bool uppercase = false: Would you like capitalize all the represented data ?
			-> Color color = Color.white: Controls the color of all data materialized by debugging.
		Note that you have the possibility of customizing the properties of each data represented. All you have to do is specify the name of the
		key corresponding to the data to be personalized and from it assigned a dictionary supporting the keys: mrgtop, mrgbottom, mrgleft, mrgright,
		color, charcount and uppercase.
		String | Array | PoolStringArray filter: Contains the key (s) of the elements that will be represented during debugging.
		float delay: What is the dead time before materialization data ?
"""
static func debug_data (visible: bool, object: Node, data: Dictionary = Dictionary ({}), options: Dictionary = Dictionary ({}), filter = null, delay: float = 0.0) -> void:
	# The game is running.
	if not Engine.editor_hint:
		# If the game is not initialised.
		if delay == 0.0 && not is_game_initialised (): yield (object.get_tree (), "idle_frame");
		# Otherwise.
		elif delay > 0.0: yield (object.get_tree ().create_timer (delay), "timeout");
		# Getting available game control debug node.
		var root: Viewport = object.get_viewport (); var gme_ctrl_dbg = root.get_node_or_null ("game_debug");
		# Getting the vbox container from the debug control.
		var container = gme_ctrl_dbg.get_node_or_null ("container") if gme_ctrl_dbg is Control else null;
		# Checks visible value.
		if visible:
			# Converting the filter parameter into an PoolStringArray.
			filter = Array ([filter]) if not is_array (filter) else Array (filter); filter = Array (PoolStringArray (filter));
			# Corrects filter value.
			filter = Array ([]) if filter.size () == 1 and filter [0] == "Null" else filter;
			# The game control debug node exists.
			if not gme_ctrl_dbg is Control:
				# Creating a new control node named "game_debug".
				var game_state_contr: Control = Control.new (); game_state_contr.name = "game_debug";
				# Creating a VBoxContainer node named "container".
				var vcontainer: VBoxContainer = VBoxContainer.new (); vcontainer.name = "container";
				# Adding the vbox container to this node as a child and add the final result to root node.
				game_state_contr.add_child (vcontainer); root.add_child (game_state_contr);
				# Updating base nodes.
				gme_ctrl_dbg = root.get_node_or_null ("game_debug"); container = gme_ctrl_dbg.get_node_or_null ("container");
			# Configures VBoxContainer.
			container.alignment = int (options.align) if options.has ("align") && is_number (options.align) else container.alignment;
			container.margin_top = int (options.mrgtop) if options.has ("mrgtop") && is_number (options.mrgtop) else container.margin_top;
			container.margin_right = int (options.mrgright) if options.has ("mrgright") && is_number (options.mrgright) else container.margin_right;
			container.margin_bottom = int (options.mrgbottom) if options.has ("mrgbottom") && is_number (options.mrgbottom) else container.margin_bottom;
			container.margin_left = int (options.mrgleft) if options.has ("mrgleft") && is_number (options.mrgleft) else container.margin_left;
			# Shows all data.
			for item in data:
				# Converting the current item as string format.
				var n = (item as String).lstrip (' ').rstrip (' '); var v: String = str_replace (n.replace (' ', '_'), PoolStringArray (['.', ':', '@', '/', '"']), '');
				# Getting the current label node.
				var lb: Label = container.get_node_or_null (v);
				# Should us display it ?
				if n in filter || filter.empty ():
					# The current label is not defined.
					if not lb is Label:
						# Contains a label instance and updates the base pointor.
						var label: Label = Label.new (); label.name = v; container.add_child (label); lb = container.get_node_or_null (v);
					# Configures label with global keys.
					lb.text = (n + ": " + str (data [item]) + ("\n" if options.has ("vspace") && options.vspace is bool && options.vspace else ''));
					lb.modulate = options.color if options.has ("color") && options.color is Color else lb.modulate;
					lb.visible_characters = int (options.charcount) if options.has ("charcount") && is_number (options.charcount) else -1;
					lb.mouse_filter = Control.MOUSE_FILTER_IGNORE; lb.mouse_default_cursor_shape = Control.CURSOR_ARROW;
					lb.uppercase = options.uppercase if options.has ("uppercase") && options.uppercase is bool else lb.uppercase;
					lb.align = Label.ALIGN_LEFT; lb.valign = Label.VALIGN_TOP; lb.percent_visible = 1;
					# There are a specific property(ies) about the current label.
					if options.has (n):
						# Re-configures label custom values.
						var mrgleft: int = int (options [n].mrgleft) if options [n].has ("mrgleft") && is_number (options [n].mrgleft) else 0;
						var mrgright: int = int (options [n].mrgright) if options [n].has ("mrgright") && is_number (options [n].mrgright) else 0;
						var mrgtop: int = int (options [n].mrgtop) if options [n].has ("mrgtop") && is_number (options [n].mrgtop) else 0;
						var mrgbottom: int = int (options [n].mrgbottom) if options [n].has ("mrgbottom") && is_number (options [n].mrgbottom) else 0;
						for _j in range (1, mrgleft): lb.text = (' ' + lb.text); for _n in range (1, mrgright): lb.text = (lb.text + ' ');
						for _i in range (1, mrgtop): lb.text = ("\n" + lb.text); for _p in range (1, mrgbottom): lb.text = (lb.text + "\n");
						lb.visible_characters = int (options [n].charcount) if options [n].has ("charcount") && is_number (options [n].charcount) else lb.visible_characters;
						lb.uppercase = options [n].uppercase if options [n].has ("uppercase") && options [n].uppercase is bool else lb.uppercase;
						lb.modulate = options [n].color if options [n].has ("color") && options [n].color is Color else lb.modulate;
				# Otherwise.
				elif lb is Label: lb.queue_free ();
		# Destroys debug control.
		elif gme_ctrl_dbg is Control: gme_ctrl_dbg.queue_free ();
	# Warning message.
	else: _output ("This method must be called on game runtime only.", Message.WARNING);

"""
	@Description: Changes game globals configurations. Calls this method on game runtime only.
	@Parameters:
		Dictionary settings: Contains differents options for managing game globals configurations. This dictionary supports the keys following:
			-> int orientation = 0: The screen orientation.
			-> bool broderless = false: Game window borderles.
			-> bool fullscreen = false: Game fullscreen effect.
			-> bool resizable = true: Game window is it resizable ?
			-> Vector2 resolution = Vector2 (1024, 600): Game window size control.
			-> bool foreground = false: Game window should it always on top ?
			-> Vector2 minsize = Vector2 (0, 0): What is the minimum resolution of the game window ? If only one of the values ​​of this vector is negative or zero,
				we will consider that the minimum value of the latter is zero. Don't give a resolution greater than that of the game window.
			-> Vector2 position = Vector2 (-1, -1): What is the position of the window of the Game ? If only one of the values ​​of this vector is negative, the latter
				will be automatically centered on the screen at the value not respecting the conditions of a resolution when the game is run for the first time.
			-> Vector2 maxsize = Vector2 (0, 0): What is the maximum resolution of the game window ? If would be only one of the values ​​of this vector is less than
				the current resolution of the game window, we will consider that the maximum size is equal to the current one within the game window. Do not give
				a resolution higher than this that can support the screen of your machine.
			-> int quality = MegaAssets.GameQuality.LOW: The game quality. The possibles values are:
				MegaAssets.GameGrade.LOW or 0: Low game quality.
				MegaAssets.GameGrade.MEDIUM or 1: Medium game quality.
				MegaAssets.GameGrade.HIGH or 2: High game quality.
			-> float volume: Global game sound volume.
			-> int contrast = 13: The game screen contrast (percentage unit).
			-> int brightness = 13: The game screen brightness (percentage unit).
			-> int saturation = 13: The game screen saturation (percentage unit).
			-> bool vsync = true: See godot documentation about vsync property on OS class.
			-> bool optimization = false: Game proccessor usage optimization.
			-> bool vsyncompositor = false: See godot documentation about vsync_compositor property on OS class.
			-> bool keepscreen = true: Do you want keep computer screen on while the game is playing ?
		Node object: Which knot will be considered to make the differents operations ?
		String | Array | PoolStringArray | NodePath nodes: Contains the different node (s) to targeted for the application of the value of certain
			dictionary keys provided. This parameter supports nodes of type Camara and WorldEnvironment to update the value of properties staturation, quality,
			contrast and brightness; AudioStreamPlayer, AudioStreamPlayer2D and AudioStreamPlayer3D to update the value of properties "volume_db" and "max_db".
		float delay: What is the dead time before the application of configurations ?
"""
static func apply_game_settings (settings: Dictionary, object: Node, nodes = null, delay: float = 0.0) -> void:
	# The game is running.
	if not Engine.editor_hint:
		# Waiting for the given delay and converting the nodes parameter into a PoolStringArray.
		if delay > 0: yield (object.get_tree ().create_timer (delay), "timeout"); nodes = Array ([nodes]) if !nodes is Array && !nodes is PoolStringArray else nodes;
		# Game window configurations.
		OS.screen_orientation = int (settings.orientation) if settings.has ("orientation") && is_number (settings.orientation) else OS.screen_orientation;
		OS.window_borderless = settings.borderless if settings.has ("borderless") && settings.borderless is bool else OS.window_borderless;
		OS.window_fullscreen = settings.fullscreen if settings.has ("fullscreen") && settings.fullscreen is bool else OS.window_fullscreen;
		OS.window_resizable = settings.resizable if settings.has ("resizable") && settings.resizable is bool else OS.window_resizable;
		var resolution: Vector2 = any_to_vector2 (settings.resolution) if settings.has ("resolution") && is_vector (settings.resolution) else OS.window_size;
		resolution.x = 0.0 if resolution.x < 0.0 else resolution.x; resolution.y = 0.0 if resolution.y < 0.0 else resolution.y; OS.window_size = resolution;
		OS.set_window_always_on_top (settings.foreground if settings.has ("foreground") && settings.foreground is bool else OS.is_window_always_on_top ());
		var min_size: Vector2 = any_to_vector2 (settings.minsize) if settings.has ("minsize") && is_vector (settings.minsize) else Vector2.ZERO;
		min_size.x = 0.0 if min_size.x < 0.0 else min_size.x; min_size.y = 0.0 if min_size.y < 0.0 else min_size.y;
		min_size.x = OS.get_real_window_size ().x if min_size.x > OS.get_real_window_size ().x else min_size.x;
		min_size.y = OS.get_real_window_size ().y if min_size.y > OS.get_real_window_size ().y else min_size.y;
		var max_size: Vector2 = any_to_vector2 (settings.maxsize) if settings.has ("maxsize") && is_vector (settings.maxsize) else OS.get_real_window_size ();
		max_size.x = OS.get_real_window_size ().x if max_size.x < OS.get_real_window_size ().x else max_size.x;
		max_size.x = OS.get_screen_size ().x if max_size.x > OS.get_screen_size ().x else max_size.x;
		max_size.y = OS.get_real_window_size ().y if max_size.y < OS.get_real_window_size ().y else max_size.y;
		max_size.y = OS.get_screen_size ().y if max_size.y > OS.get_screen_size ().y else max_size.y; OS.min_window_size = min_size; OS.max_window_size = max_size;
		OS.keep_screen_on = settings.keepscreen if settings.has ("keepscreen") && settings.keepscreen is bool else OS.keep_screen_on;
		OS.vsync_via_compositor = settings.vsyncompositor if settings.has ("vsyncompositor") && settings.vsyncompositor is bool else OS.vsync_via_compositor;
		OS.low_processor_usage_mode = settings.optimization if settings.has ("optimization") && settings.optimization is bool else OS.low_processor_usage_mode;
		OS.vsync_enabled = settings.vsync if settings.has ("vsync") && settings.vsync is bool else OS.vsync_enabled;
		if settings.has ("position") && is_vector (settings.position):
			var win_pos: Vector2 = any_to_vector2 (settings.position);
			if settings.position.x < 0: win_pos.x = ((OS.get_screen_size ().x / 2) - (OS.get_real_window_size ().x / 2));
			if settings.position.y < 0: win_pos.y = ((OS.get_screen_size ().y / 2) - (OS.get_real_window_size ().y / 2)); OS.window_position = win_pos;
		OS.window_size = Vector2 (1366, 768) if settings.has ("maximize") && settings.maximize is bool && settings.maximize else resolution;
		# External nodes configurations.
		for node in nodes:
			# Filters each item from the node list.
			node = object.get_node_or_null (node) if node is String or node is NodePath else node;
			# The current node is a Camera or WorldEnvirnment.
			if node is WorldEnvironment or node is Camera:
				# Configures a certain properties.
				var env = node.environment if node.environment != null else null;
				# The current environment variable is not null.
				if env is Environment:
					env.adjustment_enabled = true; randomize ();
					var qly = int (settings.quality) if settings.has ("quality") && is_number (settings.quality) && is_range (settings.quality, 0.0, 2.0) else -1;
					qly = (randi () % 2 + 1) if qly == 1 else (env.tonemap_mode if qly < 0 else ((qly + 1) if qly == 2 else qly)); env.tonemap_mode = qly;
					var brightness = ((settings.brightness * 8) / 100) if settings.has ("brightness") && is_number (settings.brightness) else env.adjustment_brightness;
					var contrast = ((settings.contrast * 8) / 100) if settings.has ("contrast") && is_number (settings.contrast) else env.adjustment_contrast;
					var saturation = ((settings.saturation * 8) / 100) if settings.has ("saturation") && is_number (settings.saturation) else env.adjustment_saturation;
					env.adjustment_brightness = brightness; env.adjustment_contrast = contrast; env.adjustment_saturation = saturation;
			# The current node is audio stream 2d or simple.
			elif node is AudioStreamPlayer or node is AudioStreamPlayer2D:
				# Configures "volume_db" property.
				node.volume_db = linear2db (settings.volume * db2linear (24) / 100) if settings.has ("volume") && is_number (settings.volume) else node.volume_db;
			# The current node is audio stream 3d.
			elif node is AudioStreamPlayer3D:
				# Configures "max_db" property.
				node.max_db = linear2db (settings.volume * db2linear (6) / 100) if settings.has ("volume") && is_number (settings.volume) else node.max_db;
	# Warning message.
	else: _output ("This method must be called on game runtime only.", Message.WARNING);

"""
	@Description: Returns a certain folders paths from the installed Operating System.
	@Parameters:
		int path: Contains a specified path. The possible values are:
			MegaAssets.Path.GAME_LOCATION or 0: Game location folder.
			MegaAssets.Path.OS_ROOT or 1: Operating system root folder.
			MegaAssets.Path.USER_DATA or 2: The user data folder.
			MegaAssets.Path.USER_ROOT or 3: The user root folder.
			MegaAssets.Path.USER_DESKTOP or 4: The user desktop folder.
			MegaAssets.Path.USER_PICTURES or 5: The user pictures folder.
			MegaAssets.Path.USER_MUSIC or 6: The user musics folder.
			MegaAssets.Path.USER_VIDEOS or 7: The user videos folder.
			MegaAssets.Path.USER_DOCUMENTS or 8: The user documents folder.
			MegaAssets.Path.USER_DOWNLOADS or 9: The user downloads folder.
"""
static func get_os_dir (path: int) -> String:
	# Checks the given path enum index.
	if path == Path.GAME_LOCATION: return "res://";  elif path == Path.OS_ROOT && OS.get_name () == "X11" || path == Path.OS_ROOT && OS.get_name () == "OSX": return '/';
	elif path == Path.OS_ROOT && OS.get_name () == "Windows": return "C://"; elif path == Path.USER_DATA: return OS.get_user_data_dir ();
	elif path == Path.USER_ROOT: return "user://"; elif path == Path.USER_DESKTOP: return OS.get_system_dir (OS.SYSTEM_DIR_DESKTOP);
	elif path == Path.USER_PICTURES: return OS.get_system_dir (OS.SYSTEM_DIR_PICTURES); elif path == Path.USER_MUSIC: return OS.get_system_dir (OS.SYSTEM_DIR_MUSIC);
	elif path == Path.USER_VIDEOS: return OS.get_system_dir (OS.SYSTEM_DIR_MOVIES); elif path == Path.USER_DOCUMENTS: return OS.get_system_dir (OS.SYSTEM_DIR_DOCUMENTS);
	elif path == Path.USER_DOWNLOADS: return OS.get_system_dir (OS.SYSTEM_DIR_DOWNLOADS); else: return "Null";

"""@Description: Watches for window size changes and handles, game screen scaling with exact integer and multiples of a base resolution in mind."""
func pixels_adjusment () -> void:
	# Contains some usefull paths.
	var paths = ["display/window/integer_resolution_handler/base_width", "display/window/integer_resolution_handler/base_height", "display/window/stretch/shrink"];
	# Parses project settings.
	if ProjectSettings.has_setting (paths [0]): self.__cash__.base_resolution.x = ProjectSettings.get_setting (paths [0]);
	if ProjectSettings.has_setting (paths [1]): self.__cash__.base_resolution.y = ProjectSettings.get_setting (paths [1]);
	# Checks stretch mode from project settings.
	match ProjectSettings.get_setting ("display/window/stretch/mode"):
		"2d": __cash__.stretch_mode = SceneTree.STRETCH_MODE_2D; "viewport": __cash__.stretch_mode = SceneTree.STRETCH_MODE_VIEWPORT;
		_: __cash__.stretch_mode = SceneTree.STRETCH_MODE_DISABLED;
	# Checks stretch aspect from project settings.
	match ProjectSettings.get_setting ("display/window/stretch/aspect"):
		"keep": __cash__.stretch_aspect = SceneTree.STRETCH_ASPECT_KEEP; "keep_height": __cash__.stretch_aspect = SceneTree.STRETCH_ASPECT_KEEP_HEIGHT;
		"keep_width": __cash__.stretch_aspect = SceneTree.STRETCH_ASPECT_KEEP_WIDTH; "expand": __cash__.stretch_aspect = SceneTree.STRETCH_ASPECT_EXPAND;
		_: __cash__.stretch_aspect = SceneTree.STRETCH_ASPECT_IGNORE;
	# Enforces minimum resolution.
	OS.min_window_size = self.__cash__.base_resolution; var root: Viewport = self.get_viewport ();
	# Removes default stretch behavior.
	self.get_tree ().set_screen_stretch (SceneTree.STRETCH_MODE_DISABLED, SceneTree.STRETCH_ASPECT_IGNORE, self.__cash__.base_resolution, 1);
	var video_mode: Vector2 = OS.get_screen_size () if OS.window_fullscreen else OS.window_size;
	# Calculates pixels scale.
	var scale: int = int (max (floor (min ((video_mode.x / self.__cash__.base_resolution.x), (video_mode.y / self.__cash__.base_resolution.y))), 1));
	var screen_size: Vector2 = self.__cash__.base_resolution; var viewport_size: Vector2 = (screen_size * scale);
	var overscan: Vector2 = ((video_mode - viewport_size) / scale).floor (); var margin: Vector2; var margin2: Vector2;
	# Re-checks stretch aspect.
	match self.__cash__.stretch_aspect:
		SceneTree.STRETCH_ASPECT_KEEP_WIDTH: screen_size.y += overscan.y; SceneTree.STRETCH_ASPECT_KEEP_HEIGHT: screen_size.x += overscan.x;
		SceneTree.STRETCH_ASPECT_EXPAND, SceneTree.STRETCH_ASPECT_IGNORE: screen_size += overscan;
	viewport_size = (screen_size * scale); margin = ((video_mode - viewport_size) / 2); margin2 = margin.ceil (); margin = margin.floor ();
	# Re-checks stretch mode.
	match self.__cash__.stretch_mode:
		SceneTree.STRETCH_MODE_VIEWPORT:
			root.set_size ((screen_size / ProjectSettings.get_setting (paths [2])).floor ());
			root.set_attach_to_screen_rect (Rect2 (margin, viewport_size)); root.set_size_override_stretch (false); root.set_size_override (false);
		SceneTree.STRETCH_MODE_2D, _:
			root.set_size ((viewport_size / ProjectSettings.get_setting (paths [2])).floor ());
			root.set_attach_to_screen_rect (Rect2 (margin, viewport_size)); root.set_size_override_stretch (true);
			root.set_size_override (true, (screen_size / ProjectSettings.get_setting (paths [2])).floor ());
	# Checks margins value.
	margin.x = 0.0 if margin.x < 0.0 else margin.x; margin.y = 0.0 if margin.y < 0.0 else margin.y;
	margin2.x = 0.0 if margin2.x < 0.0 else margin2.x; margin2.y = 0.0 if margin2.y < 0.0 else margin2.y;
	# Updates black bars.
	VisualServer.black_bars_set_margins (int (margin.x), int (margin.y), int (margin2.x), int (margin2.y));

"""
	@Description: Draws a 3d line with the passed data.
	@Parameters:
		Dictionary data: Contains the different configurations on tracing and line behavior. The dictionary supports the keys following:
			-> NodePath | String parent: A plotted line will be the child of which parent? If the value specified is invalid or if there is no value,
				the value in the "object" parameter will be used.
			-> Variant points: Contains the coordinates of different points on which will define the trajectory that the plotted line will take.
				Note that you have the option of giving an instance of a node of type "RayCast".
			-> String id = "LineRender": Contains the line name.
			-> int mesh = Mesh.PRIMITIVE_TRIANGLES: What mesh used to draw the line? Note that the possible values ​​are those of Godot.
			-> int fze = MegaAssets.Axis.NONE: Which axis will be froozen ? The possibles values are:
				-> MegaAssets.Axis.NONE or 0: No axis will be froozen.
				-> MegaAssets.Axis.X or 1: freeze x axis.
				-> MegaAssets.Axis.Y or 2: freeze y axis.
				-> MegaAssets.Axis.Z or 3: freeze z axis.
				-> MegaAssets.Axis.XY or 7: freeze xy axis.
				-> MegaAssets.Axis.XZ or 8: freeze xz axis.
				-> MegaAssets.Axis.YZ or 9: freeze yz axis.
				-> MegaAssets.Axis.XYZ or 13: freeze xyz axis.
			-> Color | ShaderMaterial | SpatialMaterial skin = Color.White: Contains the line skin.
			-> bool visible = true: Control the line visibility.
			-> float | Vector2 | int width = 0.004: What is the line width ?
			-> float | Vector2 | int smooth = 5.0: What is the rounding of the corners and caps ?
			-> bool skinscale = false: Do us to increase the scale of enlargement of the complexion loaded currently on the line ?
			-> Dictionary | Array actions: Contains the (s) action (s) to be performed during line design. The use of this parameter is
				already described at the basics of the framework. see "run_slot" method to get more details.
			-> Dictionary oncolliding: Called when the raycast referred detects an object carrying a collider. Only use this key if you give
				at the "points" key, a raycast.
			-> NodePath | String | PoolStringArray impact: Which is/are the path(s) of the object(s) to be set to the position of the point of impact given by
				a raycast ? You also have the possibility to give path(s) pointing to of prefabricated object(s). Use this key only inside "oncolliding" key.
			-> bool destroy = true: Should us destroy the imported prefab object(s) when no object is detected ?
			Note that you have the possibility to use one of the identifiers of an object (the name, the group and the type of class belonging to the object
			detected by a raycast) to change the line behavior.
		Node object: Which knot will be considered to make the differents operations ?
		float live: What is the lifespan of the line after its creation.
		float delay: What is the dead time before line tracing ?
"""
static func draw_line_3d (data: Dictionary, object: Node, live: float = -1.0, delay: float = 0.0) -> void:
	# Live parameter value is different of zero.
	if live != 0.0:
		# The game is running.
		if !Engine.editor_hint:
			# If the game is not initialised.
			if delay == 0.0 && !is_game_initialised (): yield (object.get_tree (), "idle_frame");
			# Otherwise.
			elif delay > 0.0: yield (object.get_tree ().create_timer (delay), "timeout");
		# Checks parent key.
		data.parent = (data.parent if data.parent is String || data.parent is NodePath else object.get_path ()) if data.has ("parent") else object.get_path ();
		# Getting the real parent reference.
		data.parent = object.get_node_or_null (data.parent); data.points = data.points if data.has ("points") else null;
		# Converts the passed points into an array.
		data.points = Array ([data.points]) if !is_array (data.points) else (Array (data.points) if len (data.points) > 0 else Array ([null]));
		# Getting the given node from points array.
		var raycast = object.get_node_or_null (data.points [0]) if data.points [0] is String or data.points [0] is NodePath else data.points [0];
		# Checks whether the current point is a RayCast.
		data.parent = raycast if raycast is RayCast else data.parent;
		# The parent is it a 3d node ?
		if data.parent is Spatial:
			# Getting the passed line id.
			data.id = str_replace (data.id, ['.', ':', '@', '/', '"'], '').lstrip (' ').rstrip (' ') if data.has ("id") && data.id is String else "$LineRender";
			# Getting the existing immediate geometry.
			randomize (); data.id = (data.id + str (randi () % 100000000)) if data.id == "$LineRender" else data.id; var geometry = data.parent.get_node_or_null (data.id);
			# Is it visible ?
			if (data.visible if data.has ("visible") && data.visible is bool else true):
				# Immediate geomety node don't exists.
				if not geometry is ImmediateGeometry:
					# Creating a new geometry from the given id.
					var geo: ImmediateGeometry = ImmediateGeometry.new (); geo.name = data.id;
					# Adds this node to the given parent and update geometry variable.
					data.parent.add_child (geo); geometry = data.parent.get_node_or_null (data.id);
					# Sets geometry position to his parent position.
					geometry.transform.origin = Vector3.ZERO; _destroy_geometry_after_live (live, geometry, object);
				# A raycast has been passed.
				if data.parent is RayCast:
					# Contains the raycast points data.
					var ray_pts: PoolVector3Array = PoolVector3Array ([Vector3.ZERO, data.parent.cast_to]);
					# Initializing geometry.
					data = _run_line_common_configs (data, Dictionary ({}), geometry, ray_pts, object);
					# Is it colliding ?
					if data.parent.is_colliding ():
						# Updates the collision point of the ray.
						ray_pts [1] = geometry.to_local (data.parent.get_collision_point ()); var collider = data.parent.get_collider ();
						# Updates line geometry to raycast collision point.
						data = _run_line_common_configs (data, Dictionary ({}), geometry, ray_pts, object);
						# "oncolliding" key is it correct ?
						if data.has ("oncolliding") && data.oncolliding is Dictionary && !data.oncolliding.empty ():
							# Updating colliding configurations.
							data.oncolliding = _run_line_common_configs (data.oncolliding, data, geometry, ray_pts, object);
							# Getting from the collider a useful informations.
							var re: Array = Array ([collider.name, collider.get_class (), collider.get_groups ()]);
							# The current detected object has a valid name.
							if data.oncolliding.has (re [0]) && data.oncolliding [re [0]] is Dictionary && !data.oncolliding [re [0]].empty ():
								# Runs the given configurations.
								data.oncolliding [re [0]] = _run_line_common_configs (data.oncolliding [re [0]], data.oncolliding, geometry, ray_pts, object);
								# Runs all given actions.
								_run_line_ray_common_configs (data.oncolliding [re [0]], data.parent, geometry, object);
							# The current detected object is found.
							elif data.oncolliding.has (re [1]) && data.oncolliding [re [1]] is Dictionary && !data.oncolliding [re [1]].empty ():
								# Runs the given configurations.
								data.oncolliding [re [1]] = _run_line_common_configs (data.oncolliding [re [1]], data.oncolliding, geometry, ray_pts, object);
								# Runs all given actions.
								_run_line_ray_common_configs (data.oncolliding [re [1]], data.parent, geometry, object);
							# The collider has a group.
							elif not re [2].empty ():
								# Ckecks whether an id is found.
								var found_id: bool = false;
								# Searches the main group name.
								for id in re [2]:
									# The target group name has been found.
									if data.oncolliding.has (id) && data.oncolliding [id] is Dictionary && !data.oncolliding [id].empty ():
										# Runs the given configurations.
										data.oncolliding [id] = _run_line_common_configs (data.oncolliding [id], data.oncolliding, geometry, ray_pts, object);
										# Runs all given actions.
										_run_line_ray_common_configs (data.oncolliding [id], data.parent, geometry, object); found_id = true; break;
								# Is it found an id ?
								if not found_id: _run_line_ray_common_configs (data.oncolliding, data.parent, geometry, object);
							# On colliding scope.
							else: _run_line_ray_common_configs (data.oncolliding, data.parent, geometry, object);
					# No collision found.
					else: _run_line_ray_common_configs (data, null, geometry, object);
				# Otherwise.
				else: data = _run_line_common_configs (data, Dictionary ({}), geometry, data.points, object);
			# Destroys the existing immediate geometry.
			elif geometry is ImmediateGeometry: geometry.queue_free ();
		# Error message.
		else: _output ("The parent node must be an instance of Spatial.", Message.ERROR);

"""
	@Description: Draws a line with the passed points.
	@Parameters:
		bool show: Do you want to show final result ?
		String id: Contains the line name.
		Spatial | NodePath | String | Vector2 | Vector3 | PoolVector2Array | PoolVector3Array | Array | PoolStringArray pts: Contains all points to used to draw the line.
		Spatial parent: Where do you want to draw the line ?
		Color color: Contains the line color.
		int fze: Which axis will be froozen ? The possibles values are:
			-> MegaAssets.Axis.NONE or 0: No axis will be froozen.
			-> MegaAssets.Axis.X or 1: freeze x axis.
			-> MegaAssets.Axis.Y or 2: freeze y axis.
			-> MegaAssets.Axis.Z or 3: freeze z axis.
			-> MegaAssets.Axis.XY or 7: freeze xy axis.
			-> MegaAssets.Axis.XZ or 8: freeze xz axis.
			-> MegaAssets.Axis.YZ or 9: freeze yz axis.
			-> MegaAssets.Axis.XYZ or 13: freeze xyz axis.
		int mesh: Which mesh used to draw the line ? The possibles values are:
			-> Mesh.PRIMITIVE_LINES or 1: Render array as lines (every two vertices a line is created).
			-> Mesh.PRIMITIVE_LINE_STRIP or 2: Render array as line strip.
			-> Mesh.PRIMITIVE_LINE_LOOP or 3: Render array as line loop (like line strip, but closed).
		float delay: The dead time before drawing line.
"""
static func debug_line_3d (show: bool, id: String, pts, parent: Spatial, color: Color = Color.black, fze: int = Axis.NONE, mesh: int = 2, delay: float = 0.0) -> void:
	# The game is running.
	if !Engine.editor_hint:
		# If the game is not initialised.
		if delay == 0.0 && !is_game_initialised (): yield (parent.get_tree (), "idle_frame"); elif delay > 0.0: yield (parent.get_tree ().create_timer (delay), "timeout");
	# Getting the existing immediate geometry.
	id = str_replace (id, PoolStringArray (['.', ':', '@', '/', '"']), '').lstrip (' ').rstrip (' '); var geometry = parent.get_node_or_null (id);
	# Is it visible ?
	if show:
		# Converting points parameters into an array.
		pts = Array ([pts]) if !pts is Array && !pts is PoolVector2Array && !pts is PoolVector3Array else pts;
		# The mesh value is less than 1 and greather than 3.
		mesh = Mesh.PRIMITIVE_LINES if mesh <= Mesh.PRIMITIVE_LINES else mesh; mesh = Mesh.PRIMITIVE_LINE_LOOP if mesh >= Mesh.PRIMITIVE_LINE_LOOP else mesh;
		# Immediate geomety node don't exists.
		if not geometry is ImmediateGeometry:
			# Creating a new geometry from the given id and adds this node to the given parent and update geometry variable.
			var geo: ImmediateGeometry = ImmediateGeometry.new (); geo.name = id; parent.add_child (geo); geometry = parent.get_node_or_null (id);
			# Sets geometry position to his parent position.
			geometry.transform.origin = Vector3.ZERO;
		# Creates a new spatial material to fill the line and initializing geometry.
		var colour: SpatialMaterial = SpatialMaterial.new (); colour.albedo_color = color; geometry.material_override = colour; geometry.clear (); geometry.begin (mesh);
		# Adding each point a immediate geometry after clearing of the preview data.
		for pt in pts:
			# Drawing the current point.
			pt = _get_real_point_cords (fze, pt, parent); if pt != null: geometry.add_vertex (pt);
		# Ends the drawing sequence.
		geometry.end ();
	# Destroys the existing immediate geometry.
	elif geometry is ImmediateGeometry: geometry.queue_free ();

"""
	@Description: Draws a ray with the passed points.
	@Parameters:
		bool show: Do you want to show final result ?
		String id: Contains the ray name.
		Vector2 | Vector3 | Spatial | NodePath | String start: Contains the start point.
		Vector2 | Vector3 | Spatial | NodePath | String end: Contains the stop point.
		Spatial parent: Where do you want to draw the ray ?
		Color color: Contains the line color.
		int fze: which axis will be froozen ? The possibles values are:
			-> MegaAssets.Axis.NONE or 0: No axis will be froozen.
			-> MegaAssets.Axis.X or 1: freeze x axis.
			-> MegaAssets.Axis.Y or 2: freeze y axis.
			-> MegaAssets.Axis.Z or 3: freeze z axis.
			-> MegaAssets.Axis.XY or 7: freeze xy axis.
			-> MegaAssets.Axis.XZ or 8: freeze xz axis.
			-> MegaAssets.Axis.YZ or 9: freeze yz axis.
			-> MegaAssets.Axis.XYZ or 13: freeze xyz axis.
		float delay: The dead time before drawing ray.
"""
static func debug_ray_3d (show: bool, id: String, start, end, parent: Spatial, color: Color = Color.black, fze: int = Axis.NONE, delay: float = 0.0) -> void:
	# Just draws a line with start and end points.
	debug_line_3d (show, id, Array ([start, end]), parent, color, fze, Mesh.PRIMITIVE_LINE_STRIP, delay);

"""
	@Description: Determinates whether an input is a number (integer or real).
	@Parameters:
		Variant input: Contains an input.
"""
static func is_number (input) -> bool:
	# The imput is an integer or a real.
	if input is int || input is float: return true;
	# The input is a string.
	elif input is String:
		# Contains the real type value of the given string.
		var real_value = get_variant (input); return real_value is int or real_value is float;
	# Otherwise.
	else: return false;

"""
	@Description: Determinates whether an input is a vector (Vector2 or Vector3).
	@Parameters:
		Variant input: Contains an input.
"""
static func is_vector (input) -> bool:
	# The given input is a Vector2 or Vector3.
	if input is Vector2 or input is Vector3: return true;
	# The input is a PoolIntArray, PoolRealArray or PoolByteArray.
	elif input is PoolIntArray or input is PoolRealArray or input is PoolByteArray: return is_range (float (input.size ()), 2.0, 3.0);
	# The given input is an Array.
	elif input is Array && is_range (float (input.size ()), 2.0, 3.0) && is_number (input [0]) && is_number (input [1]):
		# Returns the final result.
		return input.size () == 2 || input.size () == 3 && is_number (input [2]);
	# The input is a string.
	elif input is String:
		# Contains the real type value of the given string.
		var real_value = get_variant (input); return !real_value is String and is_vector (real_value);
	# Otherwise.
	else: return false;

"""
	@Description: Subdivides a path represented by several points of the space in sub-small points of the space each of which belongs
		to the trajectory represented by the original points.
	@Parameters:
		Array | PoolVector2Array | PoolVector3Array points: Contains the set of points representing any path.
		int count: The different segments formed by the original points will be subdivided into how many sub-segments ?
"""
static func split_vector3_path (points, count: int):
	# Converts the passed points into an array.
	points = Array ([points]) if !is_array (points) else Array (points);
	# The count is greather than 1.
	if count > 1 and points.size () > 1:
		# Contains the final result.
		var results: PoolVector3Array = PoolVector3Array ([]);
		# Calculates subdivision.
		for j in range (len (points) - 1):
			# Corrects the current point and the next point.
			points [j] = any_to_vector3 (points [j]); points [(j + 1)] = any_to_vector3 (points [(j + 1)]);
			# Checks points type.
			if points [j] is Vector3 and points [(j + 1)] is Vector3:
				# Contains the distance between two points.
				var global_distance: float = points [j].distance_to (points [(j + 1)]);
				# Contains the distance of each subdivision.
				var sub_distance: float = (global_distance / count); var current_distance: float = 0.0;
				# Calculates all sub points coordinates.
				while current_distance <= global_distance:
					# Is it the first or the last element of the array ?
					if current_distance == 0.0 and (j - 1) < 0: results.append (points [j]); elif current_distance == global_distance: results.append (points [(j + 1)]);
					# Otherwise.
					elif current_distance > 0.0:
						# Calculates the current sub point coordinates and adds the points to existing results.
						var x: float = ((current_distance * (points [(j + 1)].x - points [j].x)) + points [j].x);
						var y: float = ((current_distance * (points [(j + 1)].y - points [j].y)) + points [j].y);
						var z: float = ((current_distance * (points [(j + 1)].z - points [j].z)) + points [j].z); results.append (Vector3 (x, y, z));
					# Increases the current distance for the next point.
					current_distance += sub_distance;
			# Error message.
			else: _output ("Can't generate the splited vectors path.", Message.ERROR);
		# Returns the final result.
		return results;
	# Otherwise.
	else: return points;

"""
	@Description: Converts an input into Vector2.
	@Parameters:
		Variant input: Contains an input.
"""
static func any_to_vector2 (input):
	# The input is a Vector2 or Vector3.
	if input is Vector2 or input is Vector3: return Vector2 (input.x, input.y);
	# The input is a PoolIntArray, PoolRealArray or PoolByteArray.
	elif input is PoolIntArray or input is PoolRealArray or input is PoolByteArray:
		# The input size is equal to 2 or 3.
		if is_range (float (input.size ()), 2.0, 3.0): return Vector2 (float (input [0]), float (input [1])); else: return null;
	# The given input is an Array.
	elif input is Array && is_range (float (input.size ()), 2.0, 3.0) && is_number (input [0]) && is_number (input [1]):
		# Returns the final result.
		return Vector2 (float (input [0]), float (input [1]));
	# The input is a String.
	elif input is String:
		# Contains the real type value of the given string.
		var real_value = get_variant (input); return any_to_vector2 (real_value) if not real_value is String else null;
	# Otherwise.
	else: return null;

"""
	@Description: Converts an input into Vector3.
	@Parameters:
		Variant input: Contains an input.
"""
static func any_to_vector3 (input):
	# The input is a Vector2 or Vector3.
	if input is Vector3: return input; elif input is Vector2: return Vector3 (input.x, input.y, 0.0);
	# The input is a PoolIntArray, PoolRealArray or PoolByteArray.
	elif input is PoolIntArray or input is PoolRealArray or input is PoolByteArray:
		# Checks whether input size is equal to 2.
		if input.size () == 2: return Vector3 (float (input [0]), float (input [1]), 0.0);
		# Checks whether input size is equal to 3.
		elif input.size () == 3: return Vector3 (float (input [0]), float (input [1]), float (input [2])); else: return null;
	# The given input is an Array.
	elif input is Array && is_range (float (input.size ()), 2.0, 3.0) && is_number (input [0]) && is_number (input [1]):
		# The input size is equal to 2.
		if input.size () == 2: return Vector3 (float (input [0]), float (input [1]), 0.0);
		# Checks whether input size is equal to 3.
		elif is_number (input [2]): return Vector3 (float (input [0]), float (input [1]), float (input [2])); else: return null;
	# The input is a String.
	elif input is String:
		# Contains the real type value of the given string.
		var real_value = get_variant (input); return any_to_vector3 (real_value) if not real_value is String else null;
	# Otherwise.
	else: return null;

"""
	@Description: Generates a PoolBytesArray. This method can be useful for key generation.
	@Parameters:
		int size: Contains the PoolByteArray size.
"""
static func rand_bytes (size: int) -> PoolByteArray:
	# The bytes array size is greather than 0.
	if size > 0:
		# Generates a bytes array.
		var bytes = PoolByteArray ([]); for _i in range (size): bytes.append (randi () % 256); return bytes;
	# Otherwise.
	else: return PoolByteArray ([]);

"""
	@Description: Replaces one or more character(s) with one or several other character(s).
	@Parameters:
		String string: Contains the string value.
		String | PoolStringArray what: What is/are the character(s) that will be replaced ?
		String | PoolStringArray to: What is/are the character(s) that will take the place of the designated character(s) ?
		String | int start: From which character or position the replacements will be made ?
		String | int end: What character or position the replacements will stop ?
"""
static func str_replace (string: String, what, to, start = -1, end = -1) -> String:
	# Converts "what" and "to" parameters into a PoolStringArray.
	what = PoolStringArray (Array ([what]) if not is_array (what) else Array (what)); to = PoolStringArray (Array ([to]) if not is_array (to) else Array (to));
	# Converts "start" parameter into an integer.
	start = int (start) if is_number (start) else (string.find (start) if start is String else 0);
	# Converts "end" parameter into an integer.
	end = int (end) if is_number (end) else (string.find (end) if end is String else (len (string) - 1));
	# Corrects the start and end value.
	start = start if is_range (start, 0, (len (string) - 1)) else 0; end = end if is_range (end, start, (len (string) - 1)) else (len (string) - 1);
	# Corrects asignment or the start value is equal to the end value.
	if start == 0 and end == (len (string) - 1):
		# Starts replacing each character to each other.
		for k in what.size ():
			# Replaces the current string into "what" array to other string into "to" array.
			if !what [k].empty (): string = string.replace (what [k], to [k]) if k < len (to) else string.replace (what [k], to [(len (to) - 1)]);
	# Otherwise.
	else:
		# Replaces the current string into "what" array to other string into "to" array.
		for k in what.size (): for x in range (start, end): string [x] = (to [k] if k < len (to) else to [(len (to) - 1)]) if string [x] == what [k] else string [x];
	# Returns the final result with replacement(s).
	return string;

"""
	@Description: Removes characters on the left and right from a string.
	@Parameters:
		String string: Contains the string value.
		String | PoolStringArray what_left: What is/are the character(s) to be deleted to the left of the given character string ?
		String | PoolStringArray what_right: What is/are the character(s) to be deleted to the right of the given character string ?
"""
static func str_lrstrip (string: String, what_left = String (''), what_right = String ('')) -> String:
	# Converts "what_left" parameter into a PoolStringArray.
	what_left = PoolStringArray (Array ([what_left]) if not is_array (what_left) else Array (what_left));
	# Converts "what_right" parameter into a PoolStringArray.
	what_right = PoolStringArray (Array ([what_right]) if not is_array (what_right) else Array (what_right));
	# Starts removing each character from the left string.
	for k in what_left.size (): if !what_left [k].empty (): string = string.lstrip (what_left [k]);
	# Starts removing each character from the right string and returns the final result with replacement(s).
	for y in what_right.size (): if !what_right [y].empty (): string = string.rstrip (what_right [y]); return string;

"""
	@Description: Returns the corresponding option position about "Format" enumeration of Godot Image class.
	@Parameters:
		int option: Contains an option position of "MegaAssets.ImageFormat" enumeration.
"""
static func get_real_image_format (option: int) -> int:
	# Apply all supported formats.
	if option <= ImageFormat.RH: option = Image.FORMAT_RH; elif option == ImageFormat.RF: option = Image.FORMAT_RF;
	elif option == ImageFormat.RGH: option = Image.FORMAT_RGH; elif option == ImageFormat.RGF: option = Image.FORMAT_RGF;
	elif option == ImageFormat.RGBH: option = Image.FORMAT_RGBH; elif option == ImageFormat.RGBF: option = Image.FORMAT_RGBF;
	elif option == ImageFormat.RGBAH: option = Image.FORMAT_RGBAH; elif option == ImageFormat.RGBAF: option = Image.FORMAT_RGBAF;
	elif option == ImageFormat.RGBA4444: option = Image.FORMAT_RGBA4444; elif option == ImageFormat.RGBA5551: option = Image.FORMAT_RGBA5551;
	else: option = Image.FORMAT_RGBE9995; return option;

"""
	@Description: Returns the corresponding option position about "CompressMode" enumeration of Godot Image class.
	@Parameters:
		int option: Contains an option position of "MegaAssets.ImageCompression" enumeration.
"""
static func get_real_image_compression (option: int) -> int: return 3 if option == 1 else (4 if option == 2 else (0 if option >= 3 else -1));

"""
	@Description: Returns all data from the created screenshot from the active camera.
	@Parameters:
		Node object: Which knot will be considered to make the differents operations ?
		Dictionary data: Contains the different configurations to be made on the capture, once generated. For more information, consult the documentation
			about "get_screen_shot ()" method.
"""
static func get_screen_shot_data (object: Node, data: Dictionary = Dictionary ({})) -> Array:
	# Returns the generated game screenshot data.
	return Array ((get_screen_shot (object, data) as Texture).get_data ().get_data ());

"""
	@Description: Opens the documentation associated with the given class name, method, property and of an event.
	@Parameters:
		Node object: Which knot will be considered to make the differents operations ?
		String path: A web link to access to module documentation directly.
		String feature: What the is the target feature on the documentation ?
		float delay: What is the timeout before open module documentation ?
"""
static func open_doc_manager (object: Node, path: String = String (''), feature: String = String (''), delay: float = 0.0) -> void:
	# Waiting for the given delay.
	if delay > 0.0 and !Engine.editor_hint: yield (object.get_tree ().create_timer (delay), "timeout");
	# Corrects the given feature name.
	feature = feature.lstrip ('(').rstrip ('(').lstrip (')').rstrip (')').replace (' ', '');
	# Opens the default user browser with the class documentation path.
	if OS.shell_open (path + (('#' + feature) if not feature.empty () else '')) != OK: pass;

"""
	@Description: Loads the contents of an extension file (.csv).
	@Parameters:
		String path: Contains the path pointing to the csv file to loaded.
		String separator: What string should we use to distinguish between different data in csv file ?
"""
static func load_csv_file (path: String, separator: String = ',') -> Array:
	# Corrects path separator and removes all spaces.
	path = path.replace ('\\', '/').replace (' ', '');
	# If path value is not empty.
	if not path.empty ():
		# Checks path value.
		if path.find_last ('.') != -1:
			# Checks csv file extension.
			if path.ends_with (".csv"):
				# Contains the file name and his reference.
				var fname: String = path.get_file (); var res: Array = Array ([]); var csv_file: File = File.new (); var index: int = 0;
				# Opens the given csv file.
				if csv_file.open (path, File.READ) == OK:
					# Gets the first line of the opened csv file.
					var line: String = csv_file.get_line ();
					# We aren't at the end of the csv file.
					while not line.empty ():
						# Splits the current line.
						var sp: PoolStringArray = line.split (separator);
						# The line count is null.
						if index == 0:
							# Initializes all available languages.
							for k in range (1, sp.size ()): if not sp [k].empty (): res.append (Dictionary ({sp [k]: Dictionary ({})}));
						# Otherwise.
						else:
							# A key has been donated.
							if sp.size () > 0 and not sp [0].empty ():
								# Getting the associate colonne value.
								for j in range (res.size ()):
									# Some data has been detected.
									var cur = (j + 1); if cur < sp.size (): res [j] [res [j].keys () [0]] [sp [0]] = (sp [cur] if not sp [cur].empty () else "Null");
									# No data found.
									else: res [j] [res [j].keys () [0]] [sp [0]] = "Null";
						# Go to the next line and increases the current line count.
						line = csv_file.get_line (); index += 1;
					# Returns the final result.
					csv_file.close (); return res;
				# Warning message.
				else: _output (("Failed to load {" + fname + "}."), Message.WARNING);
			# Error message.
			else: _output ("The extension of the csv file must be (.csv) format.", Message.ERROR);
		# Warning message.
		else: _output ("The csv file is not defined.", Message.WARNING);
	# Warning message.
	else: _output ("The path is not defined.", Message.WARNING); return Array ([]);

"""
	@Description: Opens the documentation associated with this class.
	@Parameters:
		Node object: Which node will be considered to perform the different operations ?
		String feature: The documentation will target which functionality of style ?
		float delay: What is the deadtime before the opening of the documentation ?
"""
static func open_doc (object: Node, feature: String = String (''), delay: float = 0.0) -> void:
	# Opens the documentation.
	open_doc_manager (object, "https://godot-mega-assets.herokuapp.com/docs/bases/megaassets", feature, delay);

"""
	@Description: Opens the documentation associated with this class.
	@Parameters:
		int visibility: What is the visibility of the target method ?
			-> MegaAssets.MethodAccessMode.PUBLIC or 0: For public methods.
			-> MegaAssets.MethodAccessMode.PRIVATE or 1: For private methods.
			-> MegaAssets.MethodAccessMode.PROTECTED or 2: For protected methods.
			-> MegaAssets.MethodAccessMode.ANY or 3: For method convention detection. Note that option use python methods visibility convention.
		String | PoolStringArray scripts: What is all derived classes file names ? Use only this option on "PROTECTED" mode.
"""
static func apply_visibility (visibility: int = MethodAccessMode.ANY, scripts = null) -> bool:
	# Getting program code stack.
	var stack = get_stack (); if not stack.empty ():
		# Removes the first element(s) of the list.
		stack.pop_front (); if stack [0].function == "apply_visibility": stack.pop_front ();
		# Corrects the stack trace for other case.
		stack = Array ([stack [0], stack [1]]) if len (stack) >= 2 else stack; stack = stack [0] if len (stack) == 1 else stack;
		# For private visibility.
		if visibility == MethodAccessMode.PRIVATE:
			# Checks the code stack count.
			if stack is Array and stack.size () == 2:
				# Checks method caller source file.
				if stack [0].source != stack [1].source:
					# Contains the message that will be show into the console.
					var message: String = (str (stack [1].line) + " from {" + stack [1].source + " -> " + stack [1].function + " ()} file.");
					# Error message.
					_output (('{' + stack [0].function + " ()} method call is denied. Because it private::line " + message), Message.ERROR); return false;
				# Otherwise.
				else: return true;
			# Otherwise.
			else: return true;
		# For protected visibility.
		elif visibility == MethodAccessMode.PROTECTED:
			# Checks the code stack count.
			if stack is Array and stack.size () == 2:
				# The call comes from the same file.
				if stack [0].source == stack [1].source: return true;
				# Otherwise.
				else:
					# Converts the given script(s) into an array.
					scripts = PoolStringArray (Array ([scripts])) if not is_array (scripts) else PoolStringArray (Array (scripts));
					# Corrects the passed script(s) file(s).
					for n in range (len (scripts)): scripts [n] = ((scripts [n] + ".gd") if not scripts [n].ends_with (".gd") else scripts [n]).to_lower ();
					# An external call has been detected.
					if not stack [1].source.get_file ().to_lower () in scripts:
						# Contains the message that will be show into the console.
						var msg: String = (str (stack [1].line) + " from {" + stack [1].source + " -> " + stack [1].function + " ()} file.");
						# Error message.
						_output (('{' + stack [0].function + " ()} method call is denied. Because it protected::line" + msg), Message.ERROR); return false;
					# Otherwise.
					else: return true;
			# Otherwise.
			else: return true;
		# For any visibility.
		elif visibility >= MethodAccessMode.ANY:
			# For private method convention.
			if stack [0].function [0] == '_' and stack [0].function [1] == '_': return apply_visibility (MethodAccessMode.PRIVATE);
			# For protected method convention.
			elif stack [0].function.begins_with ('_'): return apply_visibility (MethodAccessMode.PROTECTED, scripts);
			# For public method convention.
			else: return apply_visibility (MethodAccessMode.PUBLIC);
	# No constraints found.
	return true;
