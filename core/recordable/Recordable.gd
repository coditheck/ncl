"""
	@author: Obrymec
	@company: CodiTheck
	@famework: Godot Mega Assets
	@compatibility: Godot 3.x.x
	@platform: ANDROID || IOS || MACOSX || UWP || HTML5 || WINDOWS || LINUX
	@license: MIT
	@source: https://godot-mega-assets.herokuapp.com/home
	@language: GDscript
	@dimension: 2D || 3D
	@type: Recordable
	@version: 0.2.4
	@created: 2021-06-16
	@updated: 2021-03-19
"""
################################################################################### [Main class] #############################################################################
"""@Description: A class that represents some basics functionalities common to all indestructibles saveables modules of the Godot MegaAssets framework."""
tool class_name Recordable, "saveable.svg" extends Indestructible;

##################################################################################### [Signals] ##############################################################################
"""@Description: Trigger before updating manager of different game data."""
# warning-ignore:unused_signal
signal before_update_data ();
"""@Description: Trigger after updating the manager of the various game data."""
# warning-ignore:unused_signal
signal after_update_data ();
"""@Description: Trigger before saving module data."""
# warning-ignore:unused_signal
signal before_save_data ();
"""@Description: Trigger after saving module data."""
# warning-ignore:unused_signal
signal after_save_data ();
"""@Description: Trigger before loading module data."""
# warning-ignore:unused_signal
signal before_load_data ();
"""@Description: Trigger after loading module data."""
# warning-ignore:unused_signal
signal after_load_data ();
"""@Description: Trigger before destroying module data."""
# warning-ignore:unused_signal
signal before_destroy_data ();
"""@Description: Trigger after destroying module data."""
# warning-ignore:unused_signal
signal after_destroy_data ();

############################################################################### [Logic and main tasks] #######################################################################
## This method is called on game initialization and before all nodes instanciation.
func _init () -> void: self._basics_saveable_module_properties ();

# Called before ready method run.
func _enter_tree () -> void: if not self.is_initialized (): self._saveable_initialization (true);

# Returns a base class name.
func _get_class_name () -> String: return "Recordable" if self.apply_visibility (self.MethodAccessMode.PROTECTED, PoolStringArray (["Module", "Indestructible"])) else "Null";

################################################################################ [Availables features] #######################################################################
"""@Description: Returns module version."""
static func get_version () -> String: return "0.2.4";

"""@Description: Returns module origins."""
static func get_origin_name () -> String: return "MegaAssets.Module.Indestructible.Recordable";

"""@Description: Determinates whether module is saveable."""
static func is_saveable () -> bool: return true;

"""@Description: Returns module class name."""
func get_class () -> String: return "Recordable";

"""@Description: Determinates whether data within of module have been saved into the game data manager. It is only available on saveable modules."""
func is_saved () -> bool: return self._is_saved ();

"""
	@Description: Updates the game data manager. This function is only available on saveable modules.
	@Parameters:
		float delay: What is the timeout before updating module data ?
"""
func update_data (delay: float = 0.0) -> void: self._update_data (delay);

"""
	@Description: Back up data of module using the game's data management system. Note that it is not recommended to use this method when you want to make
		several backups at the same time. This function is only available on saveable modules.
	@Parameters:
		float delay: What is the timeout before saving module data ?
"""
func save_data (delay: float = 0.0) -> void: self._save_data (delay);

"""
	@Description: Loads data of module using the game's data management system. This function is only available on saveable modules.
	@Parameters:
		float delay: What is the timeout before updating module data ?
"""
func load_data (delay: float = 0.0) -> void: self._load_data (delay);

"""
	@Description: Destroys all data linked to a data manager module. Warning ! there will be no going back after the destruction of the latter.
		This function is only available on modules saveable.
	@Parameters:
		float delay: What is the timeout before updating module data ?
"""
func remove_data (delay: float = 0.0) -> void: self._remove_data (delay);

"""
	@Description: Opens the documentation associated with this class.
	@Parameters:
		Node object: Which node will be considered to perform the different operations ?
		String feature: The documentation will target which functionality of style ?
		float delay: What is the deadtime before the opening of the documentation ?
"""
static func open_doc (object: Node, feature: String = String (''), delay: float = 0.0) -> void:
	# Opens the documentation.
	open_doc_manager (object, "https://godot-mega-assets.herokuapp.com/docs/bases/recordable", feature, delay);

"""
	@Description: Restarts module. Made very careful during module reboots. This can be problematic in certain cases.
	@Parameters:
		float delay: What is the timeout before restarting module ?
"""
func restart (delay: float = 0.0) -> void:
	# The module is it enabled ?
	if self.check_initialization () and self.is_unlock ():
		# The game is running.
		if delay > 0.0 and !Engine.editor_hint: yield (self.get_tree ().create_timer (delay), "timeout");
		# Makes a big restarting on the current module reference, calls the last definition of Godot "_enter_tree ()" and "_ready ()" methods.
		self.verbose ("Recordable restarting..."); .restart (); self._enter_tree (); self._ready ();
